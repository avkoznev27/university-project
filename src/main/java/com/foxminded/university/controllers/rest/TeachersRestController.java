package com.foxminded.university.controllers.rest;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.TeacherService;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnUpdate;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/teachers")
@Tag(name = "Teachers", description = "Manage university teachers")
public class TeachersRestController {

    private TeacherService teacherService;

    @Autowired
    public TeachersRestController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping
    @Operation(summary = "Get list of all university teachers")
    public List<Teacher> getAllTeachers() {
        return teacherService.getAllTeachers();
    }
    
    @GetMapping("/count")
    @Operation(summary = "Get the number of university teachers")
    public long getCount() {
        return teacherService.countTeachers();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get teacher by ID")
    public Teacher getTeacher(@PathVariable("id") long id) {
        return teacherService.getTeacherById(id);
    }
    
    @GetMapping("/{id}/courses")
    @Operation(summary = "Get teacher courses by teacher ID")
    public Set<Course> getTeacherCourses(@PathVariable("id") long id) {
        return teacherService.getTeacherById(id).getCourses();
    }
    
    @GetMapping("/{id}/lessons")
    @Operation(summary = "Get teacher lessons by teacher ID")
    public List<Lesson> getTeacherLessons(@PathVariable("id") long id) {
        return teacherService.getTeacherById(id).getLessons();
    }
    
    @GetMapping("/{id}/timetable")
    @Operation(summary = "Get teacher timetable by teacher ID", 
    description = "To get the daily timetable, just pass the parameter start day."
            + "To get a shedule for a period, pass parameters start day and end day")
    public List<Lesson> createTeacherTimetable(@PathVariable("id") long id, 
                                               @Parameter(example = "2020-09-01") @RequestParam("startDay") String startDay,
                                               @Parameter(example = "2020-09-07") @RequestParam(required = false, value = "endDay") String endDay) {
        Teacher teacher = teacherService.getTeacherById(id);
        LocalDate dateStartDay = LocalDate.parse(startDay);
        if (endDay == null || startDay.equals(endDay)) {
            return teacherService.getTeacherTimetable(teacher, dateStartDay);
        } else {
            LocalDate dateEndDay = LocalDate.parse(endDay);
            return teacherService.getTeacherTimetable(teacher, dateStartDay, dateEndDay);
        }
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Add a new teacher to the university database")
    public Teacher createTeacher(@Validated(OnCreate.class) @RequestBody Teacher teacher) {
        return teacherService.addTeacher(teacher);
    }

    @PutMapping
    @Operation(summary = "Update existing teacher")
    public Teacher updateTeacher(@Validated(OnUpdate.class) @RequestBody Teacher teacher) {
        return teacherService.updateTeacher(teacher);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete teacher by ID", 
    description = "Removing only teachers without lessons. Deletion teachers with lessons is impossible")
    public Teacher deleteTeacher(@PathVariable("id") long id) {
        Teacher teacher = teacherService.getTeacherById(id);
        teacherService.removeTeacher(teacher);
        return teacher;
    }
}
