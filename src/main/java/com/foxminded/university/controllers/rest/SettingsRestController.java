package com.foxminded.university.controllers.rest;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.foxminded.university.domain.model.Settings;
import com.foxminded.university.domain.service.SettingsService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/settings")
@Tag(name = "Settings", description = "Manage university parameters")
public class SettingsRestController {

    private SettingsService settingsService;

    @Autowired
    public SettingsRestController(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    @GetMapping
    @Operation(summary = "Get current settings")
    public Settings getSettings() {
        return settingsService.getSettings();
    }

    @PostMapping("/default")
    @Operation(summary = "Set default settings")
    public Settings setDefault() {
        return settingsService.setDefaultSettings();
    }

    @PutMapping("/update")
    @Operation(summary = "Update settings")
    public Settings saveSettings(@Valid @RequestBody Settings settings) {
        return settingsService.updateSettings(settings);
    }
}
