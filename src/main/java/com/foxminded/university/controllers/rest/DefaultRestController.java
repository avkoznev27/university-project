package com.foxminded.university.controllers.rest;

import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@RestControllerAdvice
public class DefaultRestController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultRestController.class);
    
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(ResourceNotFoundException.class)
    public Map<String, String> handleResourceNotFoundException(ResourceNotFoundException ex) {
        LOGGER.debug("Handling ResourceNotFoundException: {}", ex.getMessage());
        Map<String, String> errors = new HashMap<>();
        errors.put(ex.getClass().getSimpleName(), ex.getMessage());
        return errors;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ServiceException.class)
    public Map<String, String> handleServiceException(ServiceException ex) {
        LOGGER.debug("Handling ServiceException: {}", ex.getMessage());
        Map<String, String> errors = new HashMap<>();
        errors.put(ex.getClass().getSimpleName(), ex.getMessage());
        return errors;
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(DateTimeParseException.class)
    public Map<String, String> handleDateTimeParseException(DateTimeParseException ex) {
        LOGGER.debug("Handling DateTimeParseException: {}", ex.getMessage());
        Map<String, String> errors = new HashMap<>();
        errors.put(ex.getClass().getSimpleName(), ex.getMessage());
        return errors;
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        LOGGER.debug("Handling ValidationException: {}", ex.getMessage());
        Map<String, String> errors = new HashMap<>();
        ex.getFieldErrors().forEach(error ->
            errors.put(error.getField(), error.getDefaultMessage()));
        ex.getGlobalErrors().forEach(error -> {
            String objectName = error.getObjectName() + "-" + error.getCode();
            String errorMessage = error.getDefaultMessage();
            errors.put(objectName, errorMessage);
        });
        return errors;
    }
    
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public Map<String, String> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
        LOGGER.debug("Handling HttpMessageNotReadableException: {}", ex.getMessage());
        Map<String, String> errors = new HashMap<>();
        if (ex.getCause() != null) {
            errors.put(ex.getCause().getClass().getSimpleName(), ex.getCause().getMessage());
        } else {
            errors.put(ex.getClass().getSimpleName(), ex.getMessage());
        }
        return errors;
    }
}
