package com.foxminded.university.controllers.rest;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.StudentService;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnUpdate;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/students")
@Tag(name = "Students", description = "Manage university students")
public class StudentsRestController {
    
    private StudentService studentService;
    private GroupService groupService;
    
    @Autowired
    public StudentsRestController(StudentService studentService, GroupService groupService) {
        this.studentService = studentService;
        this.groupService = groupService;
    }
    
    @GetMapping
    @Operation(summary = "Get list of all university students")
    public List<Student> getAllStudents() {
        return studentService.getAllStudents();
    }

    @GetMapping("/count")
    @Operation(summary = "Get the number of university students")
    public long getCount() {
        return studentService.countStudents();
    }

    @GetMapping("/{id}")
    @Operation(summary = "Get student by ID")
    public Student getStudent(@PathVariable("id") long id) {
        return studentService.getStudentById(id);
    }

    @GetMapping("/{id}/courses")
    @Operation(summary = "Get student courses by student ID")
    public Set<Course> getStudentCourses(@PathVariable("id") long id) {
        return studentService.getStudentById(id).getCourses();
    }
    
    @GetMapping("/{id}/lessons")
    @Operation(summary = "Get student lessons by student ID")
    public List<Lesson> getStudentLessons(@PathVariable("id") long id) {
        Student student = studentService.getStudentById(id);
        return groupService.getGroupById(student.getGroupId()).getLessons();
    }
    
    @GetMapping("/{id}/timetable")
    @Operation(summary = "Get student timetable by student ID", 
    description = "To get the daily timetable, just pass the parameter start day."
            + "To get a shedule for a period, pass parameters start day and end day")
    public List<Lesson> createStudentTimetable(@PathVariable("id") long studentId, 
                                               @Parameter(example = "2020-09-01") @RequestParam("startDay") String startDay,
                                               @Parameter(example = "2020-09-03") @RequestParam(required = false, value = "endDay") String endDay) {
        Student student = studentService.getStudentById(studentId);
        LocalDate dateStartDay = LocalDate.parse(startDay);
        if (endDay == null || startDay.equals(endDay)) {
            return studentService.getStudentTimetable(student, dateStartDay);
        } else {
            LocalDate dateEndDay = LocalDate.parse(endDay);
            return studentService.getStudentTimetable(student, dateStartDay, dateEndDay);
        }
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Add a new student to the university database")
    public Student createStudent(@Validated(OnCreate.class) @RequestBody Student student) {
        Group group = groupService.getGroupById(student.getGroupId());
        return studentService.addStudent(group, student);
    }
    
    @PutMapping
    @Operation(summary = "Update existing student")
    public Student updateStudent(@Validated(OnUpdate.class) @RequestBody Student student) {
        return studentService.updateStudent(student);
    }
    
    @DeleteMapping("/{id}")
    @Operation(summary = "Delete student by ID")
    public Student deleteStudent(@PathVariable("id") long id) {
        Student student = studentService.getStudentById(id);
        studentService.removeStudent(student);
        return student;
    }
}
