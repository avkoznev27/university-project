package com.foxminded.university.controllers.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.service.AuditoriumService;
import com.foxminded.university.domain.service.DateTimeService;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnUpdate;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/auditoriums")
@Tag(name = "Auditoriums", description = "Manage university auditoriums")
public class AuditoriumsRestController {
    
    private AuditoriumService auditoriumService;
    private DateTimeService dateTimeService;
    
    @Autowired
    public AuditoriumsRestController(AuditoriumService auditoriumService, DateTimeService dateTimeService) {
        this.auditoriumService = auditoriumService;
        this.dateTimeService = dateTimeService;
    }
    
    @GetMapping
    @Operation(summary = "Get list of all university auditoriums")
    public List<Auditorium> getAllAuditoriums() {
        return auditoriumService.getAllAuditoriums();
    }
    
    @GetMapping("/count")
    @Operation(summary = "Get the number of university auditoriums")
    public long getCount() {
        return auditoriumService.countAuditoriums();
    }
    
    @GetMapping("/{id}")
    @Operation(summary = "Get auditorium by ID")
    public Auditorium getAuditorium(@PathVariable("id") long id) {
        return auditoriumService.getAuditoriumById(id);
    }
    
    @GetMapping("/{id}/lessons")
    @Operation(summary = "Get auditorium lessons by auditorium ID")
    public List<Lesson> getAuditoriumLessons(@PathVariable("id") long id) {
        return auditoriumService.getAuditoriumById(id).getLessons();
    }
    
    @GetMapping("/free/{dateTimeId}")
    @Operation(summary = "Get free auditorium by date time ID")
    public List<Auditorium> getAllFreeAuditoriums(@PathVariable("dateTimeId") long dateTimeId) {
        DateTime dateTime = dateTimeService.getById(dateTimeId);
        return auditoriumService.getAllFreeAuditoriums(dateTime);
    }
    
    @GetMapping("/free/{dateTimeId}/{capacity}")
    @Operation(summary = "Get free auditorium by date time ID with capacity less than or equal to this")
    public List<Auditorium> getAllFreeAuditoriums(@PathVariable("dateTimeId") long dateTimeId, @PathVariable("capacity") int capacity) {
        DateTime dateTime = dateTimeService.getById(dateTimeId);
        return auditoriumService.getAllFreeAuditoriums(dateTime, capacity);
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Add a new auditorium to the university database")
    public Auditorium createAuditorium(@Validated(OnCreate.class) @RequestBody Auditorium auditorium) {
        return auditoriumService.addAuditorium(auditorium);
    }
    
    @PutMapping
    @Operation(summary = "Update existing auditorium")
    public Auditorium updateAuditorium(@Validated(OnUpdate.class) @RequestBody Auditorium auditorium) {
        return auditoriumService.updateAuditorium(auditorium);
    }
    
    @DeleteMapping("/{id}")
    @Operation(summary = "Delete auditorium by ID", 
    description = "Removing only auditoriums without lessons. Deletion auditoriums with lessons is impossible")
    public Auditorium deleteAuditorium(@PathVariable("id") long id) {
        Auditorium auditorium = auditoriumService.getAuditoriumById(id);
        auditoriumService.removeAuditorium(auditorium);
        return auditorium;
    }
}
