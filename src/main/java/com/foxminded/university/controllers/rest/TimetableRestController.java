package com.foxminded.university.controllers.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.service.DateTimeService;
import com.foxminded.university.domain.service.LessonService;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnUpdate;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import com.foxminded.university.domain.validators.LessonUpdateValidation;
import com.foxminded.university.domain.validators.LessonCreateValidation;

@RestController
@RequestMapping("/api/timetable")
@Tag(name = "Timetable", description = "Manage university timetable")
public class TimetableRestController {
    
    private LessonService lessonService;
    private DateTimeService dateTimeService;
    
    @Autowired
    public TimetableRestController(LessonService lessonService, DateTimeService dateTimeService) {
        this.lessonService = lessonService;
        this.dateTimeService = dateTimeService;
    }
    
    @GetMapping("/datetimes")
    @Operation(summary = "Get list of all date times in timetable")
    public List<DateTime> getAllDateTimes() {
        return dateTimeService.getAllDateTimes();
    }
    
    @GetMapping("/datetimes/{dateTimeId}")
    @Operation(summary = "Get date time by ID")
    public DateTime getDateTime(@PathVariable("dateTimeId") long dateTimeId) {
        return dateTimeService.getById(dateTimeId);
    }
    
    @GetMapping("/datetimes/{dateTimeId}/lessons")
    @Operation(summary = "Get all lessons for date time by date time ID")
    public List<Lesson> getLessonsDateTime(@PathVariable("dateTimeId") long dateTimeId) {
        return dateTimeService.getById(dateTimeId).getLessons();
    }
    
    @GetMapping("/lessons")
    @Operation(summary = "Get list of all lessons in timetable")
    public List<Lesson> getAllLessons() {
        return lessonService.getAllLessons();
    }
    
    @GetMapping("/lessons/{lessonId}") 
    @Operation(summary = "Get lesson by ID")
    public Lesson getLesson(@PathVariable("lessonId") long lessonId) {
        return lessonService.getLessonById(lessonId);
    }
    
    @PostMapping("/lessons")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Add a new lesson to the university timetable")
    public Lesson createLesson(@Validated(LessonCreateValidation.class) @RequestBody Lesson lesson) {
        return lessonService.addLesson(lesson);
    }
    
    @PostMapping("/datetimes")
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Add a new date time to the university timetable")
    public DateTime createDateTime(@Validated(OnCreate.class) @RequestBody DateTime dateTime) {
        return dateTimeService.addDateTime(dateTime);
    }
    
    @PutMapping("/datetimes")
    @Operation(summary = "Update existing date time")
    public DateTime updateDateTime(@Validated(OnUpdate.class) @RequestBody DateTime dateTime) {
        return dateTimeService.updateDateTime(dateTime);
    }
    
    @PutMapping("/lessons")
    @Operation(summary = "Update existing lesson")
    public Lesson updateLesson(@Validated(LessonUpdateValidation.class) @RequestBody Lesson lesson) {
        return lessonService.updateLesson(lesson);
    }
    
    @DeleteMapping("/datetimes/{dateTimeId}")
    @Operation(summary = "Delete date time by ID", 
    description = "Removing only date time without lessons. Deletion date time with lessons is impossible")
    public DateTime deleteTime(@PathVariable("dateTimeId") long dateTimeId) {
        DateTime dateTime = dateTimeService.getById(dateTimeId);
        dateTimeService.removeDateTime(dateTime);
        return dateTime;
    }
    
    @DeleteMapping("/lessons/{lessonId}")
    @Operation(summary = "Delete lesson by ID")
    public Lesson deleteLesson(@PathVariable("lessonId") long lessonId) {
        Lesson lesson = lessonService.getLessonById(lessonId);
        lessonService.removeLesson(lesson);
        return lesson;
    }
}
