package com.foxminded.university.controllers.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.service.DateTimeService;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnUpdate;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/groups")
@Tag(name = "Groups", description = "Manage university groups")
public class GroupsRestController {

    private GroupService groupService;
    private DateTimeService dateTimeService;

    @Autowired
    public GroupsRestController(GroupService groupService, DateTimeService dateTimeService) {
        this.groupService = groupService;
        this.dateTimeService = dateTimeService;
    }

    @GetMapping
    @Operation(summary = "Get list of all university groups")
    public List<Group> getAllGroups() {
        return groupService.getAllGroups();
    }
    
    @GetMapping("/count")
    @Operation(summary = "Get the number of university groups")
    public long getCount() {
        return groupService.countGroups();
    }
    
    @GetMapping("/{id}")
    @Operation(summary = "Get group by ID")
    public Group getGroup(@PathVariable("id") long id) {
        return groupService.getGroupById(id);
    }
    
    @GetMapping("/{id}/lessons")
    @Operation(summary = "Get group lessons by group ID")
    public List<Lesson> getGroupLessons(@PathVariable("id") long id) {
        return groupService.getGroupById(id).getLessons();
    }
    
    @GetMapping("/{id}/students")
    @Operation(summary = "Get group students by group ID")
    public List<Student> getGroupStudents(@PathVariable("id") long id) {
        return groupService.getGroupById(id).getStudents();
    }
    
    @GetMapping("/incomplete")
    @Operation(summary = "Get all incomplete groups")
    public List<Group> getAllIncompleteGroups() {
        return groupService.getAllIncompleteGroups();
    }
    
    @GetMapping("/free/{dateTimeId}")
    @Operation(summary = "Get free groups by date time ID")
    public List<Group> getAllFreeGroups(@PathVariable("dateTimeId") long dateTimeId) {
        DateTime dateTime = dateTimeService.getById(dateTimeId);
        return groupService.getAllFreeGroups(dateTime);
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Add a new group to the university database")
    public Group createGroup(@Validated(OnCreate.class) @RequestBody Group group) {
        return groupService.addGroup(group);
    }

    @PutMapping
    @Operation(summary = "Update existing group")
    public Group updateGroup(@Validated(OnUpdate.class) @RequestBody Group group) {
        return groupService.updateGroup(group);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete group by ID", 
    description = "Removing only groups without students and lessons. Deletion groups with students or lessons is impossible")
    public Group deleteGroup(@PathVariable("id") long id) {
        Group group = groupService.getGroupById(id);
        groupService.removeGroup(group);
        return group;
    }
}
