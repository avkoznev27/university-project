package com.foxminded.university.controllers.rest;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnUpdate;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/courses")
@Tag(name = "Courses", description = "Manage university courses")
public class CoursesRestController {

    private CourseService courseService;

    @Autowired
    public CoursesRestController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping
    @Operation(summary = "Get list of all university courses")
    public List<Course> getAllCourses() {
        return courseService.getAllCourses();
    }
    
    @GetMapping("/count")
    @Operation(summary = "Get the number of university courses")
    public long getCount() {
        return courseService.countCourses();
    }
    
    @GetMapping("/{id}")
    @Operation(summary = "Get course by ID")
    public Course getCourse(@PathVariable("id") long id) {
        return courseService.getCourseById(id);
    }
    
    @GetMapping("/{id}/lessons")
    @Operation(summary = "Get course lessons by course ID")
    public List<Lesson> getCourseLessons(@PathVariable("id") long id) {
        return courseService.getCourseById(id).getLessons();
    }
    
    @GetMapping("/{id}/students")
    @Operation(summary = "Get all students on the course by course ID")
    public List<Student> getCourseStudents(@PathVariable("id") long id) {
        return courseService.getCourseById(id).getStudents();
    }
    
    @GetMapping("/{id}/teachers")
    @Operation(summary = "Get all teachers of the course by course ID")
    public List<Teacher> getCourseTeachers(@PathVariable("id") long id) {
        return courseService.getCourseById(id).getTeachers();
    }
    
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Add a new course to the university database")
    public Course createCourse(@Validated(OnCreate.class) @RequestBody Course course) {
        return courseService.addCourse(course);
    }

    @PutMapping
    @Operation(summary = "Update existing course")
    public Course updateCourse(@Validated(OnUpdate.class) @RequestBody Course course) {
        return courseService.updateCourse(course);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "Delete course by ID", 
    description = "Removing only courses without lessons. Deletion courses with lessons is impossible")
    public Course deleteCourse(@PathVariable("id") long id) {
        Course course = courseService.getCourseById(id);
        courseService.removeCourse(course);
        return course;
    }
}
