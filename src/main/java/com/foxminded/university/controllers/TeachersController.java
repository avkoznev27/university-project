package com.foxminded.university.controllers;

import java.time.LocalDate;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.service.TeacherService;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnDelete;
import com.foxminded.university.domain.validators.OnUpdate;

@Controller
@RequestMapping("/teachers")
@SessionAttributes({ "teacher","allCourses", "startDay", "endDay" })
public class TeachersController {

    private TeacherService teacherService;
    private CourseService courseService;

    @Autowired
    public TeachersController(TeacherService teacherService, CourseService courseService) {
        this.teacherService = teacherService;
        this.courseService = courseService;
    }

    @GetMapping("/teachers-list")
    public Model getAllTeachers(Model model, SessionStatus sessionStatus) {
        model.addAttribute("teachers", teacherService.getAllTeachers());
        sessionStatus.setComplete();
        return model;
    }

    @ModelAttribute("teacher")
    public Teacher teacher() {
        return new Teacher();
    }

    @GetMapping("/new")
    public Teacher newTeacher(@ModelAttribute("teacher") Teacher teacher, Model model) {
        model.addAttribute("allCourses", courseService.getAllCourses());
        return teacher;
    }

    @PostMapping("/new")
    public String createTeacher(@Validated(OnCreate.class) @ModelAttribute("teacher") Teacher teacher,
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "teachers/new";
        } else {
            teacherService.addTeacher(teacher);
            redirectAttributes.addFlashAttribute("success",
                    String.format("Teacher %s added succesfully", teacher.getName()));
            return "redirect:/teachers/teachers-list";
        }
    }

    @GetMapping("/{id}")
    public String showTeacher(@PathVariable("id") long id, Model model) {
        Teacher teacher = teacherService.getTeacherById(id);
        model.addAttribute("teacher", teacher);
        model.addAttribute("allCourses", courseService.getAllCourses());
        return "teachers/teacher-page";
    }

    @PatchMapping("/{id}")
    public String update(@Validated(OnUpdate.class) @ModelAttribute("teacher") Teacher updatedTeacher,
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "teachers/teacher-page";
        } else {
            teacherService.updateTeacher(updatedTeacher);
            redirectAttributes.addFlashAttribute("success",
                    String.format("Teacher with id = %d updated succesfully", updatedTeacher.getTeacherId()));
            return "redirect:/teachers/teachers-list";
        }
    }

    @DeleteMapping("/{id}")
    public String delete(@Validated(OnDelete.class) @ModelAttribute("teacher") Teacher teacher,
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "teachers/teacher-page";
        } else {
            teacherService.removeTeacher(teacher);
            redirectAttributes.addFlashAttribute("success",
                    String.format("Teacher %s removed succesfully", teacher.getName()));
            return "redirect:/teachers/teachers-list";
        }
    }

    @PostMapping("/teacher-timetable")
    public String createTeacherTimetable(@RequestParam("teacherId") int teacherId,
                                         @RequestParam("startDay") String startDay, 
                                         @RequestParam(required = false, value = "endDay") String endDay,
                                         Model model, HttpSession session) {
        Teacher teacher = teacherService.getTeacherById(teacherId);
        model.addAttribute("teacher", teacher).addAttribute("startDay", startDay).addAttribute("endDay", endDay);
        session.setAttribute("startDay", startDay);
        session.setAttribute("endDay", endDay);
        if (endDay.isEmpty() || startDay.equals(endDay)) {
            LocalDate dateStartDay = LocalDate.parse(startDay);
            model.addAttribute("teacherTimetable", teacherService.getTeacherTimetable(teacher, dateStartDay));
            model.addAttribute("dailyTimetable", true);
        } else {
            LocalDate dateStartDay = LocalDate.parse(startDay);
            LocalDate dateEndDay = LocalDate.parse(endDay);
            model.addAttribute("teacherTimetable",
                    teacherService.getTeacherTimetable(teacher, dateStartDay, dateEndDay));
            model.addAttribute("dailyTimetable", false);
        }
        return "teachers/teacher-timetable";
    }
}
