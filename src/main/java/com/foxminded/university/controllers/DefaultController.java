package com.foxminded.university.controllers;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.foxminded.university.exceptions.ServiceException;

@ControllerAdvice(assignableTypes = { AuditoriumsController.class, CoursesController.class, 
        GroupsController.class, SettingsController.class, StudentsController.class, 
        TeachersController.class, TimetableController.class, UniversityController.class })
public class DefaultController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultController.class);

    @ExceptionHandler(value = { ServiceException.class })
    public String handleServiceException(ServiceException ex, HttpServletRequest request, RedirectAttributes redirectAttributes, Model model) {
        LOGGER.debug("Handling ServiceException: {}", ex.getMessage());
        String message = ex.getMessage();
        LOGGER.debug("Adding redirect attribute serviceException = [{}]", message);
        redirectAttributes.addFlashAttribute("serviceException", message);
        String url = request.getHeader("referer");
        LOGGER.debug("Redirecting to url: {}", url);
        return "redirect:" + url;
    }
}
