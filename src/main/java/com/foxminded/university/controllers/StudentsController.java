package com.foxminded.university.controllers;

import java.time.LocalDate;
import java.util.List;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.StudentService;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnUpdate;

@Controller
@RequestMapping("/students")
@SessionAttributes({"student", "allCourses", "incompleteGroups", "studentLessons", "startDay", "endDay"})
public class StudentsController {
    
    private StudentService studentService;
    private GroupService groupService;
    private CourseService courseService;
    
    @Autowired
    public StudentsController(StudentService studentService, 
                              GroupService groupService, 
                              CourseService courseService) {
        this.studentService = studentService;
        this.groupService = groupService;
        this.courseService = courseService;
    }
    
    @GetMapping("/students-list")
    public List<Student> getAllStudents(Model model, SessionStatus sessionStatus) {
        List<Student> students = studentService.getAllStudents();
        model.addAttribute("students", students);
        sessionStatus.setComplete();
        return students;
    }
    
    @ModelAttribute("student")
    public Student student() {
        return new Student();
    }
    
    @GetMapping("/new")
    public Student newStudent(@ModelAttribute("student") Student student, Model model) {
        model.addAttribute("incompleteGroups", groupService.getAllIncompleteGroups());
        model.addAttribute("allCourses", courseService.getAllCourses());
        return student;
    }
    
    @PostMapping("/new")
    public String createStudent(@Validated(OnCreate.class) @ModelAttribute("student") Student student, 
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "students/new";
        } else {
            Group group = groupService.getGroupById(student.getGroupId());
            studentService.addStudent(group, student);
            redirectAttributes.addFlashAttribute("success", String.format("Student %s added succesfully", student.getName()));
            return "redirect:/students/students-list";
        }
    }
    
    @GetMapping("/{id}")
    public String showStudent(@PathVariable("id") long id, Model model) {
        Student student = studentService.getStudentById(id);
        model.addAttribute("student", student);
        model.addAttribute("incompleteGroups", groupService.getAllIncompleteGroups());
        model.addAttribute("allCourses", courseService.getAllCourses());
        model.addAttribute("studentLessons", groupService.getGroupById(student.getGroupId()).getLessons());
        return "students/student-page";
    }
    
    @PatchMapping("/{id}")
    public String updateStudent(@Validated(OnUpdate.class) @ModelAttribute("student") Student updatedStudent, 
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            System.out.println(bindingResult);
            return "students/student-page";
        } else {
            studentService.updateStudent(updatedStudent);
            redirectAttributes.addFlashAttribute("success", String.format("Student with id = %d updated succesfully", updatedStudent.getStudentId()));
            return "redirect:/students/students-list";   
        }
    }
    
    @DeleteMapping("/{id}")
    public String deleteStudent(@ModelAttribute("student") Student student, RedirectAttributes redirectAttributes) {
        studentService.removeStudent(student);
        redirectAttributes.addFlashAttribute("success", String.format("Student %s removed succesfully", student.getName()));
        return "redirect:/students/students-list";
    }
    
    @PostMapping("/student-timetable")
    public String createStudentTimetable(@RequestParam("studentId") int studentId, 
                                         @RequestParam("startDay") String startDay, 
                                         @RequestParam(required = false, value = "endDay") String endDay, 
                                         Model model, HttpSession session) {
        Student student = studentService.getStudentById(studentId);
        model.addAttribute("student", student)
             .addAttribute("startDay", startDay)
             .addAttribute("endDay", endDay);
        session.setAttribute("startDay", startDay);
        session.setAttribute("endDay", endDay);
        LocalDate dateStartDay = LocalDate.parse(startDay);
        if (endDay.isEmpty() || startDay.equals(endDay)) {
            model.addAttribute("studentTimetable", studentService.getStudentTimetable(student, dateStartDay));
            model.addAttribute("dailyTimetable", true);
        } else {
            LocalDate dateEndDay = LocalDate.parse(endDay);
            model.addAttribute("studentTimetable", studentService.getStudentTimetable(student, dateStartDay, dateEndDay));
            model.addAttribute("dailyTimetable", false);
        }
        return "students/student-timetable";
    }
}
