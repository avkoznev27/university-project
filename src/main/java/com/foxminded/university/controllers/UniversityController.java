package com.foxminded.university.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.foxminded.university.domain.service.AuditoriumService;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.StudentService;
import com.foxminded.university.domain.service.TeacherService;

@Controller
@RequestMapping("/")
public class UniversityController {
    
    private AuditoriumService auditoriumService;
    private StudentService studentService;
    private GroupService groupService;
    private CourseService courseService;
    private TeacherService teacherService;
    
    @Autowired
    public UniversityController(AuditoriumService auditoriumService, 
                           StudentService studentService,
                           GroupService groupService,
                           CourseService courseService,
                           TeacherService teacherService) {
        this.auditoriumService = auditoriumService;
        this.studentService = studentService;
        this.groupService = groupService;
        this.courseService = courseService;
        this.teacherService = teacherService;
    }
    
    @GetMapping()
    public String start(Model model) {
        model.addAttribute("numberOfAuditoriums", auditoriumService.countAuditoriums());
        model.addAttribute("numberOfStudents", studentService.countStudents());
        model.addAttribute("numberOfGroups", groupService.countGroups());
        model.addAttribute("numberOfCourses", courseService.countCourses());
        model.addAttribute("numberOfTeachers", teacherService.countTeachers());
        return "university";
    }
}
