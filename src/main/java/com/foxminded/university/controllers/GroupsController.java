package com.foxminded.university.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnDelete;
import com.foxminded.university.domain.validators.OnUpdate;

@Controller
@RequestMapping("/groups")
@SessionAttributes("group")
public class GroupsController {

    private GroupService groupService;

    @Autowired
    public GroupsController(GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping("/groups-list")
    public List<Group> getAllGroups(Model model, SessionStatus sessionStatus) {
        List<Group> groups = groupService.getAllGroups();
        model.addAttribute("groups", groups);
        sessionStatus.setComplete();
        return groups;
    }

    @ModelAttribute("group")
    public Group group() {
        return new Group();
    }

    @GetMapping("/new")
    public Group newGroup(@ModelAttribute("group") Group group) {
        return group;
    }

    @PostMapping("/new")
    public String createGroup(@Validated(OnCreate.class) @ModelAttribute("group") Group group, BindingResult bindingResult,
            RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "groups/new";
        } else {
            groupService.addGroup(group);
            redirectAttributes.addFlashAttribute("success",
                    String.format("Group %s added successfully", group.getGroupName()));
            return "redirect:/groups/groups-list";
        }
    }

    @GetMapping("/{id}")
    public String showGroup(@PathVariable("id") long id, Model model) {
        Group group = groupService.getGroupById(id);
        model.addAttribute("group", group);
        return "groups/group-page";
    }

    @PatchMapping("/{id}")
    public String updateGroup(@Validated(OnUpdate.class) @ModelAttribute("group") Group group, BindingResult bindingResult,
            RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "groups/group-page";
        } else {
            groupService.updateGroup(group);
            redirectAttributes.addFlashAttribute("success",
                    String.format("Group with id = %d updated successfully", group.getGroupId()));
            return "redirect:/groups/groups-list";
        }
    }

    @DeleteMapping("/{id}")
    public String deleteGroup(@Validated(OnDelete.class) @ModelAttribute("group") Group group, BindingResult bindingResult,
            RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "groups/group-page";
        } else {
            groupService.removeGroup(group);
            redirectAttributes.addFlashAttribute("success",
                    String.format("Group %s removed successfully", group.getGroupName()));
            return "redirect:/groups/groups-list";
        }
    }
}
