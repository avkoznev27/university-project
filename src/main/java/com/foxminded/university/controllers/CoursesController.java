package com.foxminded.university.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnDelete;
import com.foxminded.university.domain.validators.OnUpdate;

@Controller
@RequestMapping("/courses")
@SessionAttributes("course")
public class CoursesController {

    private CourseService courseService;
    
    @Autowired
    public CoursesController(CourseService courseService) {
        this.courseService = courseService;
    }
    
    @GetMapping("/courses-list")
    public List<Course> getAllCourses(Model model, SessionStatus sessionStatus) {
        List<Course> courses = courseService.getAllCourses();
        model.addAttribute("courses", courses);
        sessionStatus.setComplete();
        return courses;
    }
    
    @ModelAttribute("course")
    public Course course() {
        return new Course();
    }
    
    @GetMapping("/new")
    public Course newCourse(@ModelAttribute("course") Course course) {
        return course;
    }

    @PostMapping("/new")
    public String create(@Validated(OnCreate.class) @ModelAttribute("course") Course course,
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "courses/new";
        } else {
            courseService.addCourse(course);
            redirectAttributes.addFlashAttribute("success",
                    String.format("Course %s added successfully", course.getCourseName()));
            return "redirect:/courses/courses-list";
        }
    }
    
    @GetMapping("/{id}")
    public String showCourse(@PathVariable("id") long id, Model model) {
        Course course = courseService.getCourseById(id);
        model.addAttribute("course", course);
        return "courses/course-page";
    }
    
    @PatchMapping("/{id}")
    public String update(@Validated(OnUpdate.class) @ModelAttribute("course") Course course, 
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "courses/course-page";
        } else {
            courseService.updateCourse(course);
            redirectAttributes.addFlashAttribute("success", String.format("Course with id = %d updated successfully", 
                    course.getCourseId()));
            return "redirect:/courses/courses-list"; 
        }
    }
    
    @DeleteMapping("/{id}")
    public String delete(@Validated(OnDelete.class) @ModelAttribute("course") Course course, 
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "courses/course-page";
        } else {
            courseService.removeCourse(course);
            redirectAttributes.addFlashAttribute("success", String.format("Course %s removed successfully", 
                    course.getCourseName()));
            return "redirect:/courses/courses-list";
        }
    }
}
