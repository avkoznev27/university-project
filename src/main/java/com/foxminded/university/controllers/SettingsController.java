package com.foxminded.university.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.foxminded.university.domain.model.Settings;
import com.foxminded.university.domain.service.SettingsService;

@Controller
@RequestMapping("/")
public class SettingsController {

    private SettingsService settingsService;
    
    @Autowired
    public SettingsController(SettingsService settingsService) {
        this.settingsService = settingsService;
    }
    
    @GetMapping("/settings-page")
    public String showSettings(Model model) {
        model.addAttribute("settings", settingsService.getSettings());
        return "settings";
    }
    
    @PostMapping("/settings-page/set-default")
    public String setDefault(Model model) {
        model.addAttribute("settings", settingsService.setDefaultSettings());
        return "settings";   
    }
    
    @PatchMapping("/settings-page/save")
    public String saveSettings(@Valid @ModelAttribute Settings settings, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "settings";
        } else {
            settingsService.updateSettings(settings);
            return "settings";
        }
    }
}

