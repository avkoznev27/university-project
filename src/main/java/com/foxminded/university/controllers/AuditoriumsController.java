package com.foxminded.university.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.service.AuditoriumService;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnDelete;
import com.foxminded.university.domain.validators.OnUpdate;

@Controller
@RequestMapping("/auditoriums")
@SessionAttributes("auditorium")
public class AuditoriumsController {
    
    private AuditoriumService auditoriumService;
    
    @Autowired
    public AuditoriumsController(AuditoriumService auditoriumService) {
        this.auditoriumService = auditoriumService;
    }
    
    @GetMapping("/auditoriums-list")
    public List<Auditorium> getAllAuditoriums(Model model, SessionStatus sessionStatus) {
        List<Auditorium> auditoriums =  auditoriumService.getAllAuditoriums();
        model.addAttribute("auditoriums", auditoriums);
        sessionStatus.setComplete();
        return auditoriums;
    }
    
    @ModelAttribute("auditorium")
    public Auditorium auditorium() {
        return new Auditorium();
    }
    
    @GetMapping("/new")
    public Auditorium newAuditorium(@ModelAttribute("auditorium") Auditorium auditorium) {
        return auditorium;
    }

    @PostMapping("/new")
    public String createAuditorium(@Validated(OnCreate.class) @ModelAttribute("auditorium") Auditorium auditorium, 
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "auditoriums/new";
        } else {
            auditoriumService.addAuditorium(auditorium);
            redirectAttributes.addFlashAttribute("success", String.format("Auditorium with room number %d added successfully",
                    auditorium.getRoomNumber()));
            return "redirect:/auditoriums/auditoriums-list";
        }
    }
    
    @GetMapping("/{id}")
    public String showAuditorium(@PathVariable("id") long id, Model model) {
        Auditorium auditorium = auditoriumService.getAuditoriumById(id);
        model.addAttribute("auditorium", auditorium);
        return "auditoriums/auditorium-page";
    }
    
    @PatchMapping("/{id}")
    public String updateAuditorium(@Validated(OnUpdate.class) @ModelAttribute("auditorium") Auditorium auditorium, 
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "auditoriums/auditorium-page";
        } else {
            auditoriumService.updateAuditorium(auditorium);
            redirectAttributes.addFlashAttribute("success", String.format("Auditorium with id %d updated successfully",
                    auditorium.getAuditoriumId()));
            return "redirect:/auditoriums/auditoriums-list"; 
        }
    }
    
    @DeleteMapping("/{id}")
    public String deleteAuditorium(@Validated(OnDelete.class) @ModelAttribute("auditorium") Auditorium auditorium, 
            BindingResult bindingResult, RedirectAttributes redirectAttributes) {
        if (bindingResult.hasErrors()) {
            return "auditoriums/auditorium-page";
        } else {
            auditoriumService.removeAuditorium(auditorium);
            redirectAttributes.addFlashAttribute("success", String.format("Auditorium with room number %d removed successfully",
                    auditorium.getRoomNumber()));
            return "redirect:/auditoriums/auditoriums-list";
        }
    }
}
