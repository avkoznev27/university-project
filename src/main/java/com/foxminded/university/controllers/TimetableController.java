package com.foxminded.university.controllers;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.AuditoriumService;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.service.DateTimeService;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.LessonService;
import com.foxminded.university.domain.service.TeacherService;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnDelete;
import com.foxminded.university.domain.validators.OnUpdate;

@Controller
@RequestMapping("/timetable")
@SessionAttributes({ "dateTime" })
public class TimetableController {
    
    private LessonService lessonService;
    private DateTimeService dateTimeService;
    private AuditoriumService auditoriumService;
    private GroupService groupService;
    private CourseService courseService;
    private TeacherService teacherService;
    
    @Autowired
    public TimetableController(LessonService lessonService, 
                             DateTimeService dateTimeService, 
                             AuditoriumService auditoriumService, 
                             GroupService groupService,
                             CourseService courseService,
                             TeacherService teacherService) {
        this.lessonService = lessonService;
        this.dateTimeService = dateTimeService;
        this.auditoriumService = auditoriumService;
        this.groupService = groupService;
        this.courseService = courseService;
        this.teacherService = teacherService;
    }

    @GetMapping({ "/timetable", "/timetable/{id}" })
    public String getAllLessons(Model model, SessionStatus sessionStatus) {
        List<Lesson> lessons = lessonService.getAllLessons();
        List<Group> allGroups = groupService.getAllGroups();
        List<DateTime> allDateTimes = dateTimeService.getAllDateTimes();
        Set<LocalDate> allDates = dateTimeService.getAllDates();
        Map<String, List<DateTime>> allTimesForAllDates = allDates.stream()
                .collect(Collectors.toMap(date -> "for" + date.format(DateTimeFormatter.ofPattern("yyyyMMdd")),
                        date -> allDateTimes.stream()
                                .filter(dateTime -> dateTime.getLessonDate().equals(date))
                                .collect(Collectors.toList())));
        Map<String, List<DateTime>> allTimesForAllGroups = allGroups.stream()
                .collect(Collectors.toMap(group -> "group" + group.getGroupId(),
                        group -> group.getLessons().stream()
                                .map(Lesson::getDateTime)
                                .collect(Collectors.toList())));
        model.addAttribute("allLessons", lessons)
             .addAttribute("allGroups", allGroups)
             .addAttribute("allDates", allDates)
             .addAllAttributes(allTimesForAllDates)
             .addAllAttributes(allTimesForAllGroups);
        sessionStatus.setComplete();
        return "timetable/timetable";
    }
    
    @GetMapping("/new/{dateTimeId}/{groupId}")
    public String newLesson(@PathVariable("dateTimeId") long dateTimeId, 
                            @PathVariable("groupId") long groupId, 
                            @ModelAttribute("lesson") Lesson lesson, 
                            Model model) {
        model.addAttribute("disableApplyButton", false);
        model.addAttribute("disableSaveButton", true);
        DateTime dateTime = dateTimeService.getById(dateTimeId);
        Group group = groupService.getGroupById(groupId);
        lesson.setDateTime(dateTime);
        lesson.setGroup(group);
        List<Auditorium> allFreeAuditoriums = auditoriumService.getAllFreeAuditoriums(dateTime, group.getStudents().size());
        List<Course> allCourses = courseService.getAllCourses();
        List<Teacher> allBusyTeachers = teacherService.getAllBusyTeachers(dateTime);
        if(allBusyTeachers.size() == teacherService.countTeachers()) {
            model.addAttribute("noTeachersAtAll", "There are no available teachers at all");
        }
        if(courseIsSelected(lesson.getCourse())) {
            List<Teacher> availableTeachers = teacherService.getAllFreeTeachers(dateTime, lesson.getCourse());
            if (availableTeachers.isEmpty()) {
                model.addAttribute("noTeachers", "There are no available teachers for this course at this time");
            } else {
                model.addAttribute("availableTeachers", availableTeachers);
                model.addAttribute("disableApplyButton", true);
                model.addAttribute("disableSaveButton", false);
            }
        }
        model.addAttribute("freeAuditoriums", allFreeAuditoriums);
        model.addAttribute("allCourses", allCourses)
             .addAttribute("dayOfLesson", dateTime.getLessonDate());
        return "/timetable/new";
    }
    
    @PostMapping("/new")
    public String createLesson(@ModelAttribute("lesson") Lesson lesson, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        if (!courseIsSelected(lesson.getCourse()) ||
            !auditoriumIsSelected(lesson.getAuditorium()) || 
            !teacherIsSelected(lesson.getTeacher())) {
            redirectAttributes.addFlashAttribute("lesson", lesson);
            String redirectingUrl =  request.getHeader("referer");
            return "redirect:" + redirectingUrl;
        } else {
            lessonService.addLesson(lesson);
            String successMessage = String.format("Lesson for group %s on the course %s created successfully", 
                    lesson.getGroup().getGroupName(), lesson.getCourse().getCourseName());
            redirectAttributes.addFlashAttribute("success", successMessage);
            return "redirect:/timetable/timetable";
        }
    }
    
    @ModelAttribute("dateTime")
    public DateTime dateTime() {
        return new DateTime();
    }
    
    @GetMapping("/new-time")
    public String newTime(@ModelAttribute("dateTime") DateTime dateTime) {
        return "timetable/new-time";
    }
    
    @PostMapping("/new-time")
    public String createTime(@Validated(OnCreate.class) @ModelAttribute("dateTime") DateTime dateTime, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) {
            return "timetable/new-time";
        } else {
            dateTimeService.addDateTime(dateTime);
            return "redirect:/timetable/timetable";
        }
    }
    
    @GetMapping("/edit-time/{id}")
    public String showTime(@PathVariable("id") long id, Model model) {
        DateTime dateTime = dateTimeService.getById(id);
        model.addAttribute("dateTime", dateTime);
        return "timetable/edit-time";
    }
    
    @PatchMapping("/edit-time/{id}")
    public String editTime(@Validated(OnUpdate.class) @ModelAttribute("dateTime") DateTime dateTime, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "timetable/edit-time";
        } else {
            dateTimeService.updateDateTime(dateTime);
            return "redirect:/timetable/timetable";
        }
    }
    
    @DeleteMapping("/edit-time/{dateId}")
    public String deleteTime(@Validated(OnDelete.class) @ModelAttribute("dateTime") DateTime dateTime, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "timetable/edit-time";
        } else {
            dateTimeService.removeDateTime(dateTime);
            return "redirect:/timetable/timetable";
        }
    }
    
    @DeleteMapping("/timetable/{id}")
    public String deleteLesson(@PathVariable("id") long id, RedirectAttributes redirectAttributes) {
        Lesson lesson = lessonService.getLessonById(id);
        lessonService.removeLesson(lesson);
        String successMessage = String.format("Lesson for group %s on the course %s removed successfully", 
                lesson.getGroup().getGroupName(), lesson.getCourse().getCourseName());
        redirectAttributes.addFlashAttribute("success", successMessage);
        return "redirect:/timetable/timetable";
    }
  
    private boolean courseIsSelected(Course course) {
        return course != null && course.getCourseId() > 0;
    }
      
    private boolean teacherIsSelected(Teacher teacher) {
        return teacher != null && teacher.getTeacherId() > 0;
    }
    
    private boolean auditoriumIsSelected(Auditorium auditorium) {
        return auditorium != null && auditorium.getAuditoriumId() > 0;
    }
}
