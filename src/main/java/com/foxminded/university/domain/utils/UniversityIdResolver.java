package com.foxminded.university.domain.utils;

import com.fasterxml.jackson.annotation.ObjectIdGenerator.IdKey;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.ObjectIdGenerator;
import com.fasterxml.jackson.annotation.ObjectIdResolver;
import com.fasterxml.jackson.annotation.SimpleObjectIdResolver;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.service.LessonService;
import com.foxminded.university.domain.service.StudentService;

public class UniversityIdResolver extends SimpleObjectIdResolver {

    private LessonService lessonService;
    private StudentService studentService;

    @Autowired
    public UniversityIdResolver(LessonService lessonService, StudentService studentService) {
        this.lessonService = lessonService;
        this.studentService = studentService;
    }

    @Override
    public void bindItem(IdKey id, Object ob) {
        if (_items == null) {
            _items = new HashMap<ObjectIdGenerator.IdKey, Object>();
        } else if (_items.containsKey(id)) {
            return;
        }
        _items.put(id, ob);
    }

    @Override
    public Object resolveId(IdKey id) {
        Object resolved = super.resolveId(id);
        if (resolved == null) {
            if (id.scope == Lesson.class) {
                resolved = lessonService.getLessonById((Long) id.key);
                bindItem(id, resolved);
            }
            if (id.scope == Student.class) {
                resolved = studentService.getStudentById((Long) id.key);
                bindItem(id, resolved);
            }
        }
        return resolved;
    }

    @Override
    public ObjectIdResolver newForDeserialization(Object context) {
        return new UniversityIdResolver(lessonService, studentService);
    }
}
