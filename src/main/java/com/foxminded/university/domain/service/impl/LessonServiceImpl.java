package com.foxminded.university.domain.service.impl;

import static org.springframework.util.Assert.notNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.foxminded.university.dao.LessonRepository;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.service.LessonService;
import com.foxminded.university.exceptions.ResourceNotFoundException;

@Service
@Transactional
public class LessonServiceImpl implements LessonService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LessonServiceImpl.class);

    private LessonRepository lessonRepository;

    public LessonServiceImpl(LessonRepository lessonRepository) {
        this.lessonRepository = lessonRepository;
    }

    @Override
    public Lesson addLesson(Lesson lesson) {
        LOGGER.debug("Adding [{}]", lesson);
        notNull(lesson, "Lesson is null");
        return lessonRepository.save(lesson);
    }


    @Override
    public Lesson getLessonById(long id) {
        LOGGER.debug("Getting lesson by id = {}", id);
        return lessonRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format("Lesson with id = %d doesn't exist", id)));
    }

    @Override
    public Lesson updateLesson(Lesson lesson) {
        LOGGER.debug("Updating [{}]", lesson);
        notNull(lesson, "Lesson is null");
        if (lessonRepository.existsById(lesson.getLessonId())) {
            return lessonRepository.save(lesson);
        } else {
            throw new ResourceNotFoundException(
                    String.format("Lesson with id = %d doesn't exist", lesson.getLessonId()));
        }
    }

    @Override
    public void removeLesson(Lesson lesson) {
        LOGGER.debug("Removing [{}]", lesson);
        notNull(lesson, "Lesson is null");
        lessonRepository.delete(lesson);
    }

    @Override
    public List<Lesson> getAllLessons() {
        LOGGER.debug("Getting all lessons");
        return lessonRepository.findAll();
    }
}
