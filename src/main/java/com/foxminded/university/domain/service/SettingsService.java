package com.foxminded.university.domain.service;

import com.foxminded.university.domain.model.Settings;

public interface SettingsService {

    Settings getSettings();

    Settings updateSettings(Settings settings);

    Settings setDefaultSettings();

}
