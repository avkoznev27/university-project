package com.foxminded.university.domain.service.impl;

import static org.springframework.util.Assert.notNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@Service
@Transactional
public class CourseServiceImpl implements CourseService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseServiceImpl.class);

    private CourseRepository courseRepository;

    public CourseServiceImpl(CourseRepository courseRepository) {
        this.courseRepository = courseRepository;
    }

    @Override
    public Course addCourse(Course course) {
        LOGGER.debug("Adding [{}]", course);
        notNull(course, "Course is null");
        return courseRepository.save(course);
    }

    @Override
    public Course getCourseById(long id) {
        return courseRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format("Course with id = %d doesn't exist", id)));
    }

    @Override
    public Course updateCourse(Course course) {
        LOGGER.debug("Updating [{}]", course);
        notNull(course, "Course is null");
        if (courseRepository.existsById(course.getCourseId())) {
           return courseRepository.save(course);    
        } else {
            throw new ResourceNotFoundException(
                    String.format("Course with id = %d doesn't exist", course.getCourseId()));
        }  
    }

    @Override
    public void removeCourse(Course course) {
        LOGGER.debug("Removing [{}]", course);
        notNull(course, "Course is null");
        if (course.getLessons().isEmpty()) {
            courseRepository.delete(course);
        } else {
            throw new ServiceException("Deletion is not possible. Course has lessons."
                    + "First remove lessons");
        }
    }

    @Override
    public List<Course> getAllCourses() {
        LOGGER.debug("Getting all courses");
        return courseRepository.findAll();
    }

    @Override
    public long countCourses() {
        LOGGER.debug("Counting courses in data base");
        return courseRepository.count();
    }
}
