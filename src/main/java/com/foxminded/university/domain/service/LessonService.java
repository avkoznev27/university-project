package com.foxminded.university.domain.service;

import java.util.List;

import com.foxminded.university.domain.model.Lesson;

public interface LessonService {

    Lesson addLesson(Lesson lesson);
    
    Lesson getLessonById(long id);

    Lesson updateLesson(Lesson lesson);

    void removeLesson(Lesson lesson);

    List<Lesson> getAllLessons();
    
}
