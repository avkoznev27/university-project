package com.foxminded.university.domain.service.impl;

import static org.springframework.util.Assert.notNull;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.foxminded.university.dao.DateTimeRepository;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.service.DateTimeService;
import com.foxminded.university.domain.service.SettingsService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@Service
@Transactional
public class DateTimeServiceImpl implements DateTimeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DateTimeServiceImpl.class);

    private DateTimeRepository dateTimeRepository;
    private SettingsService settingsService;

    public DateTimeServiceImpl(DateTimeRepository dateTimeRepository, SettingsService settingsService) {
        this.dateTimeRepository = dateTimeRepository;
        this.settingsService =  settingsService;
    }

    @Override
    public DateTime addDateTime(DateTime dateTime) {
        LOGGER.debug("Adding [{}]", dateTime);
        notNull(dateTime, "DateTime is null");
        checkDuration(dateTime);
        return dateTimeRepository.save(dateTime);
    }

    @Override
    public DateTime updateDateTime(DateTime dateTime) {
        LOGGER.debug("Updating [{}]", dateTime);
        notNull(dateTime, "DateTime is null");
        if (dateTimeRepository.existsById(dateTime.getDateTimeId())) {
            checkDuration(dateTime);
            return dateTimeRepository.save(dateTime);    
        } else {
            throw new ResourceNotFoundException(
                    String.format("DateTime with id = %d doesn't exist", dateTime.getDateTimeId()));
        }
        
    }

    @Override
    public void removeDateTime(DateTime dateTime) {
        LOGGER.debug("Removing [{}]", dateTime);
        notNull(dateTime, "DateTime is null");
        if (dateTime.getLessons().isEmpty()) {
            dateTimeRepository.delete(dateTime);    
        } else {
            throw new ServiceException("Deletion is not possible. DateTime has lessons."
                    + "First remove lessons");
        }
    }

    @Override
    public DateTime getById(long id) {
        return dateTimeRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format("DateTime with id = %d doesn't exist", id)));
    }

    @Override
    public List<DateTime> getAllDateTimes() {
        LOGGER.debug("Getting all date times");
        return dateTimeRepository.findAll();
    }

    @Override
    public Set<LocalDate> getAllDates() {
        LOGGER.debug("Getting all dates");
        return dateTimeRepository.findAll().stream().map(DateTime::getLessonDate)
                .collect(Collectors.toCollection(TreeSet::new));
    }

    private void checkDuration(DateTime dateTime) {
        LOGGER.debug("Checking lesson duration for [{}]", dateTime);
        int actualDuration = (int) Duration.between(dateTime.getStartLesson(), dateTime.getEndLesson()).toMinutes();
        int expectedDuration = settingsService.getSettings().getLessonDuration();
        if (actualDuration != expectedDuration) {
            LOGGER.warn("Wrong lesson duration. Expected lesson duration in minutes = {}, actual: [{}]",
                    expectedDuration, actualDuration);
            throw new ServiceException(
                    String.format("Wrong lesson duration. Expected lesson duration in minutes = %s, actual: %s",
                            expectedDuration, actualDuration));
        }
    }
}
