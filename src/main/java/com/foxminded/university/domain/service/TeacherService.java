package com.foxminded.university.domain.service;

import java.time.LocalDate;
import java.util.List;

import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;

public interface TeacherService {
    
    Teacher addTeacher(Teacher teacher);
    
    Teacher updateTeacher(Teacher teacher);

    void removeTeacher(Teacher teacher);

    Teacher getTeacherById(long teacherId);

    List<Teacher> getAllTeachers();
    
    List<Teacher> getAllBusyTeachers(DateTime dateTime);

    List<Teacher> getAllFreeTeachers(DateTime dateTime, Course course);
    
    List<Lesson> getTeacherTimetable(Teacher teacher, LocalDate day);

    List<Lesson> getTeacherTimetable(Teacher teacher, LocalDate startDay, LocalDate endDay);
    
    long countTeachers();
}
