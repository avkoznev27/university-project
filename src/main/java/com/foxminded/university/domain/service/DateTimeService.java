package com.foxminded.university.domain.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

import com.foxminded.university.domain.model.DateTime;

public interface DateTimeService {

    DateTime addDateTime(DateTime dateTime);

    DateTime updateDateTime(DateTime dateTime);

    void removeDateTime(DateTime dateTime);
    
    DateTime getById(long id);

    List<DateTime> getAllDateTimes();
    
    Set<LocalDate> getAllDates();

}
