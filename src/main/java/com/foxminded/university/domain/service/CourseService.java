package com.foxminded.university.domain.service;

import java.util.List;

import com.foxminded.university.domain.model.Course;

public interface CourseService {
    
    Course addCourse(Course course);
    
    Course getCourseById(long id);
    
    Course updateCourse(Course course);
    
    void removeCourse(Course course);
    
    List<Course> getAllCourses();
    
    long countCourses();
}
