package com.foxminded.university.domain.service.impl;

import static java.util.stream.Collectors.toList;
import static org.springframework.util.Assert.notNull;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.foxminded.university.dao.StudentRepository;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.SettingsService;
import com.foxminded.university.domain.service.StudentService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentServiceImpl.class);

    private StudentRepository studentRepository;
    private GroupService groupService;
    private SettingsService settingsService;

    public StudentServiceImpl(StudentRepository studentRepository,
                              GroupService groupService,
                              SettingsService settingsService) {
        this.studentRepository = studentRepository;
        this.groupService = groupService;
        this.settingsService = settingsService;
    }

    @Override
    public Student addStudent(Group group, Student student) {
        LOGGER.debug("Adding student [{}] to group [{}]", student, group);
        notNull(group, "Group is null");
        notNull(student, "Student is null");
        if (group.getStudents().size() < settingsService.getSettings().getMaxGroupSize()) {
            student.setGroupId(group.getGroupId());
            group.getStudents().add(student);
            return studentRepository.save(student);
        } else {
            LOGGER.warn("Group is full. Unable to add student, group size [{}]", group.getStudents().size());
            throw new ServiceException("Group is full. Unable to add student");
        }
    }

    @Override
    public Student getStudentById(long studentId) {
        LOGGER.debug("Getting student by id = [{}]", studentId);
        return studentRepository.findById(studentId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format("Student with id = %d doesn't exist", studentId)));
    }

    @Override
    public List<Student> getAllStudents() {
        LOGGER.debug("Getting all students");
        return studentRepository.findAll();
    }

    @Override
    public Student updateStudent(Student updatedStudent) {
        LOGGER.debug("Updating [{}]", updatedStudent);
        notNull(updatedStudent, "Student is null");
        if (studentRepository.existsById(updatedStudent.getStudentId())) {
            return studentRepository.save(updatedStudent);
        } else {
            throw new ResourceNotFoundException(
                    String.format("Student with id = %d doesn't exist", updatedStudent.getStudentId()));
        }
    }

    @Override
    public void removeStudent(Student student) {
        LOGGER.debug("Removing [{}]", student);
        notNull(student, "Student is null");
        studentRepository.delete(student);
    }
    
    @Override
    public List<Lesson> getStudentTimetable(Student student, LocalDate day) {
        LOGGER.debug("Getting timetable for student [{}] for day[{}]", student, day);
        notNull(student, "Student is null");
        notNull(day, "Day is null");
        return groupService.getGroupById(student.getGroupId()).getLessons().stream()
                .filter(lesson -> lesson.getDateTime().getLessonDate().equals(day)).collect(toList());
    }

    @Override
    public List<Lesson> getStudentTimetable(Student student, LocalDate startDay, LocalDate endDay) {
        LOGGER.debug("Getting timetable for student [{}] for period from [{}] to [{}]", student, startDay, endDay);
        notNull(student, "Student is null");
        notNull(startDay, "Start day is null");
        notNull(endDay, "End day is null");
        if (endDay.isBefore(startDay)) {
            throw new ServiceException("Wrong period");
        }
        return groupService.getGroupById(student.getGroupId()).getLessons().stream()
                .filter(lesson -> {
                            LocalDate lessonDate = lesson.getDateTime().getLessonDate();
                            return lessonDate.equals(startDay) || lessonDate.equals(endDay)
                                    || (lessonDate.isAfter(startDay) && lessonDate.isBefore(endDay));
              }).collect(toList());
    }

    @Override
    public long countStudents() {
        LOGGER.debug("Counting students in data base");
        return studentRepository.count();
    }
}
