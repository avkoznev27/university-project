package com.foxminded.university.domain.service.impl;

import static org.springframework.util.Assert.notNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.foxminded.university.dao.SettingsRepository;
import com.foxminded.university.domain.model.Settings;
import com.foxminded.university.domain.service.SettingsService;

@Service
@Transactional
public class SettingsServiceImpl implements SettingsService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(SettingsServiceImpl.class);
    
    @Value("${service.max.group.size}")
    private String maxGroupSize;
    @Value("${service.lesson.duration}")
    private String lessonDuration;
    
    private SettingsRepository settingsRepository;
    
    @Autowired
    public SettingsServiceImpl(SettingsRepository settingsRepository) {
        this.settingsRepository = settingsRepository;
    }
    
    @Override
    public Settings getSettings() {
        LOGGER.debug("Getting actual settings");
        return settingsRepository.findById(1L).orElse(defaultSettings());
    }
    
    @Override
    public Settings setDefaultSettings() {
        LOGGER.debug("Setting default settings");
        return settingsRepository.save(defaultSettings());
    }
    
    @Override
    public Settings updateSettings(Settings settings) {
        LOGGER.debug("Updating settings, {}", settings);
        notNull(settings, "Settings cannot be null");
        settings.setId(1L);
        return settingsRepository.save(settings);
    }
    
    private Settings defaultSettings() {
        Settings defaultSettings = new Settings();
        defaultSettings.setId(1L);
        defaultSettings.setMaxGroupSize(Integer.valueOf(maxGroupSize));
        defaultSettings.setLessonDuration(Integer.valueOf(lessonDuration));
        return defaultSettings;
    }
}
