package com.foxminded.university.domain.service;

import java.util.List;

import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;

public interface GroupService {

    Group addGroup(Group group);

    Group updateGroup(Group group);

    void removeGroup(Group group);

    Group getGroupById(long groupId);

    List<Group> getAllGroups();

    List<Group> getAllFreeGroups(DateTime dateTime);

    List<Group> getAllIncompleteGroups();
    
    long countGroups();
}
