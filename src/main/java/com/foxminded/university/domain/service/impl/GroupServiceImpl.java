package com.foxminded.university.domain.service.impl;

import static java.util.stream.Collectors.toList;
import static org.springframework.util.Assert.notNull;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.dao.LessonRepository;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.SettingsService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@Service
@Transactional
public class GroupServiceImpl implements GroupService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GroupServiceImpl.class);

    private GroupRepository groupRepository;
    private LessonRepository lessonRepository;
    private SettingsService settingsService;

    @Autowired
    public GroupServiceImpl(GroupRepository groupRepository, LessonRepository lessonRepository, SettingsService settingsService) {
        this.groupRepository = groupRepository;
        this.lessonRepository = lessonRepository;
        this.settingsService = settingsService;
    }

    @Override
    public Group addGroup(Group group) {
        LOGGER.debug("Adding [{}]", group);
        notNull(group, "Group is null");
        return groupRepository.save(group);
    }

    @Override
    public Group updateGroup(Group group) {
        LOGGER.debug("Updating [{}]", group);
        notNull(group, "Group is null");
        if (groupRepository.existsById(group.getGroupId())) {
            return groupRepository.save(group);
        } else {
            throw new ResourceNotFoundException(
                    String.format("Group with id = %d doesn't exist", group.getGroupId()));
        }
    }

    @Override
    public void removeGroup(Group group) {
        LOGGER.debug("Removing [{}]", group);
        notNull(group, "Group is null");
        if (!group.getStudents().isEmpty()) {
            throw new ServiceException("Deletion is not possible. Group has students. First remove students");
        } else if (!group.getLessons().isEmpty()) {
            throw new ServiceException("Deletion is not possible. Group has lessons. First remove lessons");
        } else {
            groupRepository.delete(group);   
        }
    }

    @Override
    public Group getGroupById(long groupId) {
        LOGGER.debug("Getting group by id = [{}]", groupId);
        Group group = groupRepository.findById(groupId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format("Group with id = %d doesn't exist", groupId)));
        group.getStudents().sort((s1, s2) -> s1.getName().compareTo(s2.getName()));
        return group;
    }

    @Override
    public List<Group> getAllGroups() {
        LOGGER.debug("Getting all groups from database");
        return groupRepository.findAll();
    }

    @Override
    public List<Group> getAllFreeGroups(DateTime dateTime) {
        LOGGER.debug("Getting all free groups for [{}]", dateTime);
        notNull(dateTime, "DateTime is null");
        List<Group> allBusyGroupsThisTime = lessonRepository.findAll().stream()
                .filter(lesson -> lesson.getDateTime().equals(dateTime)).map(Lesson::getGroup).collect(toList());
        LOGGER.debug("Found {} groups busy this time", allBusyGroupsThisTime.size());
        List<Group> allFreeGroups = groupRepository.findAll().stream()
                .filter(group -> !allBusyGroupsThisTime.contains(group)).collect(toList());
        LOGGER.debug("Found {} groups free this time", allFreeGroups.size());
        return allFreeGroups;
    }

    @Override
    public List<Group> getAllIncompleteGroups() {
        int maxGroupSize = settingsService.getSettings().getMaxGroupSize();
        LOGGER.debug("Getting all groups no larger than [{}] students", maxGroupSize);
        return getAllGroups().stream().filter(group -> group.getStudents().size() < maxGroupSize)
                .collect(toList());
    }

    @Override
    public long countGroups() {
        LOGGER.debug("Counting groups in data base");
        return groupRepository.count();
    }
}
