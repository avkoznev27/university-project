package com.foxminded.university.domain.service;

import java.time.LocalDate;
import java.util.List;

import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Student;

public interface StudentService {

    Student addStudent(Group group, Student student);

    Student getStudentById(long studentId);

    List<Student> getAllStudents();

    Student updateStudent(Student student);

    void removeStudent(Student student);
    
    List<Lesson> getStudentTimetable(Student student, LocalDate day);

    List<Lesson> getStudentTimetable(Student student, LocalDate startDay, LocalDate endDay);
    
    long countStudents();

}
