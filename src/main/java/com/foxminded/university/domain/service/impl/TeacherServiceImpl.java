package com.foxminded.university.domain.service.impl;

import static java.util.stream.Collectors.toList;
import static org.springframework.util.Assert.notNull;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.EntityNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.dao.LessonRepository;
import com.foxminded.university.dao.TeacherRepository;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.TeacherService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@Service
@Transactional
public class TeacherServiceImpl implements TeacherService {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeacherServiceImpl.class);

    private TeacherRepository teacherRepository;
    private LessonRepository lessonRepository;
    private CourseRepository courseRepository;

    public TeacherServiceImpl(TeacherRepository teacherRepository, LessonRepository lessonRepository, CourseRepository courseRepository) {
        this.teacherRepository = teacherRepository;
        this.lessonRepository = lessonRepository;
        this.courseRepository = courseRepository;
    }

    @Override
    public Teacher addTeacher(Teacher teacher) {
        LOGGER.debug("Adding [{}]", teacher);
        notNull(teacher, "Teacher is null");
        return teacherRepository.save(teacher);
    }

    @Override
    public Teacher getTeacherById(long teacherId) {
        LOGGER.debug("Getting teacher by id = [{}]", teacherId);
        return teacherRepository.findById(teacherId)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format("Teacher with id = %d doesn't exist", teacherId)));
    }

    @Override
    public List<Teacher> getAllTeachers() {
        LOGGER.debug("Getting all teachers");
        return teacherRepository.findAll();
    }

    @Override
    public Teacher updateTeacher(Teacher teacher) {
        LOGGER.debug("Updating [{}]", teacher);
        notNull(teacher, "Teacher is null");
        if (teacherRepository.existsById(teacher.getTeacherId())) {
            return teacherRepository.save(teacher);
        } else {
            throw new ResourceNotFoundException(
                    String.format("Teacher with id = %d doesn't exist", teacher.getTeacherId()));
        }  
    }

    @Override
    public void removeTeacher(Teacher teacher) {
        LOGGER.debug("Removing [{}]", teacher);
        notNull(teacher, "Teacher is null");
        if (teacher.getLessons().isEmpty()) {
            teacherRepository.delete(teacher);    
        } else {
            throw new ServiceException("Deletion is not possible. Teacher has lessons."
                    + "First remove lessons");
        }
    }

    @Override
    public List<Teacher> getAllBusyTeachers(DateTime dateTime) {
        LOGGER.debug("Getting all busy teachers");
        notNull(dateTime, "DateTime is null");
        return lessonRepository.findAll().stream().filter(lesson -> lesson.getDateTime().equals(dateTime))
                .map(Lesson::getTeacher).collect(toList());
    }

    @Override
    public List<Teacher> getAllFreeTeachers(DateTime dateTime, Course course) {
        LOGGER.debug("Getting all free teachers for [{}] with course [{}]", dateTime, course);
        notNull(dateTime, "DateTime is null");
        notNull(course, "Course is null");
        List<Teacher> allBusyTeachersThisTime = getAllBusyTeachers(dateTime);
        LOGGER.debug("Found {} busy teachers this time. Teachers: {}", allBusyTeachersThisTime.size(),
                allBusyTeachersThisTime);
        try {
            List<Teacher> allFreeTeachers = courseRepository.getOne(course.getCourseId()).getTeachers().stream()
                    .filter(teacher -> !allBusyTeachersThisTime.contains(teacher)).collect(toList());
            LOGGER.debug("Found {} free teachers this time. Teachers: {}", allFreeTeachers.size(), allFreeTeachers);
            return allFreeTeachers;
        } catch (EntityNotFoundException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
    
    @Override
    public List<Lesson> getTeacherTimetable(Teacher teacher, LocalDate day) {
        LOGGER.debug("Getting timetable for teacher [{}] for day[{}]", teacher, day);
        notNull(teacher, "Teacher is null");
        notNull(day, "Day is null");
        return teacher.getLessons().stream()
                .filter(lesson -> lesson.getDateTime().getLessonDate().equals(day)).collect(toList());
    }

    @Override
    public List<Lesson> getTeacherTimetable(Teacher teacher, LocalDate startDay, LocalDate endDay) {
        LOGGER.debug("Getting timetable for teacher [{}] for period from [{}] to [{}]", teacher, startDay, endDay);
        notNull(teacher, "Teacher is null");
        notNull(startDay, "Start day is null");
        notNull(endDay, "End day is null");
        if (endDay.isBefore(startDay)) {
            throw new ServiceException("Wrong period");
        }
        return teacher.getLessons().stream().filter(lesson -> {
            LocalDate lessonDate = lesson.getDateTime().getLessonDate();
            return lessonDate.equals(startDay) || lessonDate.equals(endDay)
                    || (lessonDate.isAfter(startDay) && lessonDate.isBefore(endDay));
        }).collect(toList());
    }

    @Override
    public long countTeachers() {
        LOGGER.debug("Counting teachers in data base");
        return teacherRepository.count();
    }
}
