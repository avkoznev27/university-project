package com.foxminded.university.domain.service;

import java.util.List;

import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.DateTime;

public interface AuditoriumService {
    
    Auditorium addAuditorium(Auditorium auditorium);
    
    Auditorium getAuditoriumById(long id);
    
    Auditorium updateAuditorium(Auditorium auditorium);
    
    void removeAuditorium(Auditorium auditorium);
    
    List<Auditorium> getAllAuditoriums();
    
    List<Auditorium> getAllFreeAuditoriums(DateTime dateTime);
    
    List<Auditorium> getAllFreeAuditoriums(DateTime dateTime, int capacity);
    
    long countAuditoriums();
    
}
