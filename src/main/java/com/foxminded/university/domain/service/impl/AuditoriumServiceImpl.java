package com.foxminded.university.domain.service.impl;

import static java.util.stream.Collectors.toList;
import static org.springframework.util.Assert.notNull;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.foxminded.university.dao.AuditoriumRepository;
import com.foxminded.university.dao.LessonRepository;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.service.AuditoriumService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@Service
@Transactional
public class AuditoriumServiceImpl implements AuditoriumService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuditoriumServiceImpl.class);

    private AuditoriumRepository auditoriumRepository;
    private LessonRepository lessonRepository;

    @Autowired
    public AuditoriumServiceImpl(AuditoriumRepository auditoriumRepository, LessonRepository lessonRepository) {
        this.auditoriumRepository = auditoriumRepository;
        this.lessonRepository = lessonRepository;
    }

    @Override
    public Auditorium addAuditorium(Auditorium auditorium) {
        LOGGER.debug("Adding {}", auditorium);
        notNull(auditorium, "Auditorium is null");
        return auditoriumRepository.save(auditorium);
    }

    @Override
    public Auditorium getAuditoriumById(long id) {
        LOGGER.debug("Getting auditorium by id = {}", id);
        return auditoriumRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(
                        String.format("Auditorium with id = %d doesn't exist", id)));
    }

    @Override
    public Auditorium updateAuditorium(Auditorium auditorium) {
        LOGGER.debug("Updating [{}]", auditorium);
        notNull(auditorium, "Auditorium is null");
        if (auditoriumRepository.existsById(auditorium.getAuditoriumId())) {
            return auditoriumRepository.save(auditorium);
        } else {
            throw new ResourceNotFoundException(
                    String.format("Auditorium with id = %d doesn't exist", auditorium.getAuditoriumId()));
        }
    }

    @Override
    public void removeAuditorium(Auditorium auditorium) {
        LOGGER.debug("Removing [{}]", auditorium);
        notNull(auditorium, "Auditorium is null");
        if (auditorium.getLessons().isEmpty()) {
            auditoriumRepository.delete(auditorium);
        } else {
            throw new ServiceException("Deletion is not possible. Auditorium has lessons. "
                    + "First remove lessons or change auditorium for lessons");
        }
    }

    @Override
    public List<Auditorium> getAllAuditoriums() {
        LOGGER.debug("Getting all auditoriums from database");
        return auditoriumRepository.findAll();
    }

    @Override
    public List<Auditorium> getAllFreeAuditoriums(DateTime dateTime) {
        LOGGER.debug("Getting all free auditoriums for {}", dateTime);
        notNull(dateTime, "DateTime is null");
        List<Auditorium> allBusyAuditoriumsThisTime = lessonRepository.findAll().stream()
                .filter(lesson -> lesson.getDateTime().getDateTimeId() == dateTime.getDateTimeId())
                .map(Lesson::getAuditorium).collect(toList());
        return auditoriumRepository.findAll().stream()
                .filter(auditorium -> !allBusyAuditoriumsThisTime.contains(auditorium)).collect(toList());
    }

    @Override
    public List<Auditorium> getAllFreeAuditoriums(DateTime dateTime, int capacity) {
        LOGGER.debug("Getting all free auditoriums for [{}] with capacity = [{}]", dateTime, capacity);
        notNull(dateTime, "DateTime is null");
        return getAllFreeAuditoriums(dateTime).stream().filter(auditorium -> auditorium.getCapacity() >= capacity)
                .collect(toList());
    }

    @Override
    public long countAuditoriums() {
        LOGGER.debug("Counting auditoriums in data base");
        return auditoriumRepository.count();
    }
}
