package com.foxminded.university.domain.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.foxminded.university.dao.AuditoriumRepository;
import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.dao.DateTimeRepository;
import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;

public class CheckExistingValidator implements ConstraintValidator<CheckExisting, Object> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CheckExistingValidator.class);
    
    private AuditoriumRepository auditoriumRepository;
    private CourseRepository courseRepository;
    private GroupRepository groupRepository;
    private DateTimeRepository dateTimeRepository;
    
    @Autowired
    public CheckExistingValidator(
            AuditoriumRepository auditoriumRepository, 
            CourseRepository courseRepository,
            GroupRepository groupRepository,
            DateTimeRepository dateTimeRepository) {
        this.auditoriumRepository = auditoriumRepository;
        this.courseRepository = courseRepository;
        this.groupRepository = groupRepository;
        this.dateTimeRepository = dateTimeRepository;
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
        if (object == null) {
            return false;
        } else if (object instanceof Auditorium) {  
            return !isExistingAuditorium((Auditorium) object);
        } else if (object instanceof Course) {
            return !isExistingCourse((Course) object);
        } else if (object instanceof Group) {
            return !isExistingGroup((Group) object);
        } else if (object instanceof DateTime) {
            return !isExistingDateTime((DateTime) object);
        } else return false;
    }
    
    private boolean isExistingAuditorium(Auditorium auditorium) {
        LOGGER.debug("Check existing {}", auditorium);
        Auditorium persistAuditorium = auditoriumRepository.getByRoomNumber(auditorium.getRoomNumber());
        return persistAuditorium != null && persistAuditorium.getAuditoriumId() != auditorium.getAuditoriumId();
    }
    
    private boolean isExistingCourse(Course course) {
        LOGGER.debug("Check existing {}", course);
        Course persistCourse = courseRepository.getByCourseName(course.getCourseName());
        return persistCourse != null && persistCourse.getCourseId() != course.getCourseId(); 
    }
    
    private boolean isExistingGroup(Group group) {
        LOGGER.debug("Check existing {}", group);
        Group persistGroup = groupRepository.getByGroupName(group.getGroupName());
        return persistGroup != null && persistGroup.getGroupId() != group.getGroupId();
    }
    
    private boolean isExistingDateTime(DateTime dateTime) {
        LOGGER.debug("Check existing {}", dateTime);
        return dateTimeRepository.findAllByLessonDate(dateTime.getLessonDate()).stream().anyMatch(existingDateTime -> {
            if (dateTime.equals(existingDateTime)) {
                return true;
            } else if (dateTime.getStartLesson().isBefore(existingDateTime.getEndLesson()) 
                    && dateTime.getStartLesson().isAfter(existingDateTime.getStartLesson())) {
                return true;
            } else {
                return dateTime.getEndLesson().isBefore(existingDateTime.getEndLesson()) 
                        && dateTime.getEndLesson().isAfter(existingDateTime.getStartLesson());
            }
        });
    }
}
