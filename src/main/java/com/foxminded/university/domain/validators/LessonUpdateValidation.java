package com.foxminded.university.domain.validators;

import javax.validation.GroupSequence;
import javax.validation.groups.Default;

@GroupSequence({ Default.class, OnUpdate.class, OnUpdateLesson.class })
public interface LessonUpdateValidation {}
