package com.foxminded.university.domain.validators;

import java.util.Optional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.service.AuditoriumService;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.service.DateTimeService;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.LessonService;
import com.foxminded.university.domain.service.TeacherService;

public class LessonValidator implements ConstraintValidator<ValidLesson, Lesson> {
    
    private GroupService groupService;
    private AuditoriumService auditoriumService;
    private TeacherService teacherService;
    private CourseService courseService;
    private DateTimeService dateTimeService;
    private LessonService lessonService;

    public LessonValidator(GroupService groupService, 
                            AuditoriumService auditoriumService,
                            TeacherService teacherService, 
                            CourseService courseService, 
                            DateTimeService dateTimeService,
                            LessonService lessonService) {
        this.groupService = groupService;
        this.auditoriumService = auditoriumService;
        this.teacherService = teacherService;
        this.courseService = courseService;
        this.dateTimeService = dateTimeService;
        this.lessonService = lessonService;
    }

    @Override
    public boolean isValid(Lesson lesson, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        boolean isValid = true;
        Lesson checkedLesson = getCheckedLesson(lesson);
        Optional<Lesson> groupLesson = checkLessonGroup(checkedLesson);
        Optional<Lesson> auditoriumLesson = checkLessonAuditorium(checkedLesson);
        Optional<Lesson> teacherLesson = checkLessonTeacher(checkedLesson);
        
        if (groupLesson.isPresent()) {
            context.buildConstraintViolationWithTemplate(String.format("At this time the group %s is at another lesson(id = %d)", 
                            checkedLesson.getGroup().getGroupName(), groupLesson.get().getLessonId()))
            .addPropertyNode("group").addConstraintViolation();
            isValid = false;
        }
        
        if (auditoriumLesson.isPresent()) {
            context.buildConstraintViolationWithTemplate(String.format("At this time in the auditorium %s another lesson(id = %d)",
                    checkedLesson.getAuditorium().getRoomNumber(), auditoriumLesson.get().getLessonId()))
            .addPropertyNode("auditorium").addConstraintViolation();
            isValid = false;
        }
        
        if (teacherLesson.isPresent()) {
            context.buildConstraintViolationWithTemplate(String.format("At this time teacher %s has another lesson(id = %d)", 
                    checkedLesson.getTeacher().getName(), teacherLesson.get().getLessonId()))
            .addPropertyNode("teacher").addPropertyNode("lessons").addConstraintViolation();
            isValid = false;
        }
        
        if (!checkedLesson.getTeacher().getCourses().contains(checkedLesson.getCourse())) {
            context.buildConstraintViolationWithTemplate("The teacher does not know this course")
            .addPropertyNode("teacher").addPropertyNode("courses").addConstraintViolation();
            isValid = false;
        }
        return isValid;
    }

    private Lesson getCheckedLesson(Lesson lesson) {
        if (lesson.getLessonId() > 0) {
            return lessonService.getLessonById(lesson.getLessonId());
        } else {
            Lesson checkedLesson = new Lesson();
            checkedLesson.setAuditorium(auditoriumService.getAuditoriumById(lesson.getAuditorium().getAuditoriumId()));
            checkedLesson.setCourse(courseService.getCourseById(lesson.getCourse().getCourseId()));
            checkedLesson.setDateTime(dateTimeService.getById(lesson.getDateTime().getDateTimeId()));
            checkedLesson.setGroup(groupService.getGroupById(lesson.getGroup().getGroupId()));
            checkedLesson.setTeacher(teacherService.getTeacherById(lesson.getTeacher().getTeacherId()));
            return checkedLesson;  
        }
    }
    
    private Optional<Lesson> checkLessonGroup(Lesson lesson) {
        return lesson.getGroup().getLessons().stream()
                .filter(groupLesson -> groupLesson.getDateTime().equals(lesson.getDateTime())
                        && groupLesson.getLessonId() != lesson.getLessonId())
                .findFirst();
    }
    
    private Optional<Lesson> checkLessonAuditorium(Lesson lesson) {
        return lesson.getAuditorium().getLessons().stream()
                .filter(auditoriumLesson -> auditoriumLesson.getDateTime().equals(lesson.getDateTime())
                        && auditoriumLesson.getLessonId() != lesson.getLessonId())
                .findFirst();
    }
    
    private Optional<Lesson> checkLessonTeacher(Lesson lesson) {
        return lesson.getTeacher().getLessons().stream()
                .filter(teacherLesson -> teacherLesson.getDateTime().equals(lesson.getDateTime())
                        && teacherLesson.getLessonId() != lesson.getLessonId())
                .findFirst();
    }
}
