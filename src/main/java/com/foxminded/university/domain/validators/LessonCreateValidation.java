package com.foxminded.university.domain.validators;

import javax.validation.GroupSequence;
import javax.validation.groups.Default;

@GroupSequence({ Default.class, OnCreate.class, OnCreateLesson.class })
public interface LessonCreateValidation {}
