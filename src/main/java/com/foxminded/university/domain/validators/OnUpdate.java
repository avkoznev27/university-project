package com.foxminded.university.domain.validators;

import javax.validation.groups.Default;

public interface OnUpdate extends Default {}
