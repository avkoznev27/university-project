package com.foxminded.university.domain.validators;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy = LessonValidator.class)
@Retention(RUNTIME)
@Target(TYPE)
public @interface ValidLesson {
    
    String message() default "";
    
    Class<?>[] groups() default {};
  
    Class<? extends Payload>[] payload() default {};
    
}
