package com.foxminded.university.domain.validators;

import javax.validation.groups.Default;

public interface OnDelete extends Default {}
