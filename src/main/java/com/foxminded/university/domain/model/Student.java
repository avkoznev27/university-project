package com.foxminded.university.domain.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.foxminded.university.domain.utils.UniversityIdResolver;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnUpdate;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

@Entity
@Table(name = "students")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "studentId", 
                  resolver = UniversityIdResolver.class, scope = Student.class)
@Schema(description = "Student entity")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NegativeOrZero(groups = OnCreate.class, message = "Student ID for creating must be zero")
    @Positive(groups = OnUpdate.class, message = "Student ID for updating must be greater than 0")
    @Schema(example = "1")
    private long studentId;
    
    @NotBlank(message = "Student name must not be empty")
    @Schema(example = "Vasya")
    private String name;
    
    @Column(name = "group_id")
    @Positive(message = "Wrong group or no group selected")
    @Schema(example = "1")
    private long groupId;
    
    @ManyToOne
    @JoinColumn(name = "group_id", insertable = false, updatable = false)
    @Schema(accessMode = AccessMode.READ_ONLY)
    private Group group;
    
    @ManyToMany
    @JoinTable(
        name = "students_courses", 
        joinColumns = { @JoinColumn(name = "student_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "course_id") }
    )
    @LazyCollection(LazyCollectionOption.FALSE)
    @Schema(description = "List of all student courses")
    private Set<Course> courses;

    public Student(long groupId, String name) {
        this.groupId = groupId;
        this.name = name;
        this.courses = new HashSet<>();
    }
    
    public Student(long studentId, long groupId, String name) {
        this.studentId = studentId;
        this.groupId = groupId;
        this.name = name;
        this.courses = new HashSet<>();
    }
    
    public Student(long studentId) {
        this.studentId = studentId;
        this.courses = new HashSet<>();
    }

    public Student() {
        this.courses = new HashSet<>();
    }

    public long getStudentId() {
        return studentId;
    }

    public long getGroupId() {
        return groupId;
    }

    public String getName() {
        return name;
    }

    public Set<Course> getCourses() {
        return courses;
    }
    
    public Group getGroup() {
        return group;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (groupId ^ (groupId >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + (int) (studentId ^ (studentId >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass())
            return false;
        Student other = (Student) obj;
        if (groupId != other.groupId)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (studentId != other.studentId)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Student [studentId=" + studentId 
                    + ", groupId=" + groupId 
                    + ", name=" + name 
                    + "]";
    }
}
