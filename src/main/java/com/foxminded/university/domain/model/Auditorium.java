package com.foxminded.university.domain.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.foxminded.university.domain.validators.CheckExisting;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnDelete;
import com.foxminded.university.domain.validators.OnUpdate;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

@Entity
@Table(name = "auditoriums")
@CheckExisting(groups = { OnCreate.class, OnUpdate.class }, message = "Auditorium with this room number already exist")
public class Auditorium {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NegativeOrZero(groups = OnCreate.class, message = "Auditorium ID for creating must be zero")
    @Positive(groups = OnUpdate.class, message = "Auditorium ID for updating must be greater than 0")
    @Schema(example = "1")
    private long auditoriumId;
    
    @Positive
    @Max (value = 999, message = "Room number cannot be more than 999")
    @Schema(example = "5")
    private int roomNumber;
    
    @Positive
    @Max(value = 200, message = "Capacity cannot be more than 200")
    @Schema(example = "30")
    private int capacity;
    
    @OneToMany(mappedBy = "auditorium")
    @LazyCollection(LazyCollectionOption.FALSE)
    @Size(groups = OnDelete.class, max = 0, message = "Deletion is not possible. Auditorium has lessons. "
            + "First remove lessons or change auditorium for lessons")
    @JsonIdentityReference(alwaysAsId = true)
    @Schema(description = "List of all lessons in the auditorium. In JSON list of lessons ids", 
    example = "[1, 2, 3]", accessMode = AccessMode.READ_ONLY)
    private List<Lesson> lessons;
   
    public Auditorium(int roomNumber, int capacity) {
        this.roomNumber = roomNumber;
        this.capacity = capacity;
        this.lessons = new ArrayList<>();
    }
    
    public Auditorium(long auditoriumId, int roomNumber, int capacity) {
        this.auditoriumId = auditoriumId;
        this.roomNumber = roomNumber;
        this.capacity = capacity;
        this.lessons = new ArrayList<>();
    }
    
    public Auditorium(long auditoriumId) {
        this.auditoriumId = auditoriumId;
        this.lessons = new ArrayList<>();
    }

    public Auditorium() {}

    public long getAuditoriumId() {
        return auditoriumId;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public int getCapacity() {
        return capacity;
    }
    
    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setAuditoriumId(long auditoriumId) {
        this.auditoriumId = auditoriumId;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (auditoriumId ^ (auditoriumId >>> 32));
        result = prime * result + capacity;
        result = prime * result + roomNumber;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass())
            return false;
        Auditorium other = (Auditorium) obj;
        if (auditoriumId != other.auditoriumId)
            return false;
        if (capacity != other.capacity)
            return false;
        return roomNumber == other.roomNumber;
    }

    @Override
    public String toString() {
        return "Auditorium [auditoriumId=" + auditoriumId 
                + ", roomNumber=" + roomNumber 
                + ", capacity=" + capacity + "]";
    }
}
