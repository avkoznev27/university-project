package com.foxminded.university.domain.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.foxminded.university.domain.validators.CheckExisting;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnDelete;
import com.foxminded.university.domain.validators.OnUpdate;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

@Entity
@Table(name = "courses")
@CheckExisting(groups = { OnCreate.class, OnUpdate.class }, message = "Course with the same name already exists")
public class Course {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NegativeOrZero(groups = OnCreate.class, message = "Course ID for creating must be zero")
    @Positive(groups = OnUpdate.class, message = "Course ID for updating must be greater than 0")
    @Schema(example = "1")
    private long courseId;
    
    @NotBlank(message = "Course name must not be empty")
    @Schema(example = "History")
    private String courseName;
    
    @NotBlank(message = "Course description must not be empty")
    @Schema(example = "History course description")
    private String courseDescription;

    @ManyToMany(mappedBy = "courses")
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    private List<Student> students;

    @ManyToMany(mappedBy = "courses")
    @LazyCollection(LazyCollectionOption.FALSE)
    @JsonIgnore
    private List<Teacher> teachers;

    @OneToMany(mappedBy = "course")
    @LazyCollection(LazyCollectionOption.FALSE)
    @Size(groups = OnDelete.class, max = 0, message = "Deletion is not possible. Course has lessons. First remove lessons")
    @JsonIdentityReference(alwaysAsId = true)
    @Schema(description = "List of all lessons for this course. In JSON list of lessons ids", 
    example = "[1, 2, 3]", accessMode = AccessMode.READ_ONLY)
    private List<Lesson> lessons;

    public Course(String courseName, String courseDescription) {
        this.courseName = courseName;
        this.courseDescription = courseDescription;
        this.students = new ArrayList<>();
        this.teachers = new ArrayList<>();
        this.lessons = new ArrayList<>();
    }

    public Course(long courseId, String courseName, String courseDescription) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.courseDescription = courseDescription;
        this.students = new ArrayList<>();
        this.teachers = new ArrayList<>();
        this.lessons = new ArrayList<>();
    }

    public Course(long courseId) {
        this.courseId = courseId;
        this.students = new ArrayList<>();
        this.teachers = new ArrayList<>();
        this.lessons = new ArrayList<>();
    }

    public Course() {
    }

    public long getCourseId() {
        return courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public List<Student> getStudents() {
        return students;
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setCourseId(long courseId) {
        this.courseId = courseId;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public void setCourseDescription(String courseDescription) {
        this.courseDescription = courseDescription;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void setTeachers(List<Teacher> teachers) {
        this.teachers = teachers;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (courseId ^ (courseId >>> 32));
        result = prime * result + ((courseName == null) ? 0 : courseName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass())
            return false;
        Course other = (Course) obj;
        if (courseId != other.courseId)
            return false;
        if (courseName == null) {
            if (other.courseName != null)
                return false;
        } else if (!courseName.equals(other.courseName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Course [courseId=" + courseId 
                + ", courseName=" + courseName 
                + ", courseDescription=" + courseDescription + "]";
    }
}
