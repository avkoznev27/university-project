package com.foxminded.university.domain.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Positive;

import io.swagger.v3.oas.annotations.media.Schema;

@Entity
@Table(name = "settings")
public class Settings {
    
    @Id
    @Schema(example = "1")
    private long id;
    
    @Positive
    @Schema(example = "60", defaultValue = "45")
    private int lessonDuration;
    
    @Positive
    @Schema(example = "50", defaultValue = "30")
    private int maxGroupSize;
    
    public Settings(long id, @Positive int lessonDuration, @Positive int maxGroupSize) {
        this.id = id;
        this.lessonDuration = lessonDuration;
        this.maxGroupSize = maxGroupSize;
    }

    public Settings() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getLessonDuration() {
        return lessonDuration;
    }

    public void setLessonDuration(int lessonDuration) {
        this.lessonDuration = lessonDuration;
    }

    public int getMaxGroupSize() {
        return maxGroupSize;
    }

    public void setMaxGroupSize(int maxGroupSize) {
        this.maxGroupSize = maxGroupSize;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        result = prime * result + lessonDuration;
        result = prime * result + maxGroupSize;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Settings other = (Settings) obj;
        if (id != other.id)
            return false;
        if (lessonDuration != other.lessonDuration)
            return false;
        if (maxGroupSize != other.maxGroupSize)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Settings [id=" + id 
                + ", lessonDuration=" + lessonDuration 
                + ", maxGroupSize=" + maxGroupSize + "]";
    }
}
