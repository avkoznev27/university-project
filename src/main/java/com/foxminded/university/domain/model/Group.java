package com.foxminded.university.domain.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.foxminded.university.domain.validators.CheckExisting;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnDelete;
import com.foxminded.university.domain.validators.OnUpdate;

import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

@Entity
@Table(name = "groups")
@CheckExisting(groups = { OnCreate.class, OnUpdate.class }, message = "Group with the same name already exists")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NegativeOrZero(groups = OnCreate.class, message = "Group ID for creating must be zero")
    @Positive(groups = OnUpdate.class, message = "Group ID for updating must be greater than 0")
    @Schema(example = "1")
    private long groupId;
    
    @NotBlank(message = "Group name must not be empty")
    @Schema(example = "GG-21")
    private String groupName;
    
    @OneToMany(mappedBy = "group")
    @LazyCollection(LazyCollectionOption.FALSE)
    @Size(groups = OnDelete.class, max = 0, message = "Deletion is not possible. Group has students. First remove students")
    @JsonIdentityReference(alwaysAsId = true)
    @ArraySchema(arraySchema = @Schema(description = "List of all students in this group. In JSON list of students ids", 
    example = "[1, 2, 3]", accessMode = AccessMode.READ_ONLY))
    private List<Student> students;
    
    @OneToMany(mappedBy = "group")
    @LazyCollection(LazyCollectionOption.FALSE)
    @Size(groups = OnDelete.class, max = 0, message = "Deletion is not possible. Group has lessons. First remove lessons")
    @JsonIdentityReference(alwaysAsId = true)
    @Schema(description = "List of all lessons for this group. In JSON list of lessons ids", 
    example = "[1, 2, 3]", accessMode = AccessMode.READ_ONLY)
    private List<Lesson> lessons;

    public Group(String groupName) {
        this.groupName = groupName;
        this.students = new ArrayList<>();
        this.lessons = new ArrayList<>();
    }
    
    public Group(long groupId, String groupName) {
        this.groupId = groupId;
        this.groupName = groupName;
        this.students = new ArrayList<>();
        this.lessons = new ArrayList<>();
    }
    
    public Group(long groupId) {
        this.groupId = groupId;
        this.students = new ArrayList<>();
        this.lessons = new ArrayList<>();
    }

    public Group() {
        this.students = new ArrayList<>();
        this.lessons = new ArrayList<>();
    }
    
    public long getGroupId() {
        return groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public List<Student> getStudents() {
        return students;
    }
    
    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (groupId ^ (groupId >>> 32));
        result = prime * result + ((groupName == null) ? 0 : groupName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass())
            return false;
        Group other = (Group) obj;
        if (groupId != other.groupId)
            return false;
        if (groupName == null) {
            if (other.groupName != null)
                return false;
        } else if (!groupName.equals(other.groupName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Group [groupId=" + groupId 
                + ", groupName=" + groupName
                + "]";
    }
}
