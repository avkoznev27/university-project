package com.foxminded.university.domain.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnDelete;
import com.foxminded.university.domain.validators.OnUpdate;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

@Entity
@Table(name = "teachers")
public class Teacher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NegativeOrZero(groups = OnCreate.class, message = "Teacher ID for creating must be zero")
    @Positive(groups = OnUpdate.class, message = "Teacher ID for updating must be greater than 0")
    @Schema(example = "1")
    private long teacherId;
    
    @NotBlank(message = "Teacher name must not be empty")
    @Schema(example = "Teacher")
    private String name;
    
    @ManyToMany
    @JoinTable(
        name = "teachers_courses", 
        joinColumns = { @JoinColumn(name = "teacher_id") }, 
        inverseJoinColumns = { @JoinColumn(name = "course_id") }
    )
    @LazyCollection(LazyCollectionOption.FALSE)
    @Schema(description = "List of all teacher courses")
    private Set<Course> courses;
    
    @OneToMany(mappedBy = "teacher")
    @LazyCollection(LazyCollectionOption.FALSE)
    @Size(groups = OnDelete.class, max = 0, message = "Deletion is not possible. Teacher has lessons. First remove lessons")
    @JsonIdentityReference(alwaysAsId = true)
    @Schema(description = "List of all teacher lessons. In JSON list of lessons ids", 
    example = "[1, 2, 3]", accessMode = AccessMode.READ_ONLY)
    private List<Lesson> lessons;

    public Teacher(String name) {
        this.name = name;
        this.courses = new HashSet<>();
        this.lessons = new ArrayList<>();
    }
    
    public Teacher(long teacherId, String name) {
        this.teacherId = teacherId;
        this.name = name;
        this.courses = new HashSet<>();
        this.lessons = new ArrayList<>();
    }
    
    public Teacher(long teacherId) {
        this.teacherId = teacherId;
        this.courses = new HashSet<>();
        this.lessons = new ArrayList<>();
    }

    public Teacher() {
        this.courses = new HashSet<>();
        this.lessons = new ArrayList<>();
    }

    public long getTeacherId() {
        return teacherId;
    }

    public String getName() {
        return name;
    }

    public Set<Course> getCourses() {
        return courses;
    }
    
    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setTeacherId(long teacherId) {
        this.teacherId = teacherId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCourses(Set<Course> courses) {
        this.courses = courses;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + (int) (teacherId ^ (teacherId >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass())
            return false;
        Teacher other = (Teacher) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (teacherId != other.teacherId)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Teacher [teacherId=" + teacherId 
                + ", name=" + name 
                + "]";
    }
}
