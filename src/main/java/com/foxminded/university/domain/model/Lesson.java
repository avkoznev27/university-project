package com.foxminded.university.domain.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.foxminded.university.domain.utils.UniversityIdResolver;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnCreateLesson;
import com.foxminded.university.domain.validators.OnUpdate;
import com.foxminded.university.domain.validators.OnUpdateLesson;
import com.foxminded.university.domain.validators.ValidLesson;

import io.swagger.v3.oas.annotations.media.Schema;

@Entity
@Table(name = "lessons")
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "lessonId", 
                  resolver = UniversityIdResolver.class, scope = Lesson.class)
@ValidLesson(groups = { OnCreateLesson.class, OnUpdateLesson.class })
public class Lesson {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NegativeOrZero(groups = OnCreate.class, message = "Lesson ID for creating must be zero")
    @Positive(groups = OnUpdate.class, message = "Lesson ID for updating must be greater than 0")
    @Schema(example = "1")
    private long lessonId;
    
    @ManyToOne
    @JoinColumn(name = "date_time_id")
    @NotNull
    private DateTime dateTime;
    
    @ManyToOne
    @JoinColumn(name = "group_id")
    @NotNull
    private Group group;
    
    @ManyToOne
    @JoinColumn(name = "auditorium_id")
    @NotNull
    private Auditorium auditorium;
    
    @ManyToOne
    @JoinColumn(name = "course_id")
    @NotNull
    private Course course;
    
    @ManyToOne
    @JoinColumn(name = "teacher_id")
    @NotNull
    private Teacher teacher;
    
    public long getLessonId() {
        return lessonId;
    }
    
    public Course getCourse() {
        return course;
    }
    
    public DateTime getDateTime() {
        return dateTime;
    }
    
    public Auditorium getAuditorium() {
        return auditorium;
    }
    
    public Teacher getTeacher() {
        return teacher;
    }
    
    public Group getGroup() {
        return group;
    }
    
    public void setLessonId(long lessonId) {
        this.lessonId = lessonId;
    }
    
    public void setCourse(Course course) {
        this.course = course;
    }
    
    public void setDateTime(DateTime dateTime) {
        this.dateTime = dateTime;
    }
    
    public void setAuditorium(Auditorium auditorium) {
        this.auditorium = auditorium;
    }
    
    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
    
    public void setGroup(Group group) {
        this.group = group;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((auditorium == null) ? 0 : auditorium.hashCode());
        result = prime * result + ((course == null) ? 0 : course.hashCode());
        result = prime * result + ((dateTime == null) ? 0 : dateTime.hashCode());
        result = prime * result + ((group == null) ? 0 : group.hashCode());
        result = prime * result + (int) (lessonId ^ (lessonId >>> 32));
        result = prime * result + ((teacher == null) ? 0 : teacher.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass())
            return false;
        Lesson other = (Lesson) obj;
        if (auditorium == null) {
            if (other.auditorium != null)
                return false;
        } else if (!auditorium.equals(other.auditorium))
            return false;
        if (course == null) {
            if (other.course != null)
                return false;
        } else if (!course.equals(other.course))
            return false;
        if (dateTime == null) {
            if (other.dateTime != null)
                return false;
        } else if (!dateTime.equals(other.dateTime))
            return false;
        if (group == null) {
            if (other.group != null)
                return false;
        } else if (!group.equals(other.group))
            return false;
        if (lessonId != other.lessonId)
            return false;
        if (teacher == null) {
            if (other.teacher != null)
                return false;
        } else if (!teacher.equals(other.teacher))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Lesson [lessonId=" + lessonId 
                + ", dateTime=" + dateTime 
                + ", group=" + group 
                + ", auditorium=" + auditorium 
                + ", course=" + course 
                + ", teacher=" + teacher + "]";
    }
}
