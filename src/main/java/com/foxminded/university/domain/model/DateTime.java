package com.foxminded.university.domain.model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NegativeOrZero;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.foxminded.university.domain.validators.CheckExisting;
import com.foxminded.university.domain.validators.OnCreate;
import com.foxminded.university.domain.validators.OnDelete;
import com.foxminded.university.domain.validators.OnUpdate;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;

@Entity
@Table(name = "date_times")
@CheckExisting(groups = { OnCreate.class, OnUpdate.class }, message = "Date time already exists")
@Schema(description = "Date and time of the lesson")
public class DateTime implements Comparable<DateTime> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NegativeOrZero(groups = OnCreate.class, message = "DateTime ID for creating must be zero")
    @Positive(groups = OnUpdate.class, message = "DateTime ID for updating must be greater than 0")
    @Schema(example = "1")
    private long dateTimeId;
    
    @NotNull(message = "Date must not be null")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate lessonDate;
    
    @NotNull(message = "Start lesson time must not be null")
    @DateTimeFormat(pattern = "HH:mm")
    @JsonFormat(pattern = "HH:mm")
    @Schema(implementation = String.class, format = "HH:mm", example = "08:00")
    private LocalTime startLesson;
    
    @NotNull(message = "End lesson time must not be null")
    @DateTimeFormat(pattern = "HH:mm")
    @JsonFormat(pattern = "HH:mm")
    @Schema(implementation = String.class, format = "HH:mm", example = "08:45")
    private LocalTime endLesson;
    
    @OneToMany(mappedBy = "dateTime")
    @LazyCollection(LazyCollectionOption.FALSE)
    @Size(groups = OnDelete.class, max = 0,
    message = "Deletion is not possible. Date time has lessons. First remove lessons")
    @JsonIdentityReference(alwaysAsId = true)
    @Schema(description = "List of all lessons for this date and time. In JSON list of lessons ids",
    example = "[1, 2, 3]", accessMode = AccessMode.READ_ONLY)
    private List<Lesson> lessons;
    
    public DateTime(LocalDate lessonDate, LocalTime startLesson, LocalTime endLesson) {
        this.lessonDate = lessonDate;
        this.startLesson = startLesson;
        this.endLesson = endLesson;
        this.lessons = new ArrayList<>();
    }
    
    public DateTime(long dateTimeId, @NotNull LocalDate lessonDate, @NotNull LocalTime startLesson,
            @NotNull LocalTime endLesson) {
        this.dateTimeId = dateTimeId;
        this.lessonDate = lessonDate;
        this.startLesson = startLesson;
        this.endLesson = endLesson;
        this.lessons = new ArrayList<>();
    }
    
    public DateTime(long dateTimeId) {
        this.dateTimeId = dateTimeId;
        this.lessons = new ArrayList<>();
    }

    public DateTime() {}

    public long getDateTimeId() {
        return dateTimeId;
    }

    public void setDateTimeId(long dateTimeId) {
        this.dateTimeId = dateTimeId;
    }

    public LocalDate getLessonDate() {
        return lessonDate;
    }

    public void setLessonDate(LocalDate lessonDate) {
        this.lessonDate = lessonDate;
    }

    public LocalTime getStartLesson() {
        return startLesson;
    }

    public void setStartLesson(LocalTime startLesson) {
        this.startLesson = startLesson;
    }

    public LocalTime getEndLesson() {
        return endLesson;
    }

    public void setEndLesson(LocalTime endLesson) {
        this.endLesson = endLesson;
    }

    public List<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(List<Lesson> lessons) {
        this.lessons = lessons;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((endLesson == null) ? 0 : endLesson.hashCode());
        result = prime * result + ((lessonDate == null) ? 0 : lessonDate.hashCode());
        result = prime * result + ((startLesson == null) ? 0 : startLesson.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass())
            return false;
        DateTime other = (DateTime) obj;
        if (endLesson == null) {
            if (other.endLesson != null)
                return false;
        } else if (!endLesson.equals(other.endLesson))
            return false;
        if (lessonDate == null) {
            if (other.lessonDate != null)
                return false;
        } else if (!lessonDate.equals(other.lessonDate))
            return false;
        if (startLesson == null) {
            if (other.startLesson != null)
                return false;
        } else if (!startLesson.equals(other.startLesson))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "DateTime [dateTimeId=" + dateTimeId 
                + ", lessonDate=" + lessonDate 
                + ", startLesson=" + startLesson
                + ", endLesson=" + endLesson + "]";
    }

    @Override
    public int compareTo(DateTime other) {
        int cmp = lessonDate.compareTo(other.getLessonDate());
        if (cmp == 0) {
            return startLesson.compareTo(other.getStartLesson());
        }
        return cmp;
    }
}
