package com.foxminded.university.domain.formatters;

import java.text.ParseException;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.Formatter;
import org.springframework.stereotype.Component;

import com.foxminded.university.domain.model.Course;

@Component
public class CourseFormatter implements Formatter<Course> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(CourseFormatter.class);
    private static final String COURSE_PATTERN = "ID=(\\d+)NAME=(.+)";

    @Override
    public String print(Course course, Locale locale) {
        LOGGER.trace("Printing course : {}", course);
        if(course == null) {
            return "";
        }
        String result = String.format("ID=%sNAME=%s", course.getCourseId(), course.getCourseName());
        LOGGER.trace("Printed course [{}]", result);
        return result;
    }

    @Override
    public Course parse(String text, Locale locale) throws ParseException {
        LOGGER.trace("Parsing course : {} with pattern [{}]", text, COURSE_PATTERN);
        Pattern pattern = Pattern.compile(COURSE_PATTERN);
        Matcher matcher = pattern.matcher(text);
        if (matcher.find()) {
            String courseId = matcher.group(1);
            String courseName = matcher.group(2);
            Course course = new Course();
            course.setCourseId(Integer.parseInt(courseId));
            course.setCourseName(courseName);
            LOGGER.trace("Parsed course: {}", course);
            return course;
        } else {
            LOGGER.trace("parsing failed, returning emty course");
            return new Course();
        }   
    }
}
