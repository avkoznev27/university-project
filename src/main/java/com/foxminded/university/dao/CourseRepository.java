package com.foxminded.university.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foxminded.university.domain.model.Course;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {

    Course getByCourseName(String courseName);
    
}
