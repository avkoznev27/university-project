package com.foxminded.university.dao;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foxminded.university.domain.model.DateTime;


@Repository
public interface DateTimeRepository extends JpaRepository<DateTime, Long> {

    List<DateTime> findAllByLessonDate(LocalDate localDate);
    
}
