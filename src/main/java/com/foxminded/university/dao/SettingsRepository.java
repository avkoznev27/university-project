package com.foxminded.university.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foxminded.university.domain.model.Settings;

@Repository
public interface SettingsRepository extends JpaRepository<Settings, Long> {
    
}
