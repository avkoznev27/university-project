package com.foxminded.university.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foxminded.university.domain.model.Student;

@Repository
public interface StudentRepository extends JpaRepository<Student, Long> {
    
}
