package com.foxminded.university.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.foxminded.university.domain.model.Group;

@Repository
public interface GroupRepository extends JpaRepository<Group, Long> {
    
    Group getByGroupName(String groupName);
    
}
