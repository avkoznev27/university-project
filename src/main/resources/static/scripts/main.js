
$(document).ready(function() {
    $("#counters-container").load("/university #counters", function() {
        $("#courses").append($("#courses-count"));
        $("#teachers").append($("#teachers-count"));
        $("#groups").append($("#groups-count"));
        $("#students").append($("#students-count"));
        $("#auditoriums").append($("#auditoriums-count"));
    });

    $(".clickable-row").on('click', function() {
        window.location = $(this).data('url');
    });
    
    $("input").on("input", function() {
        $("#save").prop("disabled", false);
    });
    
});
