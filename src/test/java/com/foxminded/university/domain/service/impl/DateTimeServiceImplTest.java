package com.foxminded.university.domain.service.impl;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.foxminded.university.dao.DateTimeRepository;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Settings;
import com.foxminded.university.domain.service.SettingsService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@ExtendWith(MockitoExtension.class)
class DateTimeServiceImplTest {
    
    @Mock
    private DateTimeRepository dateTimeRepository;
    
    @Mock
    private SettingsService settingsService;

    @InjectMocks
    private DateTimeServiceImpl dateTimeService;
    
    private DateTime existingDateTime;
    private DateTime testDateTime;
    private Settings defaultSettings;

    @BeforeEach
    void setUp() throws Exception {
        defaultSettings = new Settings(1, 45, 30);
        existingDateTime = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        testDateTime = new DateTime(10, LocalDate.of(2020, 9, 1), LocalTime.of(7, 0), LocalTime.of(7, 45));
    }

    @Test
    void testAddDateTimeShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> dateTimeService.addDateTime(null));
        assertEquals("DateTime is null", exception.getMessage());
    }
    
    @Test
    void testAddDateTimeShouldThrowServiceExceptionWhenWrongDurationDateTimePassed() {
        when(settingsService.getSettings()).thenReturn(defaultSettings);
        testDateTime = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 30));
        Throwable exception1 = assertThrows(ServiceException.class, () -> dateTimeService.addDateTime(testDateTime));
        assertEquals("Wrong lesson duration. Expected lesson duration in minutes = 45, actual: 30", exception1.getMessage());
        testDateTime = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(8, 30), LocalTime.of(8, 0));
        Throwable exception2 = assertThrows(ServiceException.class, () -> dateTimeService.addDateTime(testDateTime));
        assertEquals("Wrong lesson duration. Expected lesson duration in minutes = 45, actual: -30", exception2.getMessage());
        testDateTime = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 0));
        Throwable exception3 = assertThrows(ServiceException.class, () -> dateTimeService.addDateTime(testDateTime));
        assertEquals("Wrong lesson duration. Expected lesson duration in minutes = 45, actual: 0", exception3.getMessage());
    }
    
    @Test
    void testAddDateTimeShouldReturnDateTimeWhenCorrectDurationDateTimePassed() {
        testDateTime = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(7, 0), LocalTime.of(7, 45));
        when(dateTimeRepository.save(testDateTime)).thenReturn(testDateTime);
        when(settingsService.getSettings()).thenReturn(defaultSettings);
        assertEquals(testDateTime, dateTimeService.addDateTime(testDateTime));  
    }
    
    @Test
    void testGetDateTimeByIdShouldReturnDateTimeFromDataBaseByPassedId() {
        when(dateTimeRepository.findById(1L)).thenReturn(Optional.of(existingDateTime));
        assertEquals(existingDateTime, dateTimeService.getById(1));
    }
    
    @Test
    void testGetDateTimeShouldThrowServiceExceptionWhenDateTimeWithPassedIdDoesNotExist() {
        when(dateTimeRepository.findById(anyLong())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> dateTimeService.getById(10));
        assertEquals("DateTime with id = 10 doesn't exist", exception.getMessage());
    }

    @Test
    void testUpdateDateTimeShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> dateTimeService.updateDateTime(null));
        assertEquals("DateTime is null", exception.getMessage());
    }
    
    @Test
    void testUpdateDateTimeShouldThrowServiceExceptionWhenWrongDurationDateTimePassed() {
        when(settingsService.getSettings()).thenReturn(defaultSettings);
        testDateTime = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 30));
        Throwable exception1 = assertThrows(ServiceException.class, () -> dateTimeService.addDateTime(testDateTime));
        assertEquals("Wrong lesson duration. Expected lesson duration in minutes = 45, actual: 30", exception1.getMessage());
        testDateTime = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(8, 30), LocalTime.of(8, 0));
        Throwable exception2 = assertThrows(ServiceException.class, () -> dateTimeService.addDateTime(testDateTime));
        assertEquals("Wrong lesson duration. Expected lesson duration in minutes = 45, actual: -30", exception2.getMessage());
        testDateTime = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 0));
        Throwable exception3 = assertThrows(ServiceException.class, () -> dateTimeService.addDateTime(testDateTime));
        assertEquals("Wrong lesson duration. Expected lesson duration in minutes = 45, actual: 0", exception3.getMessage());
    }
    
    @Test
    void testUpdateDateTimeShouldReturnUpdatedDateTimeWhenExistingDateTimePassed() {
        testDateTime = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(7, 0), LocalTime.of(7, 45));
        when(dateTimeRepository.existsById(anyLong())).thenReturn(true);
        when(settingsService.getSettings()).thenReturn(defaultSettings);
        when(dateTimeRepository.save(testDateTime)).thenReturn(testDateTime);
        assertEquals(testDateTime, dateTimeService.updateDateTime(testDateTime));
    }
    
    @Test
    void testUpdateDateTimeShouldThrowResourceNotFoundExceptionWhenNonExistingDateTimePassed() {
        when(dateTimeRepository.existsById(anyLong())).thenReturn(false);
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> dateTimeService.updateDateTime(testDateTime));
        assertEquals("DateTime with id = 10 doesn't exist", exception.getMessage());
    }

    @Test
    void testRemoveDateTimeShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> dateTimeService.removeDateTime(null));
        assertEquals("DateTime is null", exception.getMessage());
    }
    
    @Test
    void testRemoveDateTimeShouldDoesNotThrowAnythingWhenPassedDateTimeDoesNotHaveLessons() {
        assertDoesNotThrow(() -> dateTimeService.removeDateTime(existingDateTime));
    }
    
    @Test
    void testRemoveDateTimeShouldThrowServiceExceptionWhenPassedDateTimeHasLessons() {
        testDateTime.setLessons(singletonList(new Lesson()));
        Throwable exception = assertThrows(ServiceException.class, () -> dateTimeService.removeDateTime(testDateTime));
        assertEquals("Deletion is not possible. DateTime has lessons."
                + "First remove lessons", exception.getMessage());
    }

    @Test
    void testGetAllDateTimesShouldReturnListAllDateTimes() {
        when(dateTimeRepository.findAll()).thenReturn(singletonList(existingDateTime));
        assertEquals(singletonList(existingDateTime), dateTimeService.getAllDateTimes());
    }
    
    @Test
    void testGetAllDatesShouldReturnSortedSetAllDatesFromAllDateTimes() {
        testDateTime = new DateTime(LocalDate.of(2021, 9, 1), LocalTime.of(8, 45), LocalTime.of(9, 30));
        when(dateTimeRepository.findAll()).thenReturn(Arrays.asList(testDateTime, existingDateTime));
        Set<LocalDate> expected = new TreeSet<LocalDate>();
        expected.add(LocalDate.of(2020, 9, 1));
        expected.add(LocalDate.of(2021, 9, 1));
        assertEquals(expected, dateTimeService.getAllDates());
    }
}
