package com.foxminded.university.domain.service.impl;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.dao.LessonRepository;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@ExtendWith(MockitoExtension.class)
class CourseServiceImplTest {
    
    @Mock
    private CourseRepository courseRepository;
    @Mock
    private LessonRepository lessonRepository;

    @InjectMocks
    private CourseServiceImpl courseService;
    
    private Course testCourse;
    
    @BeforeEach
    void setUp() throws Exception {
        testCourse = new Course(100, "testCourse", "testDescription");
    }

    @Test
    void testAddCourseShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> courseService.addCourse(null));
        assertEquals("Course is null", exception.getMessage());
    }
    
    @Test
    void testAddCourseShouldReturnCourseWithGeneratedIdWhenNonExistingCoursePassed() {
        when(courseRepository.save(testCourse)).thenReturn(testCourse);
        assertEquals(testCourse, courseService.addCourse(testCourse));
    }
    
    @Test
    void testGetCourseByIdShouldReturnCourseFromDataBaseByPassedId() {
        when(courseRepository.findById(100L)).thenReturn(Optional.of(testCourse));
        assertEquals(testCourse, courseService.getCourseById(100));
    }
    
    @Test
    void testGetCourseShouldThrowServiceExceptionWhenCourseWithPassedIdDoesNotExist() {
        when(courseRepository.findById(anyLong())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> courseService.getCourseById(10));
        assertEquals("Course with id = 10 doesn't exist", exception.getMessage());
    }

    @Test
    void testUpdateCourseShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> courseService.updateCourse(null));
        assertEquals("Course is null", exception.getMessage());
    }

    @Test
    void testUpdateCourseShouldReturnUpdatedCourseWhenExistingCoursePassed() {
        when(courseRepository.existsById(anyLong())).thenReturn(true);
        when(courseRepository.save(testCourse)).thenReturn(testCourse);
        assertEquals(testCourse, courseService.updateCourse(testCourse));
    }
    
    @Test
    void testUpdateCourseShouldThrowResourceNotFoundExceptionWhenNonExistingCoursePassed() {
        when(courseRepository.existsById(anyLong())).thenReturn(false);
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> courseService.updateCourse(testCourse));
        assertEquals("Course with id = 100 doesn't exist", exception.getMessage());
    }
    
    @Test
    void testRemoveCourseShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> courseService.removeCourse(null));
        assertEquals("Course is null", exception.getMessage());
    }
    
    @Test
    void testRemoveCourseShouldDoesNotThrowAnythingWhenPassedCourseDoesNotHaveLessons() {
        assertDoesNotThrow(() -> courseService.removeCourse(testCourse));
    }
    
    @Test
    void testRemoveCourseShouldThrowServiceExceptionWhenPassedCourseHasLessons() {
        testCourse.setLessons(singletonList(new Lesson()));
        Throwable exception = assertThrows(ServiceException.class, () -> courseService.removeCourse(testCourse));
        assertEquals("Deletion is not possible. Course has lessons."
                + "First remove lessons", exception.getMessage());
    }

    @Test
    void testGetAllCoursesShouldReturnListAllCourses() {
        when(courseRepository.findAll()).thenReturn(singletonList(testCourse));
        assertEquals(singletonList(testCourse), courseService.getAllCourses());
    }
    
    @Test
    void testCountCoursesShouldReturnNumberOfCoursesFromDataBase() {
        when(courseRepository.count()).thenReturn(10L);
        assertEquals(10, courseService.countCourses());
    }
}
