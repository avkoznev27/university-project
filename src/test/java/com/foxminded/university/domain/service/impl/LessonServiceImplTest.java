package com.foxminded.university.domain.service.impl;

import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.foxminded.university.dao.LessonRepository;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.exceptions.ResourceNotFoundException;

@ExtendWith(MockitoExtension.class)
class LessonServiceImplTest {
    
    @Mock
    private LessonRepository lessonRepository;

    @InjectMocks
    private LessonServiceImpl lessonService;
    
    private Lesson testLesson;
    private Lesson expectedLesson;
    private Auditorium testAuditorium;
    private Course testCourse;
    private DateTime testDateTime;
    private Group testGroup;
    private Teacher testTeacher;
    
    @BeforeEach
    void setUp() throws Exception {
        testLesson = new Lesson();
        testLesson.setAuditorium(new Auditorium(1));
        testLesson.setCourse(new Course(1));
        testLesson.setDateTime(new DateTime(1));
        testLesson.setGroup(new Group(1));
        testLesson.setTeacher(new Teacher(1));
        testAuditorium = new Auditorium(1, 10, 100);
        testCourse = new Course(1, "testCourse", "description");
        testDateTime = new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        testGroup = new Group(1, "group");
        testTeacher = new Teacher(1, "teacher");
        expectedLesson = new Lesson();
        expectedLesson.setLessonId(10);
        expectedLesson.setAuditorium(testAuditorium);
        expectedLesson.setCourse(testCourse);
        expectedLesson.setDateTime(testDateTime);
        expectedLesson.setGroup(testGroup);
        expectedLesson.setTeacher(testTeacher);
    }

    @Test
    void testAddLessonShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> lessonService.addLesson(null));
        assertEquals("Lesson is null", exception.getMessage());
    }
    
    @Test
    void testAddLessonShouldReturnAddedLessonWhenLessonPassed() {
        when(lessonRepository.save(testLesson)).thenReturn(expectedLesson);
        assertNotNull(lessonService.addLesson(testLesson));
    }
    
    @Test
    void testGetLessonByIdShouldReturnLessonWithSettedFieldsFromDataBaseByPassedId() {
        when(lessonRepository.findById(10L)).thenReturn(Optional.of(expectedLesson));
        assertEquals(expectedLesson, lessonService.getLessonById(10));
    }
    
    @Test
    void testGetLessonByIdShouldThrowResourceNotFoundExceptionWhenLessonWithPassedIdDoesNotExist() {
        when(lessonRepository.findById(anyLong())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> lessonService.getLessonById(10));
        assertEquals("Lesson with id = 10 doesn't exist", exception.getMessage());
    }

    @Test
    void testUpdateLessonShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> lessonService.updateLesson(null));
        assertEquals("Lesson is null", exception.getMessage());
    }
    
    @Test
    void testUpdateLessonShouldReturnUpdatedLessonWhenExistingLessonPassed() {
        when(lessonRepository.existsById(anyLong())).thenReturn(true);
        when(lessonRepository.save(expectedLesson)).thenReturn(expectedLesson);
        assertEquals(expectedLesson, lessonService.updateLesson(expectedLesson));
    }
    
    @Test
    void testUpdateLessonShouldThrowResourceNotFoundExceptionWhenNonExistingLessonPassed() {
        when(lessonRepository.existsById(anyLong())).thenReturn(false);
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> lessonService.updateLesson(expectedLesson));
        assertEquals("Lesson with id = 10 doesn't exist", exception.getMessage());
    }

    @Test
    void testRemoveLessonShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> lessonService.removeLesson(null));
        assertEquals("Lesson is null", exception.getMessage());
    }
    
    @Test
    void testRemoveLessonShouldDoesNotThrowAnythingWhenLessonPassed() {
        assertDoesNotThrow(() -> lessonService.removeLesson(testLesson));
    }

    @Test
    void testGetAllLessonsShouldReturnListAllLessonsWithFilledFields() {
        when(lessonRepository.findAll()).thenReturn(singletonList(expectedLesson));
        List<Lesson> actual = lessonService.getAllLessons();
        assertEquals(testAuditorium, actual.get(0).getAuditorium());
        assertEquals(testCourse, actual.get(0).getCourse());
        assertEquals(testDateTime, actual.get(0).getDateTime());
        assertEquals(testGroup, actual.get(0).getGroup());
        assertEquals(testTeacher, actual.get(0).getTeacher());
    }
}
