package com.foxminded.university.domain.service.impl;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.dao.LessonRepository;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Settings;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.service.SettingsService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@ExtendWith(MockitoExtension.class)
class GroupServiceImplTest {

    @Mock
    private GroupRepository groupRepository;
    @Mock
    private LessonRepository lessonRepository;
    @Mock
    private SettingsService settingsService;

    @InjectMocks
    private GroupServiceImpl groupService;

    private Group testGroup;
    private Group existingGroup;
    private Lesson testLesson;
    private Student testStudent;
    private DateTime testDateTime;
    private Settings defaultSettings;

    @BeforeEach
    void setUp() throws Exception {
        defaultSettings = new Settings(1, 45, 2);
        existingGroup = new Group(1, "existingGroup");
        testGroup = new Group(2, "testGroup");
        testLesson = new Lesson();
        testStudent = new Student(1, 2, "testStudent");
        testDateTime = new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
    }

    @Test
    void testAddGroupShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> groupService.addGroup(null));
        assertEquals("Group is null", exception.getMessage());
    }

    @Test
    void testAddGroupShouldReturnGroupWithGeneratedIdWhenNonExistingGroupPassed() {
        when(groupRepository.save(testGroup)).thenReturn(testGroup);
        assertEquals(testGroup, groupService.addGroup(testGroup));
    }

    @Test
    void testUpdateGroupShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> groupService.updateGroup(null));
        assertEquals("Group is null", exception.getMessage());
    }

    @Test
    void testUpdateGroupShouldReturnUpdatedGroupWhenExistingGroupPassed() {
        when(groupRepository.existsById(anyLong())).thenReturn(true);
        when(groupRepository.save(testGroup)).thenReturn(testGroup);
        assertEquals(testGroup, groupService.updateGroup(testGroup));
    }
    
    @Test
    void testUpdateGroupShouldThrowResourceNotFoundExceptionWhenNonExistingGroupPassed() {
        when(groupRepository.existsById(anyLong())).thenReturn(false);
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> groupService.updateGroup(testGroup));
        assertEquals("Group with id = 2 doesn't exist", exception.getMessage());
    }

    @Test
    void testRemoveGroupShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> groupService.removeGroup(null));
        assertEquals("Group is null", exception.getMessage());
    }

    @Test
    void testRemoveGroupShouldDoesNotThrowAnythingWhenPassedGroupDoesNotHaveLessonsAndStudents() {
        assertDoesNotThrow(() -> groupService.removeGroup(testGroup));
    }
    
    @Test
    void testRemoveGroupShouldThrowServiceExceptionWhenPassedGroupHasLessons() {
        testGroup.setLessons(singletonList(testLesson));
        assertThrows(ServiceException.class, () -> groupService.removeGroup(testGroup));
    }
    
    @Test
    void testRemoveGroupShouldThrowServiceExceptionWhenPassedGroupHasStudents() {
        testGroup.setStudents(singletonList(testStudent));
        assertThrows(ServiceException.class, () -> groupService.removeGroup(testGroup));
    }

    @Test
    void testGetGroupByIdShouldReturnGroupWithStudentsWhenGroupIdPassed() {
        testGroup.getStudents().add(testStudent);
        testGroup.getStudents().add(new Student(50, 2, "student"));
        when(groupRepository.findById(2L)).thenReturn(Optional.of(testGroup));
        Group actual = groupService.getGroupById(2);
        assertEquals(testGroup, actual);
    }

    @Test
    void testGetGroupByIdShouldThrowResourceNotFoundExceptionWhenGroupWithPassedIdDoesNotExist() {
        when(groupRepository.findById(anyLong())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> groupService.getGroupById(10));
        assertEquals("Group with id = 10 doesn't exist", exception.getMessage());
    }

    @Test
    void testGetAllGroupsShouldReturnListAllGroups() {
        when(groupRepository.findAll()).thenReturn(singletonList(testGroup));
        List<Group> expected = singletonList(testGroup);
        List<Group> actual = groupService.getAllGroups();
        assertEquals(expected, actual);
    }

    @Test
    void testGetAllFreeGroupsShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> groupService.getAllFreeGroups(null));
        assertEquals("DateTime is null", exception.getMessage());
    }

    @Test
    void testGetAllFreeGroupsShouldReturnListAllFreeGroupsThisTimeWhenDateTimePassed() {
        Lesson testLesson2 = new Lesson();
        testLesson2.setDateTime(new DateTime(20));
        testLesson.setGroup(existingGroup);
        testLesson.setDateTime(testDateTime);
        when(lessonRepository.findAll()).thenReturn(Arrays.asList(testLesson, testLesson2));
        when(groupRepository.findAll()).thenReturn(Arrays.asList(existingGroup, testGroup));
        List<Group> expected = singletonList(testGroup);
        List<Group> actual = groupService.getAllFreeGroups(testDateTime);
        assertEquals(expected, actual);
    }

    @Test
    void testGetAllFreeGroupsShouldReturnEmptyListWhenNotFreeGroupsWithPassedDateTime() {
        testLesson.setGroup(testGroup);
        testLesson.setDateTime(testDateTime);
        when(lessonRepository.findAll()).thenReturn(singletonList(testLesson));
        when(groupRepository.findAll()).thenReturn(singletonList(testGroup));
        List<Group> actual = groupService.getAllFreeGroups(testDateTime);
        assertEquals(emptyList(), actual);
    }

    @Test
    void testGetAllIncompleteGroupsShouldReturnListAllGroupsWithGroupSizeLessThanMaxGroupSize() {
        when(settingsService.getSettings()).thenReturn(defaultSettings);
        when(groupRepository.findAll()).thenReturn(singletonList(testGroup));
        assertEquals(singletonList(testGroup), groupService.getAllIncompleteGroups());
    }

    @Test
    void testGetAllIncompleteGroupsShouldReturnEmptyListWhenAllGroupHasMaxGroupSize() {
        when(settingsService.getSettings()).thenReturn(defaultSettings);
        testGroup.setStudents(Arrays.asList(new Student(), new Student()));
        when(groupRepository.findAll()).thenReturn(singletonList(testGroup));
        assertEquals(emptyList(), groupService.getAllIncompleteGroups());
    }

    @Test
    void testCountGroupsShouldReturnNumberOfGroupsFromDataBase() {
        when(groupRepository.count()).thenReturn(10L);
        assertEquals(10, groupService.countGroups());
    }
}
