package com.foxminded.university.domain.service.impl;

import static java.util.Collections.emptyList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.foxminded.university.dao.StudentRepository;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Settings;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.SettingsService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@ExtendWith(MockitoExtension.class)
class StudentServiceImplTest {
    
    @Mock
    private StudentRepository studentRepository;
    @Mock
    private GroupService groupService;
    @Mock
    private SettingsService settingsService;

    @InjectMocks
    private StudentServiceImpl studentService;
    
    private Student testStudent;
    private Group testGroup;
    private Course testCourse;
    private DateTime testDateTime;
    private Settings defaultSettings;
    private Lesson expectedLesson;
    
    @BeforeEach
    void setUp() throws Exception {
        defaultSettings = new Settings(1, 45, 2);
        testGroup = new Group(1, "testGroup");
        testStudent = new Student(1, 1, "testStudent");
        testCourse = new Course(1, "testCourse", "description");
        testDateTime = new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        expectedLesson = new Lesson();
        expectedLesson.setGroup(testGroup);
        expectedLesson.setCourse(testCourse);
        expectedLesson.setDateTime(testDateTime);
        
    }

    @Test
    void testAddStudentShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception1 = assertThrows(IllegalArgumentException.class, () -> studentService.addStudent(null, null));
        assertEquals("Group is null", exception1.getMessage());
        Throwable exception2 = assertThrows(IllegalArgumentException.class, () -> studentService.addStudent(null, testStudent));
        assertEquals("Group is null", exception2.getMessage());
        Throwable exception3 = assertThrows(IllegalArgumentException.class, () -> studentService.addStudent(testGroup, null));
        assertEquals("Student is null", exception3.getMessage());
    }
    
    @Test
    void testAddStudentShouldThrowServiceExceptionWhenPassedGroupHasMaxGroupSize() {
        when(settingsService.getSettings()).thenReturn(defaultSettings);
        testGroup.setStudents(Arrays.asList(testStudent, testStudent));
        Throwable exception = assertThrows(ServiceException.class, () -> studentService.addStudent(testGroup, testStudent));
        assertEquals("Group is full. Unable to add student", exception.getMessage());
    }
    
    @Test
    void testAddStudentShouldAddStudentToGroupWhenStudentAndGroupPassed() {
        when(settingsService.getSettings()).thenReturn(defaultSettings);
        testStudent.getCourses().add(testCourse);
        studentService.addStudent(testGroup, testStudent);
        long expectedGroupId = testGroup.getGroupId();
        long actualGroupId = testStudent.getGroupId();
        assertEquals(expectedGroupId, actualGroupId);
        List<Student> expectedStudentList = singletonList(testStudent);
        List<Student> actualStudentList = testGroup.getStudents();
        assertEquals(expectedStudentList, actualStudentList);  
    }

    @Test
    void testGetStudentByIdShouldReturnStudentWithAllCoursesWhenStudentIdPassed() {
        testStudent.getCourses().add(testCourse);
        when(studentRepository.findById(anyLong())).thenReturn(Optional.of(testStudent));
        Student actualStudent = studentService.getStudentById(1);
        assertEquals(singleton(testCourse), actualStudent.getCourses());
    }
    
    @Test
    void testGetStudentByIdShouldThrowResourceNotFoundExceptionWhenStudentWithPassedIdDoesNotExist() {
        when(studentRepository.findById(anyLong())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> studentService.getStudentById(10));
        assertEquals("Student with id = 10 doesn't exist", exception.getMessage());
    }

    @Test
    void testGetAllStudentsShouldReturnListAllStudentWithCourses() {
        when(studentRepository.findAll()).thenReturn(singletonList(testStudent));
        Student expectedStudent = new Student(1, 1, "testStudent");
        expectedStudent.getCourses().add(testCourse);
        List<Student> expected = singletonList(expectedStudent);
        List<Student> actual = studentService.getAllStudents();
        assertEquals(expected, actual);
    }

    @Test
    void testUpdateStudentShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> studentService.updateStudent(null));
        assertEquals("Student is null", exception.getMessage());
    }
    
    @Test
    void testUpdateStudentShouldReturnUpdatedStudentWhenExistingStudentPassed() {
        when(studentRepository.existsById(anyLong())).thenReturn(true);
        when(studentRepository.save(testStudent)).thenReturn(testStudent);
        assertEquals(testStudent, studentService.updateStudent(testStudent));
    }
    
    @Test
    void testUpdateStudentShouldThrowResourceNotFoundExceptionWhenNonExistingStudentPassed() {
        when(studentRepository.existsById(anyLong())).thenReturn(false);
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> studentService.updateStudent(testStudent));
        assertEquals("Student with id = 1 doesn't exist", exception.getMessage());
    }

    @Test
    void testRemoveStudentShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> studentService.removeStudent(null));
        assertEquals("Student is null", exception.getMessage());
    }
    
    @Test
    void testRemoveStudentShouldDoesNotThrowAnythingWhenStudentPassed() {
        assertDoesNotThrow(() -> studentService.removeStudent(testStudent));
    }
    
    @Test
    void testGetStudentTimetableShouldThrowsIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception2 = assertThrows(IllegalArgumentException.class, () -> studentService.getStudentTimetable(null, LocalDate.now()));
        assertEquals("Student is null", exception2.getMessage());
        Throwable exception3 = assertThrows(IllegalArgumentException.class, () -> studentService.getStudentTimetable(testStudent, null, LocalDate.now()));
        assertEquals("Start day is null", exception3.getMessage());
        Throwable exception4 = assertThrows(IllegalArgumentException.class, () -> studentService.getStudentTimetable(testStudent, LocalDate.now(), null));
        assertEquals("End day is null", exception4.getMessage());
    }
    
    @Test
    void testGetStudentTimetableShouldReturnListAllLessonsForStudentOnPassedDay() {
        testGroup.setLessons(singletonList(expectedLesson));
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        List<Lesson> studentTimetable = studentService.getStudentTimetable(testStudent, LocalDate.of(2020, 9, 1));
        assertEquals(singletonList(expectedLesson), studentTimetable);
    }
    
    @Test
    void testGetStudentTimetableShouldReturnEmptyListWhenStudentDoesNotHaveLessonsOnPassedDay() {
        testGroup.setLessons(singletonList(expectedLesson));
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        List<Lesson> studentTimetable = studentService.getStudentTimetable(testStudent, LocalDate.of(2020, 9, 2));
        assertEquals(emptyList(), studentTimetable);
    }
    
    @Test
    void testGetStudentTimetableShouldReturnListAllLessonsForStudentOnPassedPeriodOfDay() {
        testGroup.setLessons(singletonList(expectedLesson));
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        List<Lesson> studentTimetable = studentService.getStudentTimetable(testStudent, LocalDate.of(2020, 8, 25), LocalDate.of(2020, 9, 10));
        assertEquals(singletonList(expectedLesson), studentTimetable);
    }
    
    @Test
    void testGetStudentTimetableShouldReturnEmptyListWhenStudentDoesNotHaveLessonsOnPassedPeriod() {
        testGroup.setLessons(singletonList(expectedLesson));
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        List<Lesson> studentTimetable = studentService.getStudentTimetable(testStudent, LocalDate.of(2020, 9, 2), LocalDate.of(2020, 9, 20));
        assertEquals(emptyList(), studentTimetable);
    }
    
    @Test
    void testGetStudentTimetableShouldThrowServiceEsceptionWhenEndDayIsBeforeStartDay() {
        Throwable exception = assertThrows(ServiceException.class, () -> studentService.getStudentTimetable(testStudent, LocalDate.of(2020, 9, 20), LocalDate.of(2020, 9, 2)));
        assertEquals("Wrong period", exception.getMessage());
    }
    
    @Test
    void testCountStudentsShouldReturnNumberOfStudentsFromDataBase() {
        when(studentRepository.count()).thenReturn(10L);
        assertEquals(10, studentService.countStudents());
    }
}
