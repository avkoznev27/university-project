package com.foxminded.university.domain.service.impl;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import com.foxminded.university.dao.SettingsRepository;
import com.foxminded.university.domain.model.Settings;

@ExtendWith(MockitoExtension.class)
class SettingsServiceImplTest {
    
    @Mock
    private SettingsRepository settingsRepository;
    
    @InjectMocks
    private SettingsServiceImpl settingsService;
    
    private Settings defaultSettings;
    private Settings actualSettings;

    @BeforeEach
    void setUp() throws Exception {
        defaultSettings= new Settings(1L, 45, 30);
        actualSettings = new Settings(1L, 90, 50);
        ReflectionTestUtils.setField(settingsService, "maxGroupSize", "30");
        ReflectionTestUtils.setField(settingsService, "lessonDuration", "45");
    }

    @Test
    void testGetSettingsShouldReturnActualSettings() {
        when(settingsRepository.findById(1L)).thenReturn(Optional.of(actualSettings));
        assertEquals(actualSettings, settingsService.getSettings());
    }
    
    @Test
    void testGetSettingsShouldReturnDefaultSettingsWhenSettingsNotSet() {
        when(settingsRepository.findById(1L)).thenReturn(Optional.empty());
        assertEquals(defaultSettings, settingsService.getSettings());
    }
    
    @Test
    void testSetDefaultSettingsShouldSetAndReturnDefaultSettings() {
        when(settingsRepository.save(any(Settings.class))).thenReturn(defaultSettings);
        assertEquals(defaultSettings, settingsService.setDefaultSettings());
    }
    
    @Test
    void testUpdateSettingsSouldThrowsIllegalArgumentExceptionWhenNullPassed() {
        assertThrows(IllegalArgumentException.class, () -> settingsService.updateSettings(null));
    }
    
    @Test
    void testUpdateSettingsSouldDoesNotThrowAnythingWhenSettingsPassed() {
        assertDoesNotThrow(() -> settingsService.updateSettings(actualSettings));
    }

}
