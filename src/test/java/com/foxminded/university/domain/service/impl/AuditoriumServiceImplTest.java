package com.foxminded.university.domain.service.impl;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.foxminded.university.dao.AuditoriumRepository;
import com.foxminded.university.dao.LessonRepository;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@ExtendWith(MockitoExtension.class)
class AuditoriumServiceImplTest {
    
    @Mock
    private AuditoriumRepository auditoriumRepository;
    @Mock
    private LessonRepository lessonRepository;

    @InjectMocks
    private AuditoriumServiceImpl auditoriumService;
    
    private Auditorium testAuditorium;
    private Auditorium existingAuditorium;
    private DateTime testDateTime;
    private Lesson testLesson;

    @BeforeEach
    void setUp() throws Exception {
        testAuditorium = new Auditorium(10, 50, 35);
        existingAuditorium = new Auditorium(100, 100, 100);
        testDateTime = new DateTime(1);
        testLesson = new Lesson();
    }

    @Test
    void testAddAuditoriumShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> auditoriumService.addAuditorium(null));
        assertEquals("Auditorium is null", exception.getMessage());
    }
    
    @Test
    void testAddAuditoriumShouldReturnAuditoriumWithGeneratedIdWhenNonExistingAuditoriumPassed() {
        when(auditoriumRepository.save(testAuditorium)).thenReturn(testAuditorium);
        assertEquals(testAuditorium, auditoriumService.addAuditorium(testAuditorium));
    }
    
    @Test
    void testGetAuditoriumByIdShouldReturnAuditoriumFromDataBaseByPassedId() {
        when(auditoriumRepository.findById(10L)).thenReturn(Optional.of(testAuditorium));
        assertEquals(testAuditorium, auditoriumService.getAuditoriumById(10));
    }
    
    @Test
    void testGetAuditoriumShouldThrowResourceNotFoundExceptionWhenAuditoriumWithPassedIdDoesNotExist() {
        when(auditoriumRepository.findById(anyLong())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> auditoriumService.getAuditoriumById(10));
        assertEquals("Auditorium with id = 10 doesn't exist", exception.getMessage());
    }
    
    @Test
    void testUpdateAuditoriumShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> auditoriumService.updateAuditorium(null));
        assertEquals("Auditorium is null", exception.getMessage());
    }
    
    @Test
    void testUpdateAuditoriumShouldReturnUpdatedAuditoriumWhenExistingAuditoriumPassed() {
        when(auditoriumRepository.existsById(anyLong())).thenReturn(true);
        when(auditoriumRepository.save(testAuditorium)).thenReturn(testAuditorium);
        assertEquals(testAuditorium, auditoriumService.updateAuditorium(testAuditorium));
    }
    
    @Test
    void testUpdateAuditoriumShouldThrowResourceNotFoundExceptionWhenNonExistingAuditoriumPassed() {
        when(auditoriumRepository.existsById(anyLong())).thenReturn(false);
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> auditoriumService.updateAuditorium(testAuditorium));
        assertEquals("Auditorium with id = 10 doesn't exist", exception.getMessage());
    }

    @Test
    void testRemoveAuditoriumShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> auditoriumService.removeAuditorium(null));
        assertEquals("Auditorium is null", exception.getMessage());
    }
    
    @Test
    void testRemoveAuditoriumShouldThrowServiceExceptionWhenPassedAuditoriumHasLessons() {
        testLesson.setAuditorium(testAuditorium);
        testAuditorium.setLessons(singletonList(testLesson));
        Throwable exception = assertThrows(ServiceException.class, () -> auditoriumService.removeAuditorium(testAuditorium));
        assertEquals("Deletion is not possible. Auditorium has lessons. "
                + "First remove lessons or change auditorium for lessons", exception.getMessage());
    }
    
    @Test
    void testRemoveAuditoriumShouldDoesNotThrowAnythingWhenPassedAuditoriumDoesNotHaveLessons() {
        assertDoesNotThrow(() -> auditoriumService.removeAuditorium(testAuditorium));
    }

    @Test
    void testGetAllAuditoriumsShouldReturnListAllAuditoriums() {
        when(auditoriumRepository.findAll()).thenReturn(singletonList(testAuditorium));
        assertEquals(singletonList(testAuditorium), auditoriumService.getAllAuditoriums());
    }

    @Test
    void testGetAllFreeAuditoriumsShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> auditoriumService.getAllFreeAuditoriums(null));
        assertEquals("DateTime is null", exception.getMessage());
    }

    @Test
    void testGetAllFreeAuditoriumsShouldReturnListAllFreeAuditoriumsThisDateTimeWhenDateTimePassed() {
       testLesson.setDateTime(testDateTime);
       testLesson.setAuditorium(testAuditorium);
       when(lessonRepository.findAll()).thenReturn(singletonList(testLesson));
       when(auditoriumRepository.findAll()).thenReturn(Arrays.asList(existingAuditorium, testAuditorium));
       List<Auditorium> freeAuditoriums = auditoriumService.getAllFreeAuditoriums(testDateTime);
       assertEquals(singletonList(existingAuditorium), freeAuditoriums);
    }
    
    @Test
    void testGetAllFreeAuditoriumsShouldReturnEmptyListWhenNotFreeAuditoriumsWithPassedDateTime() {
       testLesson.setDateTime(testDateTime);
       testLesson.setAuditorium(testAuditorium);
       when(lessonRepository.findAll()).thenReturn(singletonList(testLesson));
       when(auditoriumRepository.findAll()).thenReturn(singletonList(testAuditorium));
       List<Auditorium> freeAuditoriums = auditoriumService.getAllFreeAuditoriums(testDateTime);
       assertEquals(emptyList(), freeAuditoriums);
    }
    
    @Test
    void testGetAllFreeAuditoriumsDateTimeShouldThrowIllegalArgumentExceptionWhenDateTimeIsNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> auditoriumService.getAllFreeAuditoriums(null, 1));
        assertEquals("DateTime is null", exception.getMessage());
    }
    
    @Test
    void testGetAllFreeAuditoriumsShouldReturnListAllFreeAuditoriumsThisDateTimeAndMoreOrEqualsCapacityWhenDateTimePassed() {
       int testCapacity = 30;
       testLesson.setDateTime(new DateTime(2));
       testLesson.setAuditorium(testAuditorium);
       when(lessonRepository.findAll()).thenReturn(singletonList(testLesson));
       when(auditoriumRepository.findAll()).thenReturn(singletonList(testAuditorium));
       List<Auditorium> freeAuditoriums = auditoriumService.getAllFreeAuditoriums(testDateTime, testCapacity);
       assertEquals(singletonList(testAuditorium), freeAuditoriums);
    }
    
    @Test
    void testGetAllFreeAuditoriumsShouldReturnEmptyListIfNotAuditoriumsSuitableCapacityWhenDateTimePassed() {
       int testCapacity = 40;
       testLesson.setDateTime(new DateTime(2));
       testLesson.setAuditorium(testAuditorium);
       when(lessonRepository.findAll()).thenReturn(singletonList(testLesson));
       when(auditoriumRepository.findAll()).thenReturn(singletonList(testAuditorium));
       List<Auditorium> freeAuditoriums = auditoriumService.getAllFreeAuditoriums(testDateTime, testCapacity);
       assertEquals(emptyList(), freeAuditoriums);
    }
    
    @Test
    void testCountAuditoriumsShouldReturnNumberOfAuditoriumsFromDataBase() {
        when(auditoriumRepository.count()).thenReturn(10L);
        assertEquals(10, auditoriumService.countAuditoriums());
    }
}
