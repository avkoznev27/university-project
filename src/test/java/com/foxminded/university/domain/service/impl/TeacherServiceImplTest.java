package com.foxminded.university.domain.service.impl;

import static java.util.Collections.emptyList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.dao.LessonRepository;
import com.foxminded.university.dao.TeacherRepository;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@ExtendWith(MockitoExtension.class)
class TeacherServiceImplTest {
    
    @Mock
    private TeacherRepository teacherRepository;
    @Mock
    private LessonRepository lessonRepository;
    @Mock
    private CourseRepository courseRepository;

    @InjectMocks
    private TeacherServiceImpl teacherService;
    
    private Teacher testTeacher;
    private Lesson testLesson;
    private Course testCourse;
    private DateTime testDateTime;
    
    @BeforeEach
    void setUp() throws Exception {
        testTeacher = new Teacher(1, "testTeacher");
        testLesson = new Lesson();
        testCourse = new Course(1, "testCourse", "description");
        testDateTime = new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        testLesson.setDateTime(testDateTime);
        testLesson.setTeacher(testTeacher);
    }

    @Test
    void testAddTeacherShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> teacherService.addTeacher(null));
        assertEquals("Teacher is null", exception.getMessage());
    }
    
    @Test
    void testAddTeacherShouldReturnTeacherWhenTeacherPassed() {
        testTeacher.getCourses().add(testCourse);
        when(teacherRepository.save(testTeacher)).thenReturn(testTeacher);
        assertNotNull(teacherService.addTeacher(testTeacher));
    }
    
    @Test
    void testUpdateTeacherShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> teacherService.updateTeacher(null));
        assertEquals("Teacher is null", exception.getMessage());
    }
    
    @Test
    void testUpdateTeacherShouldReturnUpdatedTeacherWhenExistingTeacherPassed() {
        when(teacherRepository.existsById(anyLong())).thenReturn(true);
        when(teacherRepository.save(testTeacher)).thenReturn(testTeacher);
        assertEquals(testTeacher, teacherService.updateTeacher(testTeacher));
    }
    
    @Test
    void testUpdateTeacherShouldThrowResourceNotFoundExceptionWhenNonExistingTeacherPassed() {
        when(teacherRepository.existsById(anyLong())).thenReturn(false);
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> teacherService.updateTeacher(testTeacher));
        assertEquals("Teacher with id = 1 doesn't exist", exception.getMessage());
    }
    
    @Test
    void testRemoveTeacherShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> teacherService.removeTeacher(null));
        assertEquals("Teacher is null", exception.getMessage());
    }
    
    @Test
    void testRemoveTeacherShouldDoesNotThrowAnyExceptionWhenTeacherPassed() {
        assertDoesNotThrow(() -> teacherService.removeTeacher(testTeacher));
    }
    
    @Test
    void testRemoveTeacherShouldThrowServiceExceptionWhenPassedTeacherHasLessons() {
        testTeacher.setLessons(singletonList(testLesson));
        Throwable exception = assertThrows(ServiceException.class, () -> teacherService.removeTeacher(testTeacher));
        assertEquals("Deletion is not possible. Teacher has lessons."
                + "First remove lessons", exception.getMessage());
    }

    @Test
    void testGetTeacherByIdShouldReturnTeacherWithAllCoursesWhenTeacherIdPassed() {
        testTeacher.getCourses().add(testCourse);
        when(teacherRepository.findById(anyLong())).thenReturn(Optional.of(testTeacher));
        Teacher actualTeacher = teacherService.getTeacherById(1);
        assertEquals(singleton(testCourse), actualTeacher.getCourses());
    }
    
    @Test
    void testGetTeacherByIdShouldThrowResourceNotFoundExceptionWhenTeacherWithPassedIdDoesNotExist() {
        when(teacherRepository.findById(anyLong())).thenReturn(Optional.empty());
        Throwable exception = assertThrows(ResourceNotFoundException.class, () -> teacherService.getTeacherById(10));
        assertEquals("Teacher with id = 10 doesn't exist", exception.getMessage());
    }

    @Test
    void testGetAllTeachersShouldReturnListAllTeacherWithCourses() {
        when(teacherRepository.findAll()).thenReturn(singletonList(testTeacher));
        Teacher expectedTeacher = new Teacher(1, "testTeacher");
        expectedTeacher.getCourses().add(testCourse);
        List<Teacher> expected = singletonList(expectedTeacher);
        List<Teacher> actual = teacherService.getAllTeachers();
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetAllBusyTeachersShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception = assertThrows(IllegalArgumentException.class, () -> teacherService.getAllBusyTeachers(null));
        assertEquals("DateTime is null", exception.getMessage());
    }
    
    @Test
    void testGetAllBusyTeachersShouldReturnListAllBusyTeachersAtPassedTime() {
        Teacher testTeacher1 = new Teacher(1, "testTeacher1");
        Lesson testLesson2 = new Lesson();
        testLesson2.setDateTime(new DateTime(20));
        testLesson.setDateTime(testDateTime);
        testLesson.setTeacher(testTeacher1);
        when(lessonRepository.findAll()).thenReturn(Arrays.asList(testLesson, testLesson2));
        assertEquals(singletonList(testTeacher1), teacherService.getAllBusyTeachers(testDateTime));
    }

    @Test
    void testGetAllFreeTeachersShouldThrowIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception1 = assertThrows(IllegalArgumentException.class, () -> teacherService.getAllFreeTeachers(null, testCourse));
        assertEquals("DateTime is null", exception1.getMessage());
        Throwable exception2 = assertThrows(IllegalArgumentException.class, () -> teacherService.getAllFreeTeachers(testDateTime, null));
        assertEquals("Course is null", exception2.getMessage());
        Throwable exception3 = assertThrows(IllegalArgumentException.class, () -> teacherService.getAllFreeTeachers(null, null));
        assertEquals("DateTime is null", exception3.getMessage());
    }
    
    @Test
    void testGetAllFreeTeachersShouldReturnListAllFreeTeachersForPassedCourseAtPassedDateTime() {
        Teacher testTeacher1 = new Teacher(1, "testTeacher1");
        Teacher testTeacher2 = new Teacher(2, "testTeacher2");
        testLesson.setDateTime(testDateTime);
        testLesson.setTeacher(testTeacher1);
        testCourse.setTeachers(Arrays.asList(testTeacher1, testTeacher2));
        when(lessonRepository.findAll()).thenReturn(singletonList(testLesson));
        when(courseRepository.getOne(anyLong())).thenReturn(testCourse);
        assertEquals(singletonList(testTeacher2), teacherService.getAllFreeTeachers(testDateTime, testCourse));
    }
    
    @Test
    void testGetAllFreeTeachersShouldReturnEmptyListWhenForPassedCourseAtPassedDateTimeNotTeachers() {
        testLesson.setTeacher(testTeacher);
        testLesson.setDateTime(testDateTime);
        testCourse.setLessons(emptyList());
        when(lessonRepository.findAll()).thenReturn(singletonList(testLesson));
        when(courseRepository.getOne(anyLong())).thenReturn(testCourse);
        assertEquals(emptyList(), teacherService.getAllFreeTeachers(testDateTime, testCourse));
    }
    
    @Test
    void testGetAllFreeTeachersShouldThrowServiceExceptionWhenNonExistingCoursePassed() {
        when(courseRepository.getOne(anyLong())).thenThrow(new EntityNotFoundException("Message"));
        Throwable exception = assertThrows(ServiceException.class, () -> teacherService.getAllFreeTeachers(testDateTime, testCourse));
        assertEquals("Message", exception.getMessage());
    }
    
    @Test
    void testGetTeacherTimetableShouldThrowsIllegalArgumentExceptionWhenNullPassed() {
        Throwable exception2 = assertThrows(IllegalArgumentException.class, () -> teacherService.getTeacherTimetable(null, LocalDate.now()));
        assertEquals("Teacher is null", exception2.getMessage());
        Throwable exception3 = assertThrows(IllegalArgumentException.class, () -> teacherService.getTeacherTimetable(testTeacher, null, LocalDate.now()));
        assertEquals("Start day is null", exception3.getMessage());
        Throwable exception4 = assertThrows(IllegalArgumentException.class, () -> teacherService.getTeacherTimetable(testTeacher, LocalDate.now(), null));
        assertEquals("End day is null", exception4.getMessage());
    }
    
    @Test
    void testGetTeacherTimetableShouldReturnListAllLessonsForTeacherOnPassedDay() {
        testTeacher.setLessons(singletonList(testLesson));
        List<Lesson> teacherTimetable = teacherService.getTeacherTimetable(testTeacher, LocalDate.of(2020, 9, 1));
        assertEquals(singletonList(testLesson), teacherTimetable);
    }
    
    @Test
    void testGetTeacherTimetableShouldReturnEmptyListWhenTeacherDoesNotHaveLessonsOnPassedDay() {
        testTeacher.setLessons(singletonList(testLesson));
        List<Lesson> teacherTimetable = teacherService.getTeacherTimetable(testTeacher, LocalDate.of(2020, 9, 2));
        assertEquals(emptyList(), teacherTimetable);
    }
    
    @Test
    void testGetTeacherTimetableShouldReturnListAllLessonsForTeacherOnPassedPeriodOfDay() {
        testTeacher.setLessons(singletonList(testLesson));
        List<Lesson> teacherTimetable = teacherService.getTeacherTimetable(testTeacher, LocalDate.of(2020, 8, 25), LocalDate.of(2020, 9, 10));
        assertEquals(singletonList(testLesson), teacherTimetable);
    }
    
    @Test
    void testGetTeacherTimetableShouldReturnEmptyListWhenTeacherDoesNotHaveLessonsOnPassedPeriod() {
        testTeacher.setLessons(singletonList(testLesson));
        List<Lesson> teacherTimetable = teacherService.getTeacherTimetable(testTeacher, LocalDate.of(2020, 9, 2), LocalDate.of(2020, 9, 20));
        assertEquals(emptyList(), teacherTimetable);
    }
    
    @Test
    void testGetTeacherTimetableShouldThrowServiceEsceptionWhenEndDayIsBeforeStartDay() {
        Throwable exception = assertThrows(ServiceException.class, () -> teacherService.getTeacherTimetable(testTeacher, LocalDate.of(2020, 9, 20), LocalDate.of(2020, 9, 2)));
        assertEquals("Wrong period", exception.getMessage());
    }
    
    @Test
    void testCountTeachersShouldReturnNumberOfTeachersFromDataBase() {
        when(teacherRepository.count()).thenReturn(10L);
        assertEquals(10, teacherService.countTeachers());
    }
}
