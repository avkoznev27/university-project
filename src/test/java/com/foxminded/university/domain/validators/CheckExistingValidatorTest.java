package com.foxminded.university.domain.validators;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;

import javax.validation.ConstraintValidatorContext;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.foxminded.university.dao.AuditoriumRepository;
import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.dao.DateTimeRepository;
import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;

@ExtendWith(MockitoExtension.class)
class CheckExistingValidatorTest {
    
    @Mock
    private AuditoriumRepository auditoriumRepository;
    @Mock
    private CourseRepository courseRepository;
    @Mock
    private GroupRepository groupRepository;
    @Mock
    private DateTimeRepository dateTimeRepository;
    @Mock
    private ConstraintValidatorContext constraintValidatorContext;
    
    private Auditorium testAuditorium;
    private Course testCourse;
    private Group testGroup;
    
    private Auditorium existingAuditorium;
    private Course existingCourse;
    private Group existingGroup;
    
    @InjectMocks
    private CheckExistingValidator checkExistingValidator;

    @BeforeEach
    void setUp() throws Exception {
        existingAuditorium = new Auditorium(1, 10, 35);
        existingCourse = new Course(1, "testCourse", "testDescription");
        existingGroup = new Group(1, "testGroup");
        testAuditorium = new Auditorium(10, 35);
        testCourse = new Course("testCourse", "testDescription");
        testGroup = new Group("testGroup");
    }
    
    @Test
    void testIsValidShouldReturnFalseWhenNullPassed() {
        assertFalse(checkExistingValidator.isValid(null, constraintValidatorContext));
    }
    
    @Test
    void testIsValidShouldReturnFalseWhenNonSupportedTypePassed() {
        assertFalse(checkExistingValidator.isValid(new Object(), constraintValidatorContext));
    }

    @Test
    void testIsValidShouldReturnFalseWhenAuditoriumWithTheSameRoomNumberAsPassedAlreadyExists() {
        Mockito.when(auditoriumRepository.getByRoomNumber(Mockito.anyInt())).thenReturn(existingAuditorium);
        assertFalse(checkExistingValidator.isValid(testAuditorium, constraintValidatorContext));
    }
    
    @Test
    void testIsValidShouldReturnTrueWhenAuditoriumWithTheSameRoomNumberAsPassedNonExists() {
        when(auditoriumRepository.getByRoomNumber(anyInt())).thenReturn(null);
        assertTrue(checkExistingValidator.isValid(testAuditorium, constraintValidatorContext));
    }
    
    @Test
    void testIsValidShouldReturnTrueWhenPassedAuditoriumWithTheSameIdAsExisting() {
        testAuditorium.setAuditoriumId(1L);
        when(auditoriumRepository.getByRoomNumber(anyInt())).thenReturn(existingAuditorium);
        assertTrue(checkExistingValidator.isValid(testAuditorium, constraintValidatorContext));
    }
    
    @Test
    void testIsValidShouldReturnFalseWhenCourseWithTheSameNameAsPassedAlreadyExists() {
        when(courseRepository.getByCourseName(anyString())).thenReturn(existingCourse);
        assertFalse(checkExistingValidator.isValid(testCourse, constraintValidatorContext));
    }
    
    @Test
    void testIsValidShouldReturnTrueWhenCourseWithTheSameNameAsPassedNonExists() {
        when(courseRepository.getByCourseName(anyString())).thenReturn(null);
        assertTrue(checkExistingValidator.isValid(testCourse, constraintValidatorContext));
    }
    
    @Test
    void testIsValidShouldReturnTrueWhenPassedCourseWithTheSameIdAsExisting() {
        testCourse.setCourseId(1L);
        when(courseRepository.getByCourseName(anyString())).thenReturn(existingCourse);
        assertTrue(checkExistingValidator.isValid(testCourse, constraintValidatorContext));
    }
    
    @Test
    void testIsValidShouldReturnFalseWhenGroupWithTheSameNameAsPassedAlreadyExists() {
        when(groupRepository.getByGroupName(anyString())).thenReturn(existingGroup);
        assertFalse(checkExistingValidator.isValid(testGroup, constraintValidatorContext));
    }
    
    @Test
    void testIsValidShouldReturnTrueWhenGroupWithTheSameNameAsPassedNonExists() {
        when(groupRepository.getByGroupName(anyString())).thenReturn(null);
        assertTrue(checkExistingValidator.isValid(testGroup, constraintValidatorContext));
    }
    
    @Test
    void testIsValidShouldReturnTrueWhenPassedGroupWithTheSameIdAsExisting() {
        testGroup.setGroupId(1L);
        when(groupRepository.getByGroupName(anyString())).thenReturn(existingGroup);
        assertTrue(checkExistingValidator.isValid(testGroup, constraintValidatorContext));
    }
    
    @Test
    void testIsValidShouldReturnFalseWhenDateTimeWithTheSameNameAsPassedAlreadyExistsOrOverlappedWithExisting() {
        DateTime existingDateTime1 = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        DateTime testDateTime1 = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        DateTime existingDateTime2 = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(9, 0), LocalTime.of(9, 45));
        DateTime testDateTime2 = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(9, 10), LocalTime.of(9, 55));
        DateTime existingDateTime3 = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(10, 0), LocalTime.of(10, 45));
        DateTime testDateTime3 = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(9, 50), LocalTime.of(10, 35));
        when(dateTimeRepository.findAllByLessonDate(any(LocalDate.class)))
        .thenReturn(Arrays.asList(existingDateTime1, existingDateTime2, existingDateTime3));
        assertFalse(checkExistingValidator.isValid(testDateTime1, constraintValidatorContext));
        assertFalse(checkExistingValidator.isValid(testDateTime2, constraintValidatorContext));
        assertFalse(checkExistingValidator.isValid(testDateTime3, constraintValidatorContext));
    }
    
    @Test
    void testIsValidShouldReturnTrueWhenDateTimeWithTheSameNameAsPassedNonExists() {
        DateTime existingDateTime = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        DateTime testDateTime = new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(6, 0), LocalTime.of(6, 45));
        when(dateTimeRepository.findAllByLessonDate(any(LocalDate.class))).thenReturn(Collections.singletonList(existingDateTime));
        assertTrue(checkExistingValidator.isValid(testDateTime, constraintValidatorContext));
    }
}
