package com.foxminded.university.domain.formatters;

import static org.junit.jupiter.api.Assertions.*;

import java.text.ParseException;
import java.util.Locale;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.foxminded.university.domain.model.Course;

class CourseFormatterTest {
    
    private CourseFormatter courseFormatter;
    private Course testCourse;
    
    @BeforeEach
    void setUp() throws Exception {
    courseFormatter = new CourseFormatter();
    testCourse = new Course(1, "testCourse", "description");
    }

    @Test
    void testPrintShouldReturnFormatStringObjectCourseAccordingPattern() {
        String expected = "ID=1NAME=testCourse";
        assertEquals(expected, courseFormatter.print(testCourse, Locale.ENGLISH));
    }
    
    @Test
    void testPrintShouldReturnEmtyStringWhenNullObjectCoursePassed() {
        assertEquals("", courseFormatter.print(null, Locale.ENGLISH));
    }

    @Test
    void testParseShouldReturnCourseObjectWhenStringCoursePassed() throws ParseException {
        assertEquals(testCourse, courseFormatter.parse("ID=1NAME=testCourse", Locale.ENGLISH));
    }
    
    @Test
    void testParseShouldReturnEmptyCourseObjectWhenPassedStringDoesNotParse() throws ParseException {
        assertEquals(new Course(), courseFormatter.parse("WrongFormat", Locale.ENGLISH));
    }
}
