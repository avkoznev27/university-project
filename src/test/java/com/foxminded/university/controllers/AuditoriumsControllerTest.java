package com.foxminded.university.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static java.util.Collections.singletonList;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;

import com.foxminded.university.dao.AuditoriumRepository;
import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.dao.DateTimeRepository;
import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.AuditoriumService;

@WebMvcTest(controllers = AuditoriumsController.class)
class AuditoriumsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AuditoriumService auditoriumService;
    
    @MockBean
    private AuditoriumRepository auditoriumRepository;
    @MockBean
    private CourseRepository courseRepository;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private DateTimeRepository dateTimeRepository;
    
    private Auditorium testAuditorium;
    private Lesson testLesson;
    
    @BeforeEach
    public void setUp() throws Exception {
        testAuditorium = new Auditorium(1, 10, 100);
        testLesson = new Lesson();
        testLesson.setAuditorium(testAuditorium);
        testLesson.setCourse(new Course(1, "course1", "description"));
        testLesson.setDateTime(new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)));
        testLesson.setGroup(new Group(1, "group"));
        testLesson.setTeacher(new Teacher(1, "teacher"));
    }
    
    @Test
    void testGetAllAuditoriumsShouldAddAuditoriumsEntriesToModelAndRenderAuditoriumsListView() throws Exception {
        Auditorium testAuditorium1 = new Auditorium(1, 10, 25);
        Auditorium testAuditorium2 = new Auditorium(2, 20, 35);
        when(auditoriumService.getAllAuditoriums()).thenReturn(Arrays.asList(testAuditorium1, testAuditorium2));
        mockMvc.perform(get("/auditoriums/auditoriums-list"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/auditoriums-list"))
                .andExpect(model().attribute("auditoriums", hasSize(2)))
                .andExpect(model().attribute("auditoriums", hasItem(
                        allOf(
                                hasProperty("auditoriumId", equalTo(1L)), 
                                hasProperty("roomNumber", equalTo(10)),
                                hasProperty("capacity", equalTo(25))))))
                .andExpect(model().attribute("auditoriums", hasItem(
                        allOf(
                                hasProperty("auditoriumId", equalTo(2L)), 
                                hasProperty("roomNumber", equalTo(20)),
                                hasProperty("capacity", equalTo(35))))));
        verify(auditoriumService, times(1)).getAllAuditoriums();
        verifyNoMoreInteractions(auditoriumService);
    }
    
    @Test
    void testNewAuditoriumShouldAddEmtyAuditoriumEntryToModelAndRenderNewAuditoriumView() throws Exception {
        Auditorium emptyAuditorium = new Auditorium();
        mockMvc.perform(get("/auditoriums/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/new"))
                .andExpect(model().attribute("auditorium", emptyAuditorium));     
    }
    
    @Test
    void testCreateAuditoriumShouldReturnNewAuditoriumPageWithErrorMessageWhenExistingAuditoriumPosted() throws Exception {
        when(auditoriumRepository.getByRoomNumber(10)).thenReturn(testAuditorium);
        BindingResult bindingResult = (BindingResult) mockMvc.perform(post("/auditoriums/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("roomNumber", "10")
                .param("capacity", "100"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/new"))
                .andExpect(model().attributeHasErrors("auditorium"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "auditorium");
        assertEquals("Auditorium with this room number already exist", bindingResult.getGlobalError().getDefaultMessage());
    }
    
    @Test
    void testCreateAuditoriumShouldReturnNewAuditoriumPageWithErrorMessageWhenAuditoriumWithNotValidRoomNumberPosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(post("/auditoriums/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("roomNumber", "-10")
                .param("capacity", "100"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/new"))
                .andExpect(model().attributeHasErrors("auditorium"))
                .andExpect(model().attributeHasFieldErrorCode("auditorium", "roomNumber", "Positive"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "auditorium");
        assertEquals("must be greater than 0", bindingResult.getFieldError("roomNumber").getDefaultMessage());
        
        BindingResult bindingResult1 = (BindingResult) mockMvc.perform(post("/auditoriums/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("roomNumber", "1000")
                .param("capacity", "100"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/new"))
                .andExpect(model().attributeHasErrors("auditorium"))
                .andExpect(model().attributeHasFieldErrorCode("auditorium", "roomNumber", "Max"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "auditorium");
        assertEquals("Room number cannot be more than 999", bindingResult1.getFieldError("roomNumber").getDefaultMessage());
    }
    
    @Test
    void testCreateAuditoriumShouldReturnNewAuditoriumPageWithErrorMessageWhenAuditoriumWithNotValidCapacityPosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(post("/auditoriums/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("roomNumber", "10")
                .param("capacity", "-100"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/new"))
                .andExpect(model().attributeHasErrors("auditorium"))
                .andExpect(model().attributeHasFieldErrorCode("auditorium", "capacity", "Positive"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "auditorium");
        assertEquals("must be greater than 0", bindingResult.getFieldError("capacity").getDefaultMessage());
        
        BindingResult bindingResult1 = (BindingResult) mockMvc.perform(post("/auditoriums/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("roomNumber", "100")
                .param("capacity", "1000"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/new"))
                .andExpect(model().attributeHasErrors("auditorium"))
                .andExpect(model().attributeHasFieldErrorCode("auditorium", "capacity", "Max"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "auditorium");
        assertEquals("Capacity cannot be more than 200", bindingResult1.getFieldError("capacity").getDefaultMessage());
    }
    
    @Test
    void testCreateAuditoriumShouldAddAuditoriumEntryAndRedirectToAuditoriumsListViewWithSuccessMessage() throws Exception {
        when(auditoriumService.addAuditorium(Mockito.any(Auditorium.class))).thenReturn(testAuditorium);
        mockMvc.perform(post("/auditoriums/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("roomNumber", "10")
                .param("capacity", "100"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/auditoriums/auditoriums-list"))
                .andExpect(redirectedUrl("/auditoriums/auditoriums-list"))
                .andExpect(flash().attributeExists("success"));
        ArgumentCaptor<Auditorium> formObjectArgument = ArgumentCaptor.forClass(Auditorium.class);
        verify(auditoriumService, times(1)).addAuditorium(formObjectArgument.capture());
        verifyNoMoreInteractions(auditoriumService);
        Auditorium formObject = formObjectArgument.getValue();
        assertEquals(formObject.getAuditoriumId(), 0);
        assertEquals(formObject.getRoomNumber(), 10);
        assertEquals(formObject.getCapacity(), 100);
    }
    
    @Test
    void testShowAuditoriumShouldAddAuditoriumAndLessonsForAuditoriumEntriesToModelAndRenderAuditoriumPageView() throws Exception {
        testAuditorium.setLessons(singletonList(testLesson));
        when(auditoriumService.getAuditoriumById(anyLong())).thenReturn(testAuditorium);
        mockMvc.perform(get("/auditoriums/{id}", testAuditorium.getAuditoriumId()))
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/auditorium-page"))
                .andExpect(model().attribute("auditorium", testAuditorium));
        verify(auditoriumService, times(1)).getAuditoriumById(testAuditorium.getAuditoriumId());
    }
    
    @Test
    void testUpdateAuditoriumsShouldUpdateAuditoriumEntryAndRedirectToAuditoriumsListViewWithSuccessMessage() throws Exception {
        when(auditoriumRepository.getByRoomNumber(10)).thenReturn(testAuditorium);
        mockMvc.perform(patch("/auditoriums/{id}", testAuditorium.getAuditoriumId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("roomNumber", "10")
                .param("capacity", "100")
                .sessionAttr("auditorium", testAuditorium))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/auditoriums/auditoriums-list"))
                .andExpect(redirectedUrl("/auditoriums/auditoriums-list"))
                .andExpect(flash().attributeExists("success"));
        verify(auditoriumService, times(1)).updateAuditorium(Mockito.any(Auditorium.class));
        verifyNoMoreInteractions(auditoriumService);
    }
    
    @Test
    void testUpdateAuditoriumShouldReturnAuditoriumPageWithErrorMessageWhenExistingAuditoriumPosted() throws Exception {
        Auditorium existingAuditorium = new Auditorium(2, 10, 100);
        testAuditorium.setLessons(singletonList(testLesson));
        when(auditoriumRepository.getByRoomNumber(10)).thenReturn(existingAuditorium);
        BindingResult bindingResult = (BindingResult) mockMvc.perform(patch("/auditoriums/{id}", testAuditorium.getAuditoriumId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("roomNumber", "10")
                .param("capacity", "100")
                .sessionAttr("auditorium", testAuditorium))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/auditorium-page"))
                .andExpect(model().attributeHasErrors("auditorium"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "auditorium");
        assertEquals("Auditorium with this room number already exist", bindingResult.getGlobalError().getDefaultMessage());
    }
    
    @Test
    void testUpdateAuditoriumShouldReturnAuditoriumPageWithErrorMessageWhenAuditoriumWithNotValidRoomNumberPosted() throws Exception {
        testAuditorium.setLessons(singletonList(testLesson));
        BindingResult bindingResult = (BindingResult) mockMvc.perform(patch("/auditoriums/{id}", testAuditorium.getAuditoriumId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("roomNumber", "-10")
                .param("capacity", "100")
                .sessionAttr("auditorium", testAuditorium))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/auditorium-page"))
                .andExpect(model().attributeHasErrors("auditorium"))
                .andExpect(model().attributeHasFieldErrorCode("auditorium", "roomNumber", "Positive"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "auditorium");
        assertEquals("must be greater than 0", bindingResult.getFieldError("roomNumber").getDefaultMessage());
        
        BindingResult bindingResult1 = (BindingResult) mockMvc.perform(patch("/auditoriums/{id}", testAuditorium.getAuditoriumId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("roomNumber", "1000")
                .param("capacity", "100")
                .sessionAttr("auditorium", testAuditorium))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/auditorium-page"))
                .andExpect(model().attributeHasErrors("auditorium"))
                .andExpect(model().attributeHasFieldErrorCode("auditorium", "roomNumber", "Max"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "auditorium");
        assertEquals("Room number cannot be more than 999", bindingResult1.getFieldError("roomNumber").getDefaultMessage());
    }
    
    @Test
    void testUpdateAuditoriumShouldReturnAuditoriumPageWithErrorMessageWhenAuditoriumWithNotValidCapacityPosted() throws Exception {
        testAuditorium.setLessons(singletonList(testLesson));
        BindingResult bindingResult = (BindingResult) mockMvc.perform(patch("/auditoriums/{id}", testAuditorium.getAuditoriumId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("roomNumber", "10")
                .param("capacity", "-100")
                .sessionAttr("auditorium", testAuditorium))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/auditorium-page"))
                .andExpect(model().attributeHasErrors("auditorium"))
                .andExpect(model().attributeHasFieldErrorCode("auditorium", "capacity", "Positive"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "auditorium");
        assertEquals("must be greater than 0", bindingResult.getFieldError("capacity").getDefaultMessage());
        
        BindingResult bindingResult1 = (BindingResult) mockMvc.perform(patch("/auditoriums/{id}", testAuditorium.getAuditoriumId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("roomNumber", "100")
                .param("capacity", "1000")
                .sessionAttr("auditorium", testAuditorium))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/auditorium-page"))
                .andExpect(model().attributeHasErrors("auditorium"))
                .andExpect(model().attributeHasFieldErrorCode("auditorium", "capacity", "Max"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "auditorium");
        assertEquals("Capacity cannot be more than 200", bindingResult1.getFieldError("capacity").getDefaultMessage());
    }
    
    @Test
    void testDeleteAuditoriumShouldDeleteAuditoriumEntryAndRedirectToAuditoriumsListViewWithSuccessMessage() throws Exception {
        mockMvc.perform(delete("/auditoriums/{id}", testAuditorium.getAuditoriumId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("auditoriumId", "1")
                .param("roomNumber", "10")
                .param("capacity", "100")
                .sessionAttr("auditorium", new Auditorium()))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/auditoriums/auditoriums-list"))
                .andExpect(redirectedUrl("/auditoriums/auditoriums-list"))
                .andExpect(flash().attributeExists("success"));
        verify(auditoriumService, times(1)).removeAuditorium(Mockito.any(Auditorium.class));
        verifyNoMoreInteractions(auditoriumService);
    }
    
    @Test
    void testDeleteAuditoriumShouldReturnAuditoriumPageWithErrorMessageWhenDeletionAuditoriumHasLessons() throws Exception {
        testAuditorium.setLessons(singletonList(testLesson));
        BindingResult bindingResult = (BindingResult) mockMvc.perform(delete("/auditoriums/{id}", testAuditorium.getAuditoriumId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .sessionAttr("auditorium", testAuditorium))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("auditoriums/auditorium-page"))
                .andExpect(model().attributeHasErrors("auditorium"))
                .andExpect(model().attributeHasFieldErrorCode("auditorium", "lessons", "Size"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "auditorium");
        assertEquals("Deletion is not possible. Auditorium has lessons. First remove lessons or change auditorium for lessons", 
                bindingResult.getFieldError("lessons").getDefaultMessage());
    }
}
