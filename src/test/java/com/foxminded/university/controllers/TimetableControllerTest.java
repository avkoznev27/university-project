package com.foxminded.university.controllers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static java.util.Collections.singletonList;
import static java.util.Collections.singleton;
import static java.util.Collections.emptyList;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;

import com.foxminded.university.dao.AuditoriumRepository;
import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.dao.DateTimeRepository;
import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.domain.formatters.CourseFormatter;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.AuditoriumService;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.service.DateTimeService;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.LessonService;
import com.foxminded.university.domain.service.StudentService;
import com.foxminded.university.domain.service.TeacherService;
import com.foxminded.university.exceptions.ServiceException;

@WebMvcTest(controllers = TimetableController.class)
class TimetableControllerTest {
   
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private FormattingConversionService conversionService;

    @MockBean
    private TeacherService teacherService;
    @MockBean
    private AuditoriumService auditoriumService;
    @MockBean
    private StudentService studentService;
    @MockBean
    private GroupService groupService;
    @MockBean
    private CourseService courseService;
    @MockBean
    private DateTimeService dateTimeService;
    @MockBean
    private LessonService lessonService;
    @MockBean
    private AuditoriumRepository auditoriumRepository;
    @MockBean
    private CourseRepository courseRepository;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private DateTimeRepository dateTimeRepository;

    
    private Auditorium testAuditorium;
    private Course testCourse;
    private DateTime testDateTime;
    private Group testGroup;
    private Teacher testTeacher;
    private Lesson testLesson;

    @BeforeEach
    public void setUp() throws Exception {
        conversionService.addFormatterForFieldType(Course.class, new CourseFormatter());
        testAuditorium = new Auditorium(1, 10, 100);
        testCourse = new Course(1, "testCourse", "description");
        testDateTime = new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        testGroup = new Group(1, "group");
        testTeacher = new Teacher(1, "teacher");
        testLesson = new Lesson();
        testLesson.setLessonId(1);
        testLesson.setAuditorium(testAuditorium);
        testLesson.setCourse(testCourse);
        testLesson.setDateTime(testDateTime);
        testLesson.setGroup(testGroup);
        testLesson.setTeacher(testTeacher);
    }

    @Test
    void testGetAllLessonsShouldAddLessonsEntriesToModelAndRenderLessonsListView() throws Exception {
        testGroup.setLessons(singletonList(testLesson));
        when(lessonService.getAllLessons()).thenReturn(singletonList(testLesson));
        when(groupService.getAllGroups()).thenReturn(singletonList(testGroup));
        when(dateTimeService.getAllDateTimes()).thenReturn(singletonList(testDateTime));
        when(dateTimeService.getAllDates()).thenReturn(singleton(LocalDate.of(2020, 9, 1)));
        mockMvc.perform(get("/timetable/timetable"))
                .andExpect(status().isOk())
                .andExpect(view().name("timetable/timetable"))
                .andExpect(model().attribute("allLessons", hasSize(1)))
                .andExpect(model().attribute("allLessons", hasItem(
                        allOf(
                                hasProperty("auditorium", equalTo(testAuditorium)), 
                                hasProperty("course", equalTo(testCourse)),
                                hasProperty("dateTime", equalTo(testDateTime)),
                                hasProperty("group", equalTo(testGroup)),
                                hasProperty("teacher", equalTo(testTeacher))))))
                .andExpect(model().attribute("allGroups", singletonList(testGroup)))
                .andExpect(model().attribute("allDates", singleton(LocalDate.of(2020, 9, 1))))
                .andExpect(model().attribute("for20200901", singletonList(testDateTime)))
                .andExpect(model().attribute("group1", singletonList(testDateTime)));   
        verify(lessonService, times(1)).getAllLessons();
        verify(groupService, times(1)).getAllGroups();
        verify(dateTimeService, times(1)).getAllDateTimes();
        verify(dateTimeService, times(1)).getAllDates();
        verifyNoMoreInteractions(lessonService);
    }
    
    @Test
    void testNewLessonShouldRenderNewLessonPageForRequestedGroupAndDateTimeWithListCourses() throws Exception {
        Lesson testLesson = new Lesson();
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        when(auditoriumService.getAllFreeAuditoriums(Mockito.any(DateTime.class), anyInt())).thenReturn(singletonList(testAuditorium));
        when(courseService.getAllCourses()).thenReturn(singletonList(testCourse));
        when(teacherService.getAllBusyTeachers(testDateTime)).thenReturn(emptyList());
        testLesson.setDateTime(testDateTime);
        testLesson.setGroup(testGroup);
        mockMvc.perform(get("/timetable/new/{dateTimeId}/{groupId}", testDateTime.getDateTimeId(), testGroup.getGroupId()))
                .andExpect(status().isOk())
                .andExpect(view().name("/timetable/new"))
                .andExpect(model().attribute("disableApplyButton", false))
                .andExpect(model().attribute("disableSaveButton", true))
                .andExpect(model().attribute("lesson", testLesson))
                .andExpect(model().attribute("freeAuditoriums", singletonList(testAuditorium)))
                .andExpect(model().attribute("allCourses", singletonList(testCourse)))
                .andExpect(model().attribute("dayOfLesson", testDateTime.getLessonDate()))
                .andExpect(model().attribute("noTeachersAtAll", "There are no available teachers at all"));        
    }
    
    @Test
    void testNewLessonShouldRenderNewLessonPageForRequestedGroupAndDateTimeWithoutNoTeacherMessageWhenThereareFreeTeachers() throws Exception {
        Lesson testLesson = new Lesson();
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        when(auditoriumService.getAllFreeAuditoriums(Mockito.any(DateTime.class), anyInt())).thenReturn(singletonList(testAuditorium));
        when(courseService.getAllCourses()).thenReturn(singletonList(testCourse));
        when(teacherService.getAllBusyTeachers(testDateTime)).thenReturn(singletonList(testTeacher));
        when(teacherService.countTeachers()).thenReturn(0L);
        testLesson.setDateTime(testDateTime);
        testLesson.setGroup(testGroup);
        mockMvc.perform(get("/timetable/new/{dateTimeId}/{groupId}", testDateTime.getDateTimeId(), testGroup.getGroupId()))
                .andExpect(status().isOk())
                .andExpect(view().name("/timetable/new"))
                .andExpect(model().attribute("disableApplyButton", false))
                .andExpect(model().attribute("disableSaveButton", true))
                .andExpect(model().attributeDoesNotExist("noTeachersAtAll"));
    }
    
    @Test
    void testNewLessonShouldRenderNewLessonPageForRequestedGroupAndDateTimeWithMessageWhenNoFreeTeachersAtAll() throws Exception {
        Lesson testLesson = new Lesson();
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        when(auditoriumService.getAllFreeAuditoriums(Mockito.any(DateTime.class), anyInt())).thenReturn(singletonList(testAuditorium));
        when(courseService.getAllCourses()).thenReturn(singletonList(testCourse));
        when(teacherService.getAllBusyTeachers(testDateTime)).thenReturn(singletonList(testTeacher));
        when(teacherService.countTeachers()).thenReturn(1L);
        testLesson.setDateTime(testDateTime);
        testLesson.setGroup(testGroup);
        mockMvc.perform(get("/timetable/new/{dateTimeId}/{groupId}", testDateTime.getDateTimeId(), testGroup.getGroupId()))
                .andExpect(status().isOk())
                .andExpect(view().name("/timetable/new"))
                .andExpect(model().attributeExists("noTeachersAtAll"));     
    }
    
    @Test
    void testNewLessonShouldRenderNewLessonPageForRequestedGroupAndDateTimeWithListsFreeAuditoriumsAndTeachersWhenCourseIsSelected() throws Exception {
        Lesson testLesson = new Lesson();
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        when(auditoriumService.getAllFreeAuditoriums(Mockito.any(DateTime.class), anyInt())).thenReturn(singletonList(testAuditorium));
        when(courseService.getAllCourses()).thenReturn(singletonList(testCourse));
        when(teacherService.getAllBusyTeachers(testDateTime)).thenReturn(emptyList());
        when(teacherService.getAllFreeTeachers(testDateTime, testCourse)).thenReturn(singletonList(testTeacher));
        testLesson.setDateTime(testDateTime);
        testLesson.setGroup(testGroup);
        testLesson.setCourse(testCourse);
        mockMvc.perform(get("/timetable/new/{dateTimeId}/{groupId}", testDateTime.getDateTimeId(), testGroup.getGroupId())
                .flashAttr("lesson", testLesson))
                .andExpect(status().isOk())
                .andExpect(view().name("/timetable/new"))
                .andExpect(model().attribute("disableApplyButton", true))
                .andExpect(model().attribute("disableSaveButton",false))
                .andExpect(model().attribute("freeAuditoriums", singletonList(testAuditorium)))
                .andExpect(model().attribute("availableTeachers", singletonList(testTeacher)))
                .andExpect(model().attribute("allCourses", singletonList(testCourse)))
                .andExpect(model().attribute("dayOfLesson", testDateTime.getLessonDate()));        
    }
    
    @Test
    void testNewLessonShouldRenderNewLessonPageForRequestedGroupAndDateTimeWithMessageWhenNoFreeTeachersForSelectedCourse() throws Exception {
        Lesson testLesson = new Lesson();
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        when(auditoriumService.getAllFreeAuditoriums(Mockito.any(DateTime.class), anyInt())).thenReturn(singletonList(testAuditorium));
        when(courseService.getAllCourses()).thenReturn(singletonList(testCourse));
        when(teacherService.getAllBusyTeachers(testDateTime)).thenReturn(emptyList());
        when(teacherService.getAllFreeTeachers(testDateTime, testCourse)).thenReturn(emptyList());
        testLesson.setDateTime(testDateTime);
        testLesson.setGroup(testGroup);
        testLesson.setCourse(testCourse);
        mockMvc.perform(get("/timetable/new/{dateTimeId}/{groupId}", testDateTime.getDateTimeId(), testGroup.getGroupId())
                .flashAttr("lesson", testLesson))
                .andExpect(status().isOk())
                .andExpect(view().name("/timetable/new"))
                .andExpect(model().attributeExists("noTeachers"));        
    }
    
    @Test
    void testCreateLessonShoudAddLessonAndRedirectingToTimetablePageWithSucceessMessage() throws Exception {
        when(lessonService.addLesson(Mockito.any(Lesson.class))).thenReturn(testLesson);
        mockMvc.perform(post("/timetable/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("course.courseId", "1")
                .param("auditorium.auditoriumId", "1")
                .param("group.groupId", "1")
                .param("teacher.teacherId", "1")
                .param("dateTime.dateTimeId", "1"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/timetable/timetable"))
                .andExpect(redirectedUrl("/timetable/timetable"))
                .andExpect(flash().attributeExists("success"));
    }
    
    @Test
    void testCreateLessonShoudRedirectingToNewLessonPageWhenCourseOrAuditoriumOrTeacherIsNotSelected() throws Exception {
        mockMvc.perform(post("/timetable/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("course.courseId", "1")
                .param("auditorium.auditoriumId", "1")
                .param("group.groupId", "1")
                .param("dateTime.dateTimeId", "1")
                .header("referer", "/timetable/new"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/timetable/new"))
                .andExpect(redirectedUrl("/timetable/new"))
                .andExpect(flash().attributeExists("lesson"));
    }
    
    @Test
    void testNewTimeShouldenderNewTimeView() throws Exception {
        mockMvc.perform(get("/timetable/new-time"))
                .andExpect(status().isOk())
                .andExpect(view().name("timetable/new-time"));     
    }
    
    @Test
    void testCreateDateTimeShouldAddDateTimeEntryAndRedirectToTimetableViewWithSuccessMessage() throws Exception {
        mockMvc.perform(post("/timetable/new-time")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("lessonDate", "2020-09-01")
                .param("startLesson", "08:00")
                .param("endLesson", "08:45"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/timetable/timetable"))
                .andExpect(redirectedUrl("/timetable/timetable"));
    }
    
    @Test
    void testCreateDateTimeShouldReturnNewDateTimeViewWithErrorMessageWhenNotWalidDataPosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(post("/timetable/new-time")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("lessonDate", " ")
                .param("startLesson", " ")
                .param("endLesson", " "))
                .andExpect(status().isOk())
                .andExpect(view().name("timetable/new-time"))
                .andExpect(model().attributeHasErrors("dateTime"))
                .andExpect(model().attributeHasFieldErrorCode("dateTime", "lessonDate", "NotNull"))
                .andExpect(model().attributeHasFieldErrorCode("dateTime", "startLesson", "NotNull"))
                .andExpect(model().attributeHasFieldErrorCode("dateTime", "endLesson", "NotNull"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "dateTime");
        assertEquals("Date must not be null", bindingResult.getFieldError("lessonDate").getDefaultMessage());
        assertEquals("Start lesson time must not be null", bindingResult.getFieldError("startLesson").getDefaultMessage());
        assertEquals("End lesson time must not be null", bindingResult.getFieldError("endLesson").getDefaultMessage());
    }
    
    @Test
    void testCreateDateTimeShouldReturnNewDateTimeViewWithErrorMessageWhenExistingDateTimeDataPosted() throws Exception {
        when(dateTimeRepository.findAllByLessonDate(any(LocalDate.class))).thenReturn(singletonList(testDateTime));
        mockMvc.perform(post("/timetable/new-time")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("lessonDate", "2020-09-01")
                .param("startLesson", "08:00")
                .param("endLesson", "08:45")
                .sessionAttr("dateTime", testDateTime))
                .andExpect(status().isOk())
                .andExpect(view().name("timetable/new-time"))
                .andExpect(model().attributeHasErrors("dateTime"));
    }
    
    @Test
    void testCreateDateTimeShouldRenderNewDateTimeViewWithServiceExceptionAttributeWhenSerwiceExceptionWasThrown() throws Exception {
        when(dateTimeService.addDateTime(Mockito.any(DateTime.class))).thenThrow(new ServiceException("Message"));
        mockMvc.perform(post("/timetable/new-time")
                .header("referer", "/timetable/new-time")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("lessonDate", "2020-09-01")
                .param("startLesson", "08:00")
                .param("endLesson", "08:45"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/timetable/new-time"))
                .andExpect(redirectedUrl("/timetable/new-time"))
                .andExpect(flash().attributeExists("serviceException"));
    }
    
    @Test
    void testShowDateTimeShouldAddDateTimeEntriesToModelAndRenderDateTimePageView() throws Exception {
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        mockMvc.perform(get("/timetable/edit-time/{dateId}", testDateTime.getDateTimeId()))
                .andExpect(status().isOk())
                .andExpect(view().name("timetable/edit-time"))
                .andExpect(model().attribute("dateTime", testDateTime));
    }
    
    @Test
    void testUpdateDateTimeShouldReturnNewDateTimeViewWithErrorMessageWhenNotWalidDataPosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(patch("/timetable/edit-time/{id}", testDateTime.getDateTimeId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("lessonDate", " ")
                .param("startLesson", " ")
                .param("endLesson", " "))
                .andExpect(status().isOk())
                .andExpect(view().name("timetable/edit-time"))
                .andExpect(model().attributeHasErrors("dateTime"))
                .andExpect(model().attributeHasFieldErrorCode("dateTime", "lessonDate", "NotNull"))
                .andExpect(model().attributeHasFieldErrorCode("dateTime", "startLesson", "NotNull"))
                .andExpect(model().attributeHasFieldErrorCode("dateTime", "endLesson", "NotNull"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "dateTime");
        assertEquals("Date must not be null", bindingResult.getFieldError("lessonDate").getDefaultMessage());
        assertEquals("Start lesson time must not be null", bindingResult.getFieldError("startLesson").getDefaultMessage());
        assertEquals("End lesson time must not be null", bindingResult.getFieldError("endLesson").getDefaultMessage());
    }
    
    @Test
    void testUpdateDateTimeShouldReturnNewDateTimeViewWithErrorMessageWhenExistingDateTimeDataPosted() throws Exception {
        when(dateTimeRepository.findAllByLessonDate(any(LocalDate.class))).thenReturn(singletonList(testDateTime));
        mockMvc.perform(patch("/timetable/edit-time/{id}", testDateTime.getDateTimeId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("lessonDate", "2020-09-01")
                .param("startLesson", "08:00")
                .param("endLesson", "08:45")
                .sessionAttr("dateTime", testDateTime))
                .andExpect(status().isOk())
                .andExpect(view().name("timetable/edit-time"))
                .andExpect(model().attributeHasErrors("dateTime"));
    }
    
    @Test
    void testUpdateDateTimeShouldUpdateDateTimeEntryAndRedirectToTimetableView() throws Exception {
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        mockMvc.perform(patch("/timetable/edit-time/{dateId}", testDateTime.getDateTimeId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("lessonDate", "2020-09-01")
                .param("startLesson", "08:00")
                .param("endLesson", "08:45")
                .sessionAttr("dateTime", testDateTime))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/timetable/timetable"))
                .andExpect(redirectedUrl("/timetable/timetable"));
        verify(dateTimeService, times(1)).updateDateTime(Mockito.any(DateTime.class));
        verifyNoMoreInteractions(dateTimeService);
    }
    
    @Test
    void testUpdateDateTimeShouldRenderDateTimePageViewWithServiceExceptionAttributeWhenSerwiceExceptionWasThrown() throws Exception {
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        doThrow(new ServiceException("Message")).when(dateTimeService).updateDateTime(testDateTime);
        mockMvc.perform(patch("/timetable/edit-time/{dateId}", testDateTime.getDateTimeId())
                .header("referer", "/timetable/dateTimes/1")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("lessonDate", "2020-09-01")
                .param("startLesson", "08:00")
                .param("endLesson", "08:45")
                .sessionAttr("dateTime", testDateTime))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/timetable/dateTimes/1"))
                .andExpect(redirectedUrl("/timetable/dateTimes/1"))
                .andExpect(flash().attributeExists("serviceException"));
    }
    
    @Test
    void testDeleteDateTimeShouldDeleteDateTimeEntryAndRenderTimetableView() throws Exception {
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        mockMvc.perform(delete("/timetable/edit-time/{dateId}", testDateTime.getDateTimeId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("dateTimeId", "1")
                .sessionAttr("dateTime", testDateTime))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/timetable/timetable"))
                .andExpect(redirectedUrl("/timetable/timetable"));
        verify(dateTimeService, times(1)).removeDateTime(Mockito.any(DateTime.class));
        verifyNoMoreInteractions(dateTimeService);
    }
    
    @Test
    void testDeleteDateTimeShouldReturnDateTimePageViewWithErrorMessageWhenDateTimeHasStudents() throws Exception {
        testDateTime.setLessons(singletonList(testLesson));
        BindingResult bindingResult = (BindingResult) mockMvc.perform(delete("/timetable/edit-time/{dateId}", testDateTime.getDateTimeId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("lessonDate", "2020-09-01")
                .param("startLesson", "08:00")
                .param("endLesson", "08:45")
                .sessionAttr("dateTime", testDateTime))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("timetable/edit-time"))
                .andExpect(model().attributeHasErrors("dateTime"))
                .andExpect(model().attributeHasFieldErrorCode("dateTime", "lessons", "Size"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "dateTime");
        assertEquals("Deletion is not possible. Date time has lessons. First remove lessons", 
                bindingResult.getFieldError("lessons").getDefaultMessage());
    } 
    
    @Test
    void testDeleteLessonShouldDeleteLessonEntryAndRenderTimetableViewWithSuccessMessage() throws Exception {
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        mockMvc.perform(delete("/timetable/timetable/{id}", testLesson.getLessonId()))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/timetable/timetable"))
                .andExpect(redirectedUrl("/timetable/timetable"))
                .andExpect(flash().attributeExists("success"));
        verify(lessonService, times(1)).removeLesson(Mockito.any(Lesson.class));
    }
    
    @Test
    void testDeleteLessonShouldRenderDateTimePageViewWithServiceExceptionAttributeWhenSerwiceExceptionWasThrown() throws Exception {
        when(lessonService.getLessonById(anyLong())).thenThrow(new ServiceException("Message"));
        mockMvc.perform(delete("/timetable/timetable/{id}", testLesson.getLessonId())
                .header("referer", "/timetable/timetable/1"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/timetable/timetable/1"))
                .andExpect(redirectedUrl("/timetable/timetable/1"))
                .andExpect(flash().attributeExists("serviceException"));
    }
}
