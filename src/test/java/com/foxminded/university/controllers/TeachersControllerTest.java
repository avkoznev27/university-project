package com.foxminded.university.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;

import com.foxminded.university.domain.formatters.CourseFormatter;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.service.TeacherService;

@WebMvcTest(controllers = TeachersController.class)
class TeachersControllerTest {
    
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private FormattingConversionService conversionService;
    @MockBean
    private TeacherService teacherService;
    @MockBean
    private CourseService courseService;

    private Course testCourse;
    private Teacher testTeacher1;
    private Teacher testTeacher2;
    private Lesson testLesson;

    @BeforeEach
    public void setUp() throws Exception {
        conversionService.addFormatterForFieldType(Course.class, new CourseFormatter());
        testCourse = new Course(1, "course1", null);
        testTeacher1 = new Teacher(1, "teacher1");
        testTeacher2 = new Teacher(2, "teacher2");
        testTeacher1.getCourses().add(testCourse);
        testTeacher2.getCourses().add(testCourse);
        testLesson = new Lesson();
        testLesson.setAuditorium(new Auditorium(1, 10, 100));
        testLesson.setCourse(new Course(1, "course", "description"));
        testLesson.setDateTime(new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)));
        testLesson.setGroup(new Group(1, "testGroup"));
        testLesson.setTeacher(new Teacher(1, "teacher"));
    }

    @Test
    void testGetAllTeachersShouldAddTeachersEntriesToModelAndRenderTeachersListView() throws Exception {
        when(teacherService.getAllTeachers()).thenReturn(Arrays.asList(testTeacher1, testTeacher2));
        mockMvc.perform(get("/teachers/teachers-list"))
                .andExpect(status().isOk())
                .andExpect(view().name("teachers/teachers-list"))
                .andExpect(model().attribute("teachers", hasSize(2)))
                .andExpect(model().attribute("teachers", hasItem(
                        allOf(
                                hasProperty("teacherId", equalTo(1L)), 
                                hasProperty("name", equalTo("teacher1")),
                                hasProperty("courses", equalTo(singleton(testCourse)))))))
                .andExpect(model().attribute("teachers", hasItem(
                        allOf(
                                hasProperty("teacherId", equalTo(2L)), 
                                hasProperty("name", equalTo("teacher2")),
                                hasProperty("courses", equalTo(singleton(testCourse)))))));
        verify(teacherService, times(1)).getAllTeachers();
        verifyNoMoreInteractions(teacherService);
    }
    
    @Test
    void testNewTeacherShouldAddEmptyTeacherEntryAndAllCoursesToModelAndRenderNewTeacherView() throws Exception {
        Teacher emptyTeacher = new Teacher();
        when(courseService.getAllCourses()).thenReturn(singletonList(testCourse));
        mockMvc.perform(get("/teachers/new"))
               .andExpect(status().isOk())
               .andExpect(view().name("teachers/new"))
               .andExpect(model().attribute("teacher", emptyTeacher))
               .andExpect(model().attribute("allCourses", singletonList(testCourse)));
    }
    
    @Test
    void testCreateTeacherShouldReturnNewTeacherViewWithErrorMessageWhenNotValidNamePosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(post("/teachers/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("name", "   ")
                .param("courses", "ID=1NAME=course1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeHasErrors("teacher"))
                .andExpect(model().attributeHasFieldErrorCode("teacher", "name", "NotBlank"))
                .andExpect(view().name("teachers/new"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "teacher");
        assertEquals("Teacher name must not be empty", bindingResult.getFieldError("name").getDefaultMessage());
    }
    
    @Test
    void testCreateTeacherShouldAddTeacherEntryAndRedirectToTeachersListViewWithSuccessMessage() throws Exception {
        Teacher createdTeacher = new Teacher("teacher1");
        Course createdCourse = new Course(1, "course1", null);
        createdTeacher.getCourses().add(createdCourse);
        when(teacherService.addTeacher(createdTeacher)).thenReturn(testTeacher1);
        mockMvc.perform(post("/teachers/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("name", "teacher1")
                .param("courses", "ID=1NAME=course1"))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/teachers/teachers-list"))
                .andExpect(redirectedUrl("/teachers/teachers-list"))
                .andExpect(flash().attributeExists("success"));
        ArgumentCaptor<Teacher> formObjectArgument = ArgumentCaptor.forClass(Teacher.class);
        verify(teacherService, times(1)).addTeacher(formObjectArgument.capture());
        verifyNoMoreInteractions(teacherService);
        Teacher formObject = formObjectArgument.getValue();
        assertEquals(0, formObject.getTeacherId());
        assertEquals("teacher1", formObject.getName());
    }
    
    @Test
    void testShowTeacherShouldAddTeacherAndLessonsForTeacherEntriesToModelAndRenderTeacherPageView() throws Exception {
        testLesson.setTeacher(testTeacher1);
        testTeacher1.setLessons(singletonList(testLesson));
        when(teacherService.getTeacherById(anyLong())).thenReturn(testTeacher1);
        when(courseService.getAllCourses()).thenReturn(singletonList(testCourse));
        mockMvc.perform(get("/teachers/{id}", testTeacher1.getTeacherId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("teachers/teacher-page"))
                .andExpect(model().attribute("teacher", testTeacher1))
                .andExpect(model().attribute("allCourses", hasSize(1)))
                .andExpect(model().attribute("allCourses", hasItem(testCourse)));
        verify(teacherService, times(1)).getTeacherById(testTeacher1.getTeacherId());
        verify(courseService, times(1)).getAllCourses();
    }
    
    @Test
    void testUpdateTeacherShouldReturnTeacherPageViewWithErrorMessageWhenNotValidNamePosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(patch("/teachers/{id}", testTeacher1.getTeacherId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("name", "   ")
                .param("courses", "ID=1NAME=course1")
                .sessionAttr("teacher", testTeacher1)
                .sessionAttr("teacherLessons", singleton(testLesson))
                .sessionAttr("allCourses", singletonList(testCourse)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeHasErrors("teacher"))
                .andExpect(model().attributeHasFieldErrorCode("teacher", "name", "NotBlank"))
                .andExpect(view().name("teachers/teacher-page"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "teacher");
        assertEquals("Teacher name must not be empty", bindingResult.getFieldError("name").getDefaultMessage());
    }
    
    @Test
    void testUpdateTeachersShouldUpdateTeacherEntryAndRenderTeachersListView() throws Exception {
        mockMvc.perform(patch("/teachers/{id}", testTeacher1.getTeacherId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("name", "teacher1")
                .param("courses", "ID=1NAME=course1")
                .sessionAttr("teacher", testTeacher1))
                .andExpect(status().isFound()).andExpect(view().name("redirect:/teachers/teachers-list"))
                .andExpect(redirectedUrl("/teachers/teachers-list"))
                .andExpect(flash().attributeExists("success"));
        verify(teacherService, times(1)).updateTeacher(Mockito.any(Teacher.class));
        verifyNoMoreInteractions(teacherService);
    }
    
    @Test
    void testDeleteTeacherShouldDeleteTeacherEntryAndRenderTeachersListView() throws Exception {
        mockMvc.perform(delete("/teachers/{id}", testTeacher1.getTeacherId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("teacherId", "1")
                .param("name", "teacher1")
                .param("courses", "ID=1NAME=course1")
                .sessionAttr("teacher", new Teacher()))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/teachers/teachers-list"))
                .andExpect(redirectedUrl("/teachers/teachers-list"))
                .andExpect(flash().attributeExists("success"));
        verify(teacherService, times(1)).removeTeacher(Mockito.any(Teacher.class));
        verifyNoMoreInteractions(teacherService);
    }
    
    @Test
    void testDeleteTeacherShouldReturnTeacherPageViewWithErrorMessageWhenTeacherHasLessons() throws Exception {
        testTeacher1.setLessons(Collections.singletonList(testLesson));
        BindingResult bindingResult = (BindingResult) mockMvc.perform(delete("/teachers/{id}", testTeacher1.getTeacherId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("teacherId", "1")
                .param("teacherName", "teacher")
                .param("teacherDescription", "description")
                .sessionAttr("teacher", testTeacher1))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("teachers/teacher-page"))
                .andExpect(model().attributeHasErrors("teacher"))
                .andExpect(model().attributeHasFieldErrorCode("teacher", "lessons", "Size"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "teacher");
        assertEquals("Deletion is not possible. Teacher has lessons. First remove lessons", 
                bindingResult.getFieldError("lessons").getDefaultMessage());
    }
    
    
    @Test
    void testCreateTeacherTimetableShouldRenderTeacherTimetableViewForDayWhenRequestParameterIsDay() throws Exception {
        when(teacherService.getTeacherById(anyLong())).thenReturn(testTeacher1);
        when(teacherService.getTeacherTimetable(Mockito.any(Teacher.class), Mockito.any(LocalDate.class)))
        .thenReturn(singletonList(testLesson));
        mockMvc.perform(post("/teachers/teacher-timetable")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("teacherId", "1")
                .param("startDay", "2020-09-01")
                .param("endDay", ""))
                .andExpect(status().isOk())
                .andExpect(view().name("teachers/teacher-timetable"))
                .andExpect(model().attributeExists("teacher"))
                .andExpect(model().attributeExists("startDay"))
                .andExpect(model().attributeExists("endDay"))
                .andExpect(model().attributeExists("teacherTimetable"))
                .andExpect(model().attribute("dailyTimetable", true));
    }
    
    @Test
    void testCreateTeacherTimetableShouldRenderTeacherTimetableViewForPeriodWhenRequestParameterIsPeriodOfDays() throws Exception {
        when(teacherService.getTeacherById(anyLong())).thenReturn(testTeacher1);
        when(teacherService.getTeacherTimetable(Mockito.any(Teacher.class), 
                Mockito.any(LocalDate.class), Mockito.any(LocalDate.class))).thenReturn(singletonList(testLesson));
        mockMvc.perform(post("/teachers/teacher-timetable")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("teacherId", "1")
                .param("startDay", "2020-09-01")
                .param("endDay", "2020-09-10"))
                .andExpect(status().isOk())
                .andExpect(view().name("teachers/teacher-timetable"))
                .andExpect(model().attributeExists("teacher"))
                .andExpect(model().attributeExists("startDay"))
                .andExpect(model().attributeExists("endDay"))
                .andExpect(model().attributeExists("teacherTimetable"))
                .andExpect(model().attribute("dailyTimetable", false));
    }
}
