package com.foxminded.university.controllers;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;

import com.foxminded.university.domain.model.Settings;
import com.foxminded.university.domain.service.SettingsService;

@WebMvcTest(controllers = SettingsController.class)
class SettingsControllerTest {
    
    @Autowired
    private MockMvc mockMvc;
    
    @MockBean
    private SettingsService settingsService;
    
    private Settings defaultSettings;
    private Settings actualSettings;

    @BeforeEach
    void setUp() throws Exception {
        defaultSettings = new Settings(1, 45, 30);
        actualSettings = new Settings(1, 90, 60);
    }

    @Test
    void testShowSettingsShouldReturnSettingsPageWithActualSettingsParameters() throws Exception {
        when(settingsService.getSettings()).thenReturn(actualSettings);
        mockMvc.perform(get("/settings-page"))
                .andExpect(status().isOk())
                .andExpect(view().name("settings"))
                .andExpect(model().attribute("settings", actualSettings))
                .andDo(print());
    }
    
    @Test
    void testSetDefaultShouldReturnSettingsPageWithDefaultSettingsParameters() throws Exception {
        when(settingsService.setDefaultSettings()).thenReturn(defaultSettings);
        mockMvc.perform(post("/settings-page/set-default"))
                .andExpect(status().isOk())
                .andExpect(view().name("settings"))
                .andExpect(model().attribute("settings", defaultSettings))
                .andDo(print());
    }
    
    @Test
    void testUpdateShouldReturnSettingsPageWithErrorMessageWhenNotValidParametersPosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(patch("/settings-page/save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("lessonDuration", "-10")
                .param("maxGroupSize", "100")
                .sessionAttr("settings", actualSettings))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("settings"))
                .andExpect(model().attributeHasErrors("settings"))
                .andExpect(model().attributeHasFieldErrorCode("settings", "lessonDuration", "Positive"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "settings");
        assertEquals("must be greater than 0", bindingResult.getFieldError("lessonDuration").getDefaultMessage());
        
        BindingResult bindingResult2 = (BindingResult) mockMvc.perform(patch("/settings-page/save")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("lessonDuration", "10")
                .param("maxGroupSize", "-100")
                .sessionAttr("settings", actualSettings))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("settings"))
                .andExpect(model().attributeHasErrors("settings"))
                .andExpect(model().attributeHasFieldErrorCode("settings", "maxGroupSize", "Positive"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "settings");
        assertEquals("must be greater than 0", bindingResult2.getFieldError("maxGroupSize").getDefaultMessage());
    }
    
    @Test
    void testUpdateShouldReturnSettingsPageWithActualSettingsWhenValidParametersPosted() throws Exception {
        mockMvc.perform(patch("/settings-page/save")
                .param("id", "1")
                .param("lessonDuration", "90")
                .param("maxGroupSize", "60")
                .sessionAttr("settings", actualSettings))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("settings"))
                .andExpect(model().attribute("settings", actualSettings));
    }
}
