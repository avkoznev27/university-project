package com.foxminded.university.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;

import com.foxminded.university.dao.AuditoriumRepository;
import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.dao.DateTimeRepository;
import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.GroupService;

@WebMvcTest(controllers = GroupsController.class)
class GroupsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GroupService groupService;
    
    @MockBean
    private AuditoriumRepository auditoriumRepository;
    @MockBean
    private CourseRepository courseRepository;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private DateTimeRepository dateTimeRepository;
    
    private Group testGroup1;
    private Group testGroup2;
    private Student testStudent1;
    private Student testStudent2;
    private Lesson testLesson;

    @BeforeEach
    public void setUp() throws Exception {
        testGroup1 = new Group(1, "group1");
        testGroup2 = new Group(2, "group2");
        testStudent1 = new Student(1, 1, "student1");
        testStudent2 = new Student(2, 2, "student2");
        testGroup1.getStudents().add(testStudent1);
        testGroup2.getStudents().add(testStudent2);
        testLesson = new Lesson();
        testLesson.setAuditorium(new Auditorium(1, 10, 100));
        testLesson.setCourse(new Course(1, "course", "description"));
        testLesson.setDateTime(new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)));
        testLesson.setGroup(testGroup1);
        testLesson.setTeacher(new Teacher(1, "teacher"));
    }

    @Test
    void testGetAllGroupsShouldAddGroupsEntriesToModelAndRenderGroupsListView() throws Exception {
        when(groupService.getAllGroups()).thenReturn(Arrays.asList(testGroup1, testGroup2));
        mockMvc.perform(get("/groups/groups-list"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("groups/groups-list"))
                .andExpect(model().attribute("groups", hasSize(2)))
                .andExpect(model().attribute("groups", hasItem(
                        allOf(
                                hasProperty("groupId", equalTo(1L)), 
                                hasProperty("groupName", equalTo("group1")),
                                hasProperty("students", equalTo(Collections.singletonList(testStudent1)))))))
                .andExpect(model().attribute("groups", hasItem(
                        allOf(
                                hasProperty("groupId", equalTo(2L)), 
                                hasProperty("groupName", equalTo("group2")),
                                hasProperty("students", equalTo(Collections.singletonList(testStudent2)))))));
        verify(groupService, times(1)).getAllGroups();
        verifyNoMoreInteractions(groupService);
    }
    
    @Test
    void testNewGroupShouldAddEmtyGroupEntryToModelAndRenderNewGroupView() throws Exception {
        Group emptyGroup = new Group();
        mockMvc.perform(get("/groups/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("groups/new"))
                .andExpect(model().attribute("group", emptyGroup));     
    }
    
    @Test
    void testCreateGroupShouldReturnNewGroupPageWithErrorMessageWhenExistingGroupPosted() throws Exception {
        when(groupRepository.getByGroupName("group")).thenReturn(testGroup1);
        BindingResult bindingResult = (BindingResult) mockMvc.perform(post("/groups/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupName", "group"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("groups/new"))
                .andExpect(model().attributeHasErrors("group"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "group");
        assertEquals("Group with the same name already exists", bindingResult.getGlobalError().getDefaultMessage());
    }
    
    @Test
    void testCreateGroupShouldReturnNewGroupPageWithErrorMessageWhenGroupWithNotValidGroupNamePosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(post("/groups/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupName", "    "))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("groups/new"))
                .andExpect(model().attributeHasErrors("group"))
                .andExpect(model().attributeHasFieldErrorCode("group", "groupName", "NotBlank"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "group");
        assertEquals("Group name must not be empty", bindingResult.getFieldError("groupName").getDefaultMessage());
    }
    
    @Test
    void testCreateGroupShouldAddGroupEntryAndRedirectToGroupsListViewWithSuccessMessage() throws Exception {
        when(groupService.addGroup(Mockito.any(Group.class))).thenReturn(testGroup1);
        mockMvc.perform(post("/groups/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupName", "group"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/groups/groups-list"))
                .andExpect(redirectedUrl("/groups/groups-list"))
                .andExpect(flash().attributeExists("success"));
        ArgumentCaptor<Group> formObjectArgument = ArgumentCaptor.forClass(Group.class);
        verify(groupService, times(1)).addGroup(formObjectArgument.capture());
        verifyNoMoreInteractions(groupService);
        Group formObject = formObjectArgument.getValue();
        assertEquals(formObject.getGroupId(), 0);
        assertEquals(formObject.getGroupName(), "group");
    }
    
    @Test
    void testShowGroupShouldAddGroupAndLessonsForGroupEntriesToModelAndRenderGroupPageView() throws Exception {
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup1);
        mockMvc.perform(get("/groups/{id}", testGroup1.getGroupId()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("groups/group-page"))
                .andExpect(model().attribute("group", testGroup1));
        verify(groupService, times(1)).getGroupById(testGroup1.getGroupId());
    }
    
    @Test
    void testUpdateGroupShouldReturnNewGroupPageWithErrorMessageWhenExistingGroupPosted() throws Exception {
        Group existingGroup = new Group(2, "group");
        testGroup1.setLessons(Collections.singletonList(testLesson));
        when(groupRepository.getByGroupName("group")).thenReturn(existingGroup);
        BindingResult bindingResult = (BindingResult) mockMvc.perform(patch("/groups/{id}", testGroup1.getGroupId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupName", "group")
                .sessionAttr("group", testGroup1))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("groups/group-page"))
                .andExpect(model().attributeHasErrors("group"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "group");
        assertEquals("Group with the same name already exists", bindingResult.getGlobalError().getDefaultMessage());
    }
    
    @Test
    void testUpdateGroupShouldReturnNewGroupPageWithErrorMessageWhenGroupWithNotValidGroupNamePosted() throws Exception {
        testGroup1.setLessons(Collections.singletonList(testLesson));
        BindingResult bindingResult = (BindingResult) mockMvc.perform(patch("/groups/{id}", testGroup1.getGroupId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupName", "    ")
                .sessionAttr("group", testGroup1))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("groups/group-page"))
                .andExpect(model().attributeHasErrors("group"))
                .andExpect(model().attributeHasFieldErrorCode("group", "groupName", "NotBlank"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "group");
        assertEquals("Group name must not be empty", bindingResult.getFieldError("groupName").getDefaultMessage());
    }
    
    @Test
    void testUpdateGroupsShouldUpdateGroupEntryAndRedirectToGroupsListViewWithSuccessMessage() throws Exception {
        when(groupRepository.getByGroupName("group")).thenReturn(testGroup1);
        mockMvc.perform(patch("/groups/{id}", testGroup1.getGroupId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupName", "group")
                .sessionAttr("group", testGroup1))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/groups/groups-list"))
                .andExpect(redirectedUrl("/groups/groups-list"))
                .andExpect(flash().attributeExists("success"));
        verify(groupService, times(1)).updateGroup(Mockito.any(Group.class));
        verifyNoMoreInteractions(groupService);
    }
    
    @Test
    void testDeleteGroupShouldDeleteGroupEntryAndRedirectToGroupsListViewWithSuccessMessage() throws Exception {
        testGroup1.getStudents().clear();
        mockMvc.perform(delete("/groups/{id}", testGroup1.getGroupId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupId", "1")
                .param("groupName", "group1")
                .sessionAttr("group", testGroup1))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/groups/groups-list"))
                .andExpect(redirectedUrl("/groups/groups-list"))
                .andExpect(flash().attributeExists("success"));
        verify(groupService, times(1)).removeGroup(Mockito.any(Group.class));
        verifyNoMoreInteractions(groupService);
    }
    
    @Test
    void testDeleteGroupShouldReturnGroupPageViewWithErrorMessageWhenGroupHasLessons() throws Exception {
        testGroup1.getStudents().clear();
        testGroup1.setLessons(Collections.singletonList(testLesson));
        BindingResult bindingResult = (BindingResult) mockMvc.perform(delete("/groups/{id}", testGroup1.getGroupId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupId", "1")
                .param("groupName", "group")
                .sessionAttr("group", testGroup1))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("groups/group-page"))
                .andExpect(model().attributeHasErrors("group"))
                .andExpect(model().attributeHasFieldErrorCode("group", "lessons", "Size"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "group");
        assertEquals("Deletion is not possible. Group has lessons. First remove lessons", 
                bindingResult.getFieldError("lessons").getDefaultMessage());
    }
    
    @Test
    void testDeleteGroupShouldReturnGroupPageViewWithErrorMessageWhenGroupHasStudents() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(delete("/groups/{id}", testGroup1.getGroupId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupId", "1")
                .param("groupName", "group")
                .sessionAttr("group", testGroup1))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("groups/group-page"))
                .andExpect(model().attributeHasErrors("group"))
                .andExpect(model().attributeHasFieldErrorCode("group", "students", "Size"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "group");
        assertEquals("Deletion is not possible. Group has students. First remove students", 
                bindingResult.getFieldError("students").getDefaultMessage());
    }   
}
