package com.foxminded.university.controllers;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.foxminded.university.domain.service.AuditoriumService;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.LessonService;
import com.foxminded.university.domain.service.StudentService;
import com.foxminded.university.domain.service.TeacherService;

@ExtendWith(MockitoExtension.class)
class UniversityControllerTest {

    private MockMvc mockMvc;

    @Mock
    private TeacherService teacherService;
    @Mock
    private AuditoriumService auditoriumService;
    @Mock
    private StudentService studentService;
    @Mock
    private GroupService groupService;
    @Mock
    private CourseService courseService;
    @Mock
    private LessonService lessonService;

    @InjectMocks
    private UniversityController universityController;

    @BeforeEach
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(universityController)
                                 .setControllerAdvice(new DefaultController())
                                 .build();
    }

    @Test
    void testStartShouldAddCountersToModelAndRenderUniversityPageView() throws Exception {
        when(auditoriumService.countAuditoriums()).thenReturn(10L);
        when(teacherService.countTeachers()).thenReturn(10L);
        when(studentService.countStudents()).thenReturn(10L);
        when(groupService.countGroups()).thenReturn(10L);
        when(courseService.countCourses()).thenReturn(10L);
        mockMvc.perform(get("/"))
                .andExpect(view().name("university"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("numberOfAuditoriums", equalTo(10L)))
                .andExpect(model().attribute("numberOfTeachers", equalTo(10L)))
                .andExpect(model().attribute("numberOfStudents", equalTo(10L)))
                .andExpect(model().attribute("numberOfGroups", equalTo(10L)))
                .andExpect(model().attribute("numberOfCourses", equalTo(10L)));       
    }
}
