package com.foxminded.university.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;

import com.foxminded.university.dao.AuditoriumRepository;
import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.dao.DateTimeRepository;
import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.CourseService;

@WebMvcTest(controllers = CoursesController.class)
class CoursesControllerTest {
    
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CourseService courseService;
    
    @MockBean
    private AuditoriumRepository auditoriumRepository;
    @MockBean
    private CourseRepository courseRepository;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private DateTimeRepository dateTimeRepository;
    
    private Course testCourse;
    private Lesson testLesson;
    
    @BeforeEach
    public void setUp() throws Exception {
        testCourse = new Course(1, "course", "description");
        testLesson = new Lesson();
        testLesson.setAuditorium(new Auditorium(1, 10, 100));
        testLesson.setCourse(testCourse);
        testLesson.setDateTime(new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)));
        testLesson.setGroup(new Group(1, "group"));
        testLesson.setTeacher(new Teacher(1, "teacher"));
    }

    @Test
    void testGetAllCoursesShouldAddCoursesEntriesToModelAndRenderCoursesListView() throws Exception {
        Course testCourse1 = new Course(1, "course1", "description1");
        Course testCourse2 = new Course(2, "course2", "description2");
        when(courseService.getAllCourses()).thenReturn(Arrays.asList(testCourse1, testCourse2));
        mockMvc.perform(get("/courses/courses-list"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("courses/courses-list"))
                .andExpect(model().attribute("courses", hasSize(2)))
                .andExpect(model().attribute("courses", hasItem(
                        allOf(
                                hasProperty("courseId", equalTo(1L)), 
                                hasProperty("courseName", equalTo("course1")),
                                hasProperty("courseDescription", equalTo("description1"))))))
                .andExpect(model().attribute("courses", hasItem(
                        allOf(
                                hasProperty("courseId", equalTo(2L)), 
                                hasProperty("courseName", equalTo("course2")),
                                hasProperty("courseDescription", equalTo("description2"))))));
        verify(courseService, times(1)).getAllCourses();
        verifyNoMoreInteractions(courseService);
    }
    
    @Test
    void testNewCourseShouldAddEmtyCourseEntryToModelAndRenderNewCourseView() throws Exception {
        Course emptyCourse = new Course();
        mockMvc.perform(get("/courses/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("courses/new"))
                .andExpect(model().attribute("course", emptyCourse));     
    }
    
    @Test
    void testCreateCourseShouldReturnNewCoursePageWithErrorMessageWhenExistingCoursePosted() throws Exception {
        when(courseRepository.getByCourseName("course")).thenReturn(testCourse);
        BindingResult bindingResult = (BindingResult) mockMvc.perform(post("/courses/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("courseName", "course")
                .param("courseDescription", "description"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("courses/new"))
                .andExpect(model().attributeHasErrors("course"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "course");
        assertEquals("Course with the same name already exists", bindingResult.getGlobalError().getDefaultMessage());
    }
    
    @Test
    void testCreateCourseShouldReturnNewCoursePageWithErrorMessageWhenCourseWithNotValidCourseNamePosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(post("/courses/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("courseName", "    ")
                .param("courseDescription", "description"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("courses/new"))
                .andExpect(model().attributeHasErrors("course"))
                .andExpect(model().attributeHasFieldErrorCode("course", "courseName", "NotBlank"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "course");
        assertEquals("Course name must not be empty", bindingResult.getFieldError("courseName").getDefaultMessage());
    }
    
    @Test
    void testCreateCourseShouldReturnNewCoursePageWithErrorMessageWhenCourseWithNotValidDescriptionPosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(post("/courses/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("courseName", "course")
                .param("courseDescription", "   "))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("courses/new"))
                .andExpect(model().attributeHasErrors("course"))
                .andExpect(model().attributeHasFieldErrorCode("course", "courseDescription", "NotBlank"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "course");
        assertEquals("Course description must not be empty", bindingResult.getFieldError("courseDescription").getDefaultMessage());
    }
    
    @Test
    void testCreateCourseShouldAddCourseEntryAndRedirectToCoursesListViewWithSuccessMessage() throws Exception {
        when(courseService.addCourse(Mockito.any(Course.class))).thenReturn(testCourse);
        mockMvc.perform(post("/courses/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("courseName", "course")
                .param("courseDescription", "description"))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/courses/courses-list"))
                .andExpect(redirectedUrl("/courses/courses-list"))
                .andExpect(flash().attributeExists("success"));
        ArgumentCaptor<Course> formObjectArgument = ArgumentCaptor.forClass(Course.class);
        verify(courseService, times(1)).addCourse(formObjectArgument.capture());
        verifyNoMoreInteractions(courseService);
        Course formObject = formObjectArgument.getValue();
        assertEquals(formObject.getCourseId(), 0);
        assertEquals(formObject.getCourseName(), "course");
        assertEquals(formObject.getCourseDescription(), "description");
    }
    
    @Test
    void testShowCourseShouldAddCourseAndLessonsForCourseEntriesToModelAndRenderCoursePageView() throws Exception {
        when(courseService.getCourseById(anyLong())).thenReturn(testCourse);
        mockMvc.perform(get("/courses/{id}", testCourse.getCourseId()))
                .andExpect(status().isOk())
                .andExpect(view().name("courses/course-page"))
                .andExpect(model().attribute("course", testCourse));
        verify(courseService, times(1)).getCourseById(testCourse.getCourseId());
    }
    
    @Test
    void testUpdateCourseShouldReturnNewCoursePageWithErrorMessageWhenExistingCoursePosted() throws Exception {
        Course existingCourse = new Course(2, "course", "coursedesc");
        testCourse.setLessons(Collections.singletonList(testLesson));
        when(courseRepository.getByCourseName("course")).thenReturn(existingCourse);
        BindingResult bindingResult = (BindingResult) mockMvc.perform(patch("/courses/{id}", testCourse.getCourseId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("courseName", "course")
                .param("courseDescription", "description")
                .sessionAttr("course", testCourse))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("courses/course-page"))
                .andExpect(model().attributeHasErrors("course"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "course");
        assertEquals("Course with the same name already exists", bindingResult.getGlobalError().getDefaultMessage());
    }
    
    @Test
    void testUpdateCourseShouldReturnNewCoursePageWithErrorMessageWhenCourseWithNotValidCourseNamePosted() throws Exception {
        testCourse.setLessons(Collections.singletonList(testLesson));
        BindingResult bindingResult = (BindingResult) mockMvc.perform(patch("/courses/{id}", testCourse.getCourseId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("courseName", "    ")
                .param("courseDescription", "description")
                .sessionAttr("course", testCourse))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("courses/course-page"))
                .andExpect(model().attributeHasErrors("course"))
                .andExpect(model().attributeHasFieldErrorCode("course", "courseName", "NotBlank"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "course");
        assertEquals("Course name must not be empty", bindingResult.getFieldError("courseName").getDefaultMessage());
    }
    
    @Test
    void testUpdateCourseShouldReturnNewCoursePageWithErrorMessageWhenCourseWithNotValidDescriptionPosted() throws Exception {
        testCourse.setLessons(Collections.singletonList(testLesson));
        BindingResult bindingResult = (BindingResult) mockMvc.perform(patch("/courses/{id}", testCourse.getCourseId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("courseName", "course")
                .param("courseDescription", "   ")
                .sessionAttr("course", testCourse))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("courses/course-page"))
                .andExpect(model().attributeHasErrors("course"))
                .andExpect(model().attributeHasFieldErrorCode("course", "courseDescription", "NotBlank"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "course");
        assertEquals("Course description must not be empty", bindingResult.getFieldError("courseDescription").getDefaultMessage());
    }
    
    @Test
    void testUpdateCoursesShouldUpdateCourseEntryAndRedirectToCoursesListViewWithSuccessMessage() throws Exception {
        when(courseRepository.getByCourseName("course")).thenReturn(testCourse);
        mockMvc.perform(patch("/courses/{id}", testCourse.getCourseId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("courseName", "course")
                .param("courseDescription", "description")
                .sessionAttr("course", testCourse))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/courses/courses-list"))
                .andExpect(redirectedUrl("/courses/courses-list"))
                .andExpect(flash().attributeExists("success"));
        verify(courseService, times(1)).updateCourse(Mockito.any(Course.class));
        verifyNoMoreInteractions(courseService);
    }
    
    @Test
    void testDeleteCourseShouldDeleteCourseEntryAndRenderCoursesListView() throws Exception {
        mockMvc.perform(delete("/courses/{id}", testCourse.getCourseId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("courseId", "1")
                .param("courseName", "course")
                .param("courseDescription", "description")
                .sessionAttr("course", new Course()))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/courses/courses-list"))
                .andExpect(redirectedUrl("/courses/courses-list"))
                .andExpect(flash().attributeExists("success"));
        verify(courseService, times(1)).removeCourse(Mockito.any(Course.class));
        verifyNoMoreInteractions(courseService);
    }
    
    @Test
    void testDeleteCourseShouldReturnCoursePageViewWithErrorMessageWhenCourseHasLessons() throws Exception {
        testCourse.setLessons(Collections.singletonList(testLesson));
        BindingResult bindingResult = (BindingResult) mockMvc.perform(delete("/courses/{id}", testCourse.getCourseId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("courseId", "1")
                .param("courseName", "course")
                .param("courseDescription", "description")
                .sessionAttr("course", testCourse))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("courses/course-page"))
                .andExpect(model().attributeHasErrors("course"))
                .andExpect(model().attributeHasFieldErrorCode("course", "lessons", "Size"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "course");
        assertEquals("Deletion is not possible. Course has lessons. First remove lessons", 
                bindingResult.getFieldError("lessons").getDefaultMessage());
    }
}
