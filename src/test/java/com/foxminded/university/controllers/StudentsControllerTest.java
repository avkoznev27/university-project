package com.foxminded.university.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.*;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Arrays;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.validation.BindingResult;

import com.foxminded.university.domain.formatters.CourseFormatter;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.StudentService;
import com.foxminded.university.exceptions.ServiceException;

@WebMvcTest(controllers = StudentsController.class)
class StudentsControllerTest {
    
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private FormattingConversionService conversionService;
    @MockBean
    private StudentService studentService;
    @MockBean
    private GroupService groupService;
    @MockBean
    private CourseService courseService;

    private Group testGroup;
    private Course testCourse;
    private Student testStudent1;
    private Student testStudent2;
    private Lesson testLesson;
    
    @BeforeEach
    public void setUp() throws Exception {
        conversionService.addFormatterForFieldType(Course.class, new CourseFormatter());
        testGroup = new Group(1, "testGroup");
        testCourse = new Course(1, "course1", null);
        testStudent1 = new Student(1, 1, "student1");
        testStudent2 = new Student(2, 1, "student2");
        testStudent1.getCourses().add(testCourse);
        testStudent2.getCourses().add(testCourse);
        testStudent1.setGroup(testGroup);
        testStudent2.setGroup(testGroup);
        testLesson = new Lesson();
        testLesson.setAuditorium(new Auditorium(1, 10, 100));
        testLesson.setCourse(new Course(1, "course", "description"));
        testLesson.setDateTime(new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)));
        testLesson.setGroup(testGroup);
        testLesson.setTeacher(new Teacher(1, "teacher"));
    }

    @Test
    void testGetAllStudentsShouldAddStudentsEntriesToModelAndRenderStudentsListView() throws Exception {
        when(studentService.getAllStudents()).thenReturn(Arrays.asList(testStudent1, testStudent2));
        mockMvc.perform(get("/students/students-list"))
                .andExpect(status().isOk())
                .andExpect(view().name("students/students-list"))
                .andExpect(model().attribute("students", hasSize(2)))
                .andExpect(model().attribute("students", hasItem(
                        allOf(
                                hasProperty("studentId", equalTo(1L)), 
                                hasProperty("groupId", equalTo(1L)),
                                hasProperty("name", equalTo("student1")),
                                hasProperty("courses", equalTo(singleton(testCourse)))))))
                .andExpect(model().attribute("students", hasItem(
                        allOf(
                                hasProperty("studentId", equalTo(2L)), 
                                hasProperty("groupId", equalTo(1L)),
                                hasProperty("name", equalTo("student2")),
                                hasProperty("courses", equalTo(singleton(testCourse)))))));
    verify(studentService, times(1)).getAllStudents();
    verifyNoMoreInteractions(studentService);
    }
    
    @Test
    void testNewStudentShouldAddEmptyStudentEntryIncompleteGroupsAndAllCoursesToModelAndRenderNewStudentView() throws Exception {
        Student emptyStudent = new Student();
        when(groupService.getAllIncompleteGroups()).thenReturn(singletonList(testGroup));
        when(courseService.getAllCourses()).thenReturn(singletonList(testCourse));
        mockMvc.perform(get("/students/new"))
               .andExpect(status().isOk())
               .andExpect(view().name("students/new"))
               .andExpect(model().attribute("student", emptyStudent))
               .andExpect(model().attribute("incompleteGroups", singletonList(testGroup)))
               .andExpect(model().attribute("allCourses", singletonList(testCourse)));
    }
    
    @Test
    void testCreateStudentShouldAddStudentEntryAndRedirectToStudentsListViewWithSuccessMessage() throws Exception {
        Student createdStudent = new Student(1, "student1");
        Course createdCourse = new Course(1, "course1", null);
        createdStudent.getCourses().add(createdCourse);
        when(groupService.getGroupById(createdStudent.getGroupId())).thenReturn(testGroup);
        when(studentService.addStudent(testGroup, createdStudent)).thenReturn(testStudent1);
        mockMvc.perform(post("/students/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupId", "1")
                .param("name", "student1")
                .param("courses", "ID=1NAME=course1"))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/students/students-list"))
                .andExpect(redirectedUrl("/students/students-list"))
                .andExpect(flash().attributeExists("success"));
        ArgumentCaptor<Student> formObjectArgument = ArgumentCaptor.forClass(Student.class);
        verify(groupService, times(1)).getGroupById(1);
        verify(studentService, times(1)).addStudent(Mockito.any(Group.class), formObjectArgument.capture());
        verifyNoMoreInteractions(groupService);
        Student formObject = formObjectArgument.getValue();
        assertEquals(0, formObject.getStudentId());
        assertEquals(1, formObject.getGroupId());
        assertEquals("student1", formObject.getName());
    }
    
    @Test
    void testCreateStudentShouldReturnNewStudentViewWithErrorMessageWhenNotValidNamePosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(post("/students/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupId", "1")
                .param("name", "   ")
                .param("courses", "ID=1NAME=course1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeHasErrors("student"))
                .andExpect(model().attributeHasFieldErrorCode("student", "name", "NotBlank"))
                .andExpect(view().name("students/new"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "student");
        assertEquals("Student name must not be empty", bindingResult.getFieldError("name").getDefaultMessage());
    }
    
    @Test
    void testCreateStudentShouldReturnNewStudentViewWithErrorMessageWhenNotValidGroupIdPosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(post("/students/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupId", "-1")
                .param("name", "name")
                .param("courses", "ID=1NAME=course1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeHasErrors("student"))
                .andExpect(model().attributeHasFieldErrorCode("student", "groupId", "Positive"))
                .andExpect(view().name("students/new"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "student");
        assertEquals("Wrong group or no group selected", bindingResult.getFieldError("groupId").getDefaultMessage());
    }
    
    @Test
    void testCreateStudentShouldRenderNewStudentViewWithServiceExceptionAttributeWhenSerwiceExceptionWasThrown() throws Exception {
        when(groupService.getGroupById(anyLong())).thenReturn(new Group(1));
        when(studentService.addStudent(new Group(1), new Student(1, "student1"))).thenThrow(new ServiceException("Message"));
        mockMvc.perform(post("/students/new")
                .header("referer", "/students/new")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupId", "1")
                .param("name", "student1")
                .sessionAttr("student", new Student()))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/students/new"))
                .andExpect(redirectedUrl("/students/new"))
                .andExpect(flash().attributeExists("serviceException"));
    }
    
    @Test
    void testShowStudentShouldAddStudentAndLessonsForStudentEntriesToModelAndRenderStudentPageView() throws Exception {
        testLesson.setGroup(new Group(testStudent1.getGroupId()));
        testGroup.setLessons(singletonList(testLesson));
        when(studentService.getStudentById(anyLong())).thenReturn(testStudent1);
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        when(groupService.getAllIncompleteGroups()).thenReturn(singletonList(testGroup));
        when(courseService.getAllCourses()).thenReturn(singletonList(testCourse));
        mockMvc.perform(get("/students/{id}", testStudent1.getStudentId())
                .sessionAttr("student", testStudent1)
                .sessionAttr("studentLessons", testGroup.getLessons())
                .sessionAttr("incompleteGroups", singletonList(testGroup))
                .sessionAttr("allCourses", singletonList(testCourse)))
                .andExpect(status().isOk())
                .andExpect(view().name("students/student-page"))
                .andExpect(model().attribute("student", testStudent1))
                .andExpect(model().attribute("studentLessons", hasSize(1)))
                .andExpect(model().attribute("studentLessons", hasItem(testLesson)))
                .andExpect(model().attribute("incompleteGroups", hasSize(1)))
                .andExpect(model().attribute("incompleteGroups", hasItem(testGroup)))
                .andExpect(model().attribute("allCourses", hasSize(1)))
                .andExpect(model().attribute("allCourses", hasItem(testCourse)));
        verify(studentService, times(1)).getStudentById(testStudent1.getStudentId());
        verify(groupService, times(1)).getAllIncompleteGroups();
        verify(courseService, times(1)).getAllCourses();
    }
    
    @Test
    void testUpdateStudentsShouldUpdateStudentEntryAndRedirectToStudentsListViewWithSuccessMessage() throws Exception {
        mockMvc.perform(patch("/students/{id}", testStudent1.getStudentId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("name", "student1")
                .param("groupId", "1")
                .param("courses", "ID=1NAME=course1")
                .sessionAttr("student", testStudent1))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/students/students-list"))
                .andExpect(redirectedUrl("/students/students-list"))
                .andExpect(flash().attributeExists("success"));
        verify(studentService, times(1)).updateStudent(Mockito.any(Student.class));
        verifyNoMoreInteractions(studentService);
    }
    
    @Test
    void testUpdateStudentShouldReturnStudentPageViewWithErrorMessageWhenNotValidNamePosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(patch("/students/{id}", testStudent1.getStudentId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupId", "1")
                .param("name", "   ")
                .param("courses", "ID=1NAME=course1")
                .sessionAttr("student", testStudent1)
                .sessionAttr("studentLessons", testGroup.getLessons())
                .sessionAttr("incompleteGroups", singletonList(testGroup))
                .sessionAttr("allCourses", singletonList(testCourse)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeHasErrors("student"))
                .andExpect(model().attributeHasFieldErrorCode("student", "name", "NotBlank"))
                .andExpect(view().name("students/student-page"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "student");
        assertEquals("Student name must not be empty", bindingResult.getFieldError("name").getDefaultMessage());
    }
    
    @Test
    void testUpdateStudentShouldReturnStudentPageViewWithErrorMessageWhenNotValidGroupIdPosted() throws Exception {
        BindingResult bindingResult = (BindingResult) mockMvc.perform(patch("/students/{id}", testStudent1.getStudentId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("groupId", "-1")
                .param("name", "name")
                .param("courses", "ID=1NAME=course1")
                .sessionAttr("student", testStudent1)
                .sessionAttr("studentLessons", testGroup.getLessons())
                .sessionAttr("incompleteGroups", singletonList(testGroup))
                .sessionAttr("allCourses", singletonList(testCourse)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeHasErrors("student"))
                .andExpect(model().attributeHasFieldErrorCode("student", "groupId", "Positive"))
                .andExpect(view().name("students/student-page"))
                .andReturn().getModelAndView().getModel().get(BindingResult.MODEL_KEY_PREFIX + "student");
        assertEquals("Wrong group or no group selected", bindingResult.getFieldError("groupId").getDefaultMessage());
    }
    
    @Test
    void testDeleteStudentShouldDeleteStudentEntryAndRenderStudentsListView() throws Exception {
        mockMvc.perform(delete("/students/{id}", testStudent1.getStudentId())
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("studentId", "1")
                .param("name", "student1")
                .param("groupId", "1")
                .param("courses", "ID=1NAME=course1")
                .sessionAttr("student", new Student()))
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/students/students-list"))
                .andExpect(redirectedUrl("/students/students-list"))
                .andExpect(flash().attributeExists("success"));
        verify(studentService, times(1)).removeStudent(Mockito.any(Student.class));
        verifyNoMoreInteractions(studentService);
    }
    
    @Test
    void testCreateStudentTimetableShouldRenderStudentTimetableViewForDayWhenRequestParameterIsDay() throws Exception {
        when(studentService.getStudentById(anyLong())).thenReturn(testStudent1);
        when(studentService.getStudentTimetable(Mockito.any(Student.class), Mockito.any(LocalDate.class)))
        .thenReturn(singletonList(testLesson));
        mockMvc.perform(post("/students/student-timetable")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("studentId", "1")
                .param("startDay", "2020-09-01")
                .param("endDay", ""))
                .andExpect(status().isOk())
                .andExpect(view().name("students/student-timetable"))
                .andExpect(model().attributeExists("student"))
                .andExpect(model().attributeExists("startDay"))
                .andExpect(model().attributeExists("endDay"))
                .andExpect(model().attributeExists("studentTimetable"))
                .andExpect(model().attribute("dailyTimetable", true));
    }
    
    @Test
    void testCreateStudentTimetableShouldRenderStudentTimetableViewForPeriodWhenRequestParameterIsPeriodOfDays() throws Exception {
        when(studentService.getStudentById(anyLong())).thenReturn(testStudent1);
        when(studentService.getStudentTimetable(Mockito.any(Student.class), 
                Mockito.any(LocalDate.class), Mockito.any(LocalDate.class))).thenReturn(singletonList(testLesson));
        mockMvc.perform(post("/students/student-timetable")
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .param("studentId", "1")
                .param("startDay", "2020-09-01")
                .param("endDay", "2020-09-10"))
                .andExpect(status().isOk())
                .andExpect(view().name("students/student-timetable"))
                .andExpect(model().attributeExists("student"))
                .andExpect(model().attributeExists("startDay"))
                .andExpect(model().attributeExists("endDay"))
                .andExpect(model().attributeExists("studentTimetable"))
                .andExpect(model().attribute("dailyTimetable", false));
    }
}
