package com.foxminded.university.controllers.rest;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static java.util.Collections.singletonList;

import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.dao.AuditoriumRepository;
import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.dao.DateTimeRepository;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.DateTimeService;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.LessonService;
import com.foxminded.university.domain.service.StudentService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@WebMvcTest(controllers = GroupsRestController.class)
class GroupsRestControllerTest {
    
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    
    @MockBean
    private GroupService groupService;
    @MockBean
    private LessonService lessonService;
    @MockBean
    private StudentService studentService;
    @MockBean
    private DateTimeService dateTimeService;
    
    @MockBean
    private AuditoriumRepository auditoriumRepository;
    @MockBean
    private CourseRepository courseRepository;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private DateTimeRepository dateTimeRepository;

    private Group testGroup;
    private Student testStudent;
    private Lesson testLesson;
    private DateTime testDateTime;
    
    @BeforeEach
    public void setUp() throws Exception {
        testGroup = new Group(1, "group1");
        testStudent = new Student(1, 1, "student1");
        testDateTime = new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        testGroup.getStudents().add(testStudent);
        testLesson = new Lesson();
        testLesson.setAuditorium(new Auditorium(1, 10, 100));
        testLesson.setCourse(new Course(1, "course", "description"));
        testLesson.setDateTime(new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)));
        testLesson.setGroup(testGroup);
        testLesson.setTeacher(new Teacher(1, "teacher"));
    }

    @Test
    void testGetAllGroupsShouldReturnFoundGroupEntries() throws Exception {
        when(groupService.getAllGroups()).thenReturn(singletonList(testGroup));
        mockMvc.perform(get("/api/groups"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].groupId", is(1)))
                .andExpect(jsonPath("$[0].groupName", is(testGroup.getGroupName())));
        verify(groupService, times(1)).getAllGroups();
        verifyNoMoreInteractions(groupService);
    }
    
    @Test
    void testCountShouldReturnCountAllGroupEntries() throws Exception {
        when(groupService.countGroups()).thenReturn(10L);
        mockMvc.perform(get("/api/groups/count"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(10)));
        verify(groupService, times(1)).countGroups();
        verifyNoMoreInteractions(groupService);
    }
    
    @Test
    void testGetGroupShouldReturnFoundGroupEntryWhenExistingGroupIdPassed() throws Exception {
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        mockMvc.perform(get("/api/groups/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupId", is(1)))
                .andExpect(jsonPath("$.groupName", is(testGroup.getGroupName())));
        verify(groupService, times(1)).getGroupById(anyLong());
        verifyNoMoreInteractions(groupService);
    }
    
    @Test
    void testGetGroupShouldReturnHttpStatusCode404WhenNonExistingGroupIdPassed() throws Exception {
        when(groupService.getGroupById(anyLong())).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(get("/api/groups/{id}", 10L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
        verify(groupService, times(1)).getGroupById(anyLong());
        verifyNoMoreInteractions(groupService);
    }
    
    @Test
    void testGetGroupLessonsShouldReturnAllLessonsForGroupFoundByPassedId() throws Exception {
        testGroup.setLessons(singletonList(testLesson));
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        String result = mockMvc.perform(get("/api/groups/{id}/lessons", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(testLesson), objectMapper.readerForListOf(Lesson.class).readValue(result));
        verify(groupService, times(1)).getGroupById(anyLong());
        verifyNoMoreInteractions(groupService);
    }
    
    @Test
    void testGetGroupStudentsShouldReturnAllStudentsForGroupFoundByPassedId() throws Exception {
        Student student = new Student(1, 1, "Student");
        testGroup.setStudents(singletonList(student));
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        String result = mockMvc.perform(get("/api/groups/{id}/students", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(student), objectMapper.readerForListOf(Student.class).readValue(result));
        verify(groupService, times(1)).getGroupById(anyLong());
        verifyNoMoreInteractions(groupService);
    }
    
    @Test
    void testGetAllIncompleteGroupsShouldReturnIncompleteGroups() throws Exception {
        when(groupService.getAllIncompleteGroups()).thenReturn(singletonList(testGroup));
        mockMvc.perform(get("/api/groups/incomplete"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].groupId", is(1)))
                .andExpect(jsonPath("$[0].groupName", is(testGroup.getGroupName())));
        verify(groupService, times(1)).getAllIncompleteGroups();
    }
    
    @Test
    void testGetAllFreeGroupsShouldReturnFreeGroupsForPassedDateTime() throws Exception {
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        when(groupService.getAllFreeGroups(testDateTime)).thenReturn(singletonList(testGroup));
        mockMvc.perform(get("/api/groups/free/{dateTimeId}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].groupId", is(1)))
                .andExpect(jsonPath("$[0].groupName", is(testGroup.getGroupName())));
        verify(dateTimeService, times(1)).getById(anyLong());
        verify(groupService, times(1)).getAllFreeGroups(Mockito.any(DateTime.class));
    }
    
    @Test
    void testCreateGroupShouldCreateGroupEntryAndReturnCreatedEntry() throws Exception {
        when(groupService.addGroup(Mockito.any(Group.class))).thenReturn(testGroup);
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Group("group"))))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupId", is(1)))
                .andExpect(jsonPath("$.groupName", is(testGroup.getGroupName())));
        verify(groupService, times(1)).addGroup(Mockito.any(Group.class));
    }
    
    @Test
    void testCreateGroupShouldReturnStatusBadRequestAndValidationErrorForGroupIdWhenIdMoreThanZero() throws Exception {
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Group(2, "group"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupId", is("Group ID for creating must be zero")));
        verifyNoInteractions(groupService);
    }
    
    @Test
    void testCreateGroupShouldReturnHttpStatusBadRequestAndGlobalValidationErrorWhenGroupWithPassedRoomNumberAlreadyExists() throws Exception {
        when(groupRepository.getByGroupName(anyString())).thenReturn(testGroup);
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Group("group"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.group-CheckExisting", is("Group with the same name already exists")));
        verifyNoInteractions(groupService);
    }
    
    @Test
    void testCreateGroupShouldReturnStatusBadRequestAndValidationErrorForFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Group(""))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupName", is("Group name must not be empty")));
        
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Group())))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupName", is("Group name must not be empty")));
        verifyNoInteractions(groupService);
    }
    
    @Test
    void testUpdateGroupShouldUpdateGroupEntryAndReturnUpdatedEntry() throws Exception {
        when(groupService.updateGroup(Mockito.any(Group.class))).thenReturn(testGroup);
        mockMvc.perform(put("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Group(1, "group"))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupId", is(1)))
                .andExpect(jsonPath("$.groupName", is(testGroup.getGroupName())));
        verify(groupService, times(1)).updateGroup(Mockito.any(Group.class));
    }
    
    @Test
    void testUpdateGroupShouldReturnStatusBadRequestAndValidationErrorForGroupIdWhenPassedIdIsZero() throws Exception {
        mockMvc.perform(put("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Group("group"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupId", is("Group ID for updating must be greater than 0")));
        verifyNoInteractions(groupService);
    }
    
    @Test
    void testUpdateGroupShouldReturnHttpStatusCode404WhenNonExistingGroupIdPassed() throws Exception {
        when(groupService.updateGroup(Mockito.any(Group.class))).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(put("/api/groups/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Group(10, "group"))))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
    }
    
    @Test
    void testUpdateGroupShouldReturnHttpStatusBadRequestAndGlobalValidationErrorWhenGroupWithPassedRoomNumberAlreadyExists() throws Exception {
        when(groupRepository.getByGroupName(anyString())).thenReturn(testGroup);
        mockMvc.perform(put("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Group(2, "group"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.group-CheckExisting", is("Group with the same name already exists")));
        verifyNoInteractions(groupService);
    }
    
    @Test
    void testUpdateGroupShouldReturnStatusBadRequestAndValidationErrorForFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(put("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Group(1, ""))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupName", is("Group name must not be empty")));
    }
    
    @Test
    void testDeleteGroupShouldDeleteGroupEntryAndReturnDeletedGroup() throws Exception {
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        mockMvc.perform(delete("/api/groups/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupId", is(1)))
                .andExpect(jsonPath("$.groupName", is(testGroup.getGroupName())));
    }
    
    @Test
    void testDeleteGroupShouldReturnHttpStatusCode404WhenNonExistingGroupIdPassed() throws Exception {
        when(groupService.getGroupById(anyLong())).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(get("/api/groups/{id}", 10L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
        verify(groupService, times(1)).getGroupById(anyLong());
        verifyNoMoreInteractions(groupService);
    }
    
    @Test
    void testDeleteGroupShouldReturnHttpStatusCode400WhenGroupHasLessons() throws Exception {
        when(groupService.getGroupById(anyLong())).thenThrow(new ServiceException("Group has lessons"));
        mockMvc.perform(get("/api/groups/{id}", 10L))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ServiceException", is("Group has lessons")));
        verify(groupService, times(1)).getGroupById(anyLong());
        verifyNoMoreInteractions(groupService);
    }

}
