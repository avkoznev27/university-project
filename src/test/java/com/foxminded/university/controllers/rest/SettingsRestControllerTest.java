package com.foxminded.university.controllers.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.domain.service.SettingsService;
import com.foxminded.university.domain.model.Settings;

@WebMvcTest(controllers = SettingsRestController.class)
class SettingsRestControllerTest {
    
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    
    @MockBean
    private SettingsService settingsService;

    private Settings defaultSettings;
    private Settings actualSettings;

    @BeforeEach
    void setUp() throws Exception {
        defaultSettings = new Settings(1, 45, 30);
        actualSettings = new Settings(1, 90, 60);
    }

    @Test
    void testGetSettingsShouldReturnCurrentSettingsEntry() throws Exception {
        when(settingsService.getSettings()).thenReturn(actualSettings);
        mockMvc.perform(get("/api/settings/"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(1)))
        .andExpect(jsonPath("$.lessonDuration", is(actualSettings.getLessonDuration())))
        .andExpect(jsonPath("$.maxGroupSize", is(actualSettings.getMaxGroupSize())));
    }
    
    @Test
    void testSetDefaultShouldSetDefaultAndReturnSettingsEntry() throws Exception {
        when(settingsService.setDefaultSettings()).thenReturn(defaultSettings);
        mockMvc.perform(post("/api/settings/default"))
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.id", is(1)))
        .andExpect(jsonPath("$.lessonDuration", is(defaultSettings.getLessonDuration())))
        .andExpect(jsonPath("$.maxGroupSize", is(defaultSettings.getMaxGroupSize())));
    }
    
    @Test
    void testUpdateSettingsShouldReturnStatusBadRequestAndValidationErrorForFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(put("/api/settings/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Settings())))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.lessonDuration", is("must be greater than 0")))
                .andExpect(jsonPath("$.maxGroupSize", is("must be greater than 0")));
        
        mockMvc.perform(put("/api/settings/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Settings(1, -1000, -250))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.lessonDuration", is("must be greater than 0")))
                .andExpect(jsonPath("$.maxGroupSize", is("must be greater than 0")));
        verifyNoInteractions(settingsService);
    }
    
    @Test
    void testUpdateSettingsShouldReturnStatusIsOkAndReturnUpdatedSettingsEntryWhenValidDataPassed() throws Exception {
        when(settingsService.updateSettings(Mockito.any(Settings.class))).thenReturn(actualSettings);
        mockMvc.perform(put("/api/settings/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Settings(1, 90, 60))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.lessonDuration", is(90)))
                .andExpect(jsonPath("$.maxGroupSize", is(60)));
    }
}
