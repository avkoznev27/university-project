package com.foxminded.university.controllers.rest;

import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.LocalTime;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.service.LessonService;
import com.foxminded.university.domain.service.StudentService;
import com.foxminded.university.domain.service.TeacherService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@WebMvcTest(controllers = TeachersRestController.class)
class TeachersRestControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    
    @MockBean
    private TeacherService teacherService;
    @MockBean
    private CourseService courseService;
    @MockBean
    private LessonService lessonService;
    @MockBean
    private StudentService studentService;

    private Course testCourse;
    private Teacher testTeacher;
    private Lesson testLesson;

    @BeforeEach
    public void setUp() throws Exception {
        testCourse = new Course(1, "course1", null);
        testTeacher = new Teacher(1, "teacher1");
        testTeacher.getCourses().add(testCourse);
        testLesson = new Lesson();
        testLesson.setAuditorium(new Auditorium(1, 10, 100));
        testLesson.setCourse(new Course(1, "course", "description"));
        testLesson.setDateTime(new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)));
        testLesson.setGroup(new Group(1, "testGroup"));
        testLesson.setTeacher(new Teacher(1, "teacher"));
    }

    @Test
    void testGetAllTeachersShouldReturnFoundTeacherEntries() throws Exception {
        when(teacherService.getAllTeachers()).thenReturn(singletonList(testTeacher));
        mockMvc.perform(get("/api/teachers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].teacherId", is(1)))
                .andExpect(jsonPath("$[0].name", is(testTeacher.getName())));
        verify(teacherService, times(1)).getAllTeachers();
        verifyNoMoreInteractions(teacherService);
    }
    
    @Test
    void testCountShouldReturnCountAllTeacherEntries() throws Exception {
        when(teacherService.countTeachers()).thenReturn(10L);
        mockMvc.perform(get("/api/teachers/count"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(10)));
        verify(teacherService, times(1)).countTeachers();
        verifyNoMoreInteractions(teacherService);
    }
    
    @Test
    void testGetTeacherShouldReturnFoundTeacherEntryWhenExistingTeacherIdPassed() throws Exception {
        when(teacherService.getTeacherById(anyLong())).thenReturn(testTeacher);
        mockMvc.perform(get("/api/teachers/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.teacherId", is(1)))
                .andExpect(jsonPath("$.name", is(testTeacher.getName())));
        verify(teacherService, times(1)).getTeacherById(anyLong());
        verifyNoMoreInteractions(teacherService);
    }
    
    @Test
    void testGetTeacherShouldReturnHttpStatusCode404WhenNonExistingTeacherIdPassed() throws Exception {
        when(teacherService.getTeacherById(anyLong())).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(get("/api/teachers/{id}", 10L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
        verify(teacherService, times(1)).getTeacherById(anyLong());
        verifyNoMoreInteractions(teacherService);
    }
    
    @Test
    void testGetTeacherCoursesShouldReturnAllCoursesForTeacherFoundByPassedId() throws Exception {
        testTeacher.setCourses(singleton(testCourse));
        when(teacherService.getTeacherById(anyLong())).thenReturn(testTeacher);
        mockMvc.perform(get("/api/teachers/{id}/courses", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].courseId", is(1)))
                .andExpect(jsonPath("$[0].courseName", is(testCourse.getCourseName())))
                .andExpect(jsonPath("$[0].courseDescription", is(testCourse.getCourseDescription())));
        verify(teacherService, times(1)).getTeacherById(anyLong());
        verifyNoMoreInteractions(teacherService);
    }
    
    @Test
    void testGetTeacherLessonsShouldReturnAllLessonsForTeacherFoundByPassedId() throws Exception {
        testTeacher.setLessons(singletonList(testLesson));
        when(teacherService.getTeacherById(anyLong())).thenReturn(testTeacher);
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        String result = mockMvc.perform(get("/api/teachers/{id}/lessons", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(testLesson), objectMapper.readerForListOf(Lesson.class).readValue(result));
        verify(teacherService, times(1)).getTeacherById(anyLong());
        verifyNoMoreInteractions(teacherService);
    }
    
    @Test
    void testCreateTeacherTimetableShouldReturnTeacherLessonsForDayWhenRequestParameterIsDay() throws Exception {
        when(teacherService.getTeacherById(anyLong())).thenReturn(testTeacher);
        when(teacherService.getTeacherTimetable(Mockito.any(Teacher.class), Mockito.any(LocalDate.class))).thenReturn(singletonList(testLesson));
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        String result = mockMvc.perform(get("/api/teachers/{id}/timetable", 1)
                .param("startDay", "2020-09-01"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(testLesson), objectMapper.readerForListOf(Lesson.class).readValue(result));
        String result1 = mockMvc.perform(get("/api/teachers/{id}/timetable", 1)
                .param("startDay", "2020-09-01")
                .param("endDay", "2020-09-01"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(testLesson), objectMapper.readerForListOf(Lesson.class).readValue(result1));
    }
    
    @Test
    void testCreateTeacherTimetableShouldReturnTeacherLessonsForPeriodWhenRequestParameterIsPeriodOfDays() throws Exception {
        when(teacherService.getTeacherById(anyLong())).thenReturn(testTeacher);
        when(teacherService.getTeacherTimetable(Mockito.any(Teacher.class), Mockito.any(LocalDate.class), Mockito.any(LocalDate.class)))
        .thenReturn(singletonList(testLesson));
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        String result = mockMvc.perform(get("/api/teachers/{id}/timetable", 1)
                .param("startDay", "2020-09-01")
                .param("endDay", "2020-09-02"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(testLesson), objectMapper.readerForListOf(Lesson.class).readValue(result));
    }
    
    @Test
    void testCreateTeacherTimetableShouldReturnHttpStatusBadRequestWhenRequestParameterIsWrongFormat() throws Exception {
        when(teacherService.getTeacherById(anyLong())).thenReturn(testTeacher);
        mockMvc.perform(get("/api/teachers/{id}/timetable", 1)
                .param("startDay", "20-09-01"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.DateTimeParseException", Matchers.isA(String.class)));
    }
    
    @Test
    void testCreateTeacherShouldCreateTeacherEntryAndReturnCreatedEntry() throws Exception {
        when(teacherService.addTeacher(Mockito.any(Teacher.class))).thenReturn(testTeacher);
        mockMvc.perform(post("/api/teachers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Teacher("teacher"))))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.teacherId", is(1)))
                .andExpect(jsonPath("$.name", is(testTeacher.getName())));
        verify(teacherService, times(1)).addTeacher(Mockito.any(Teacher.class));
    }
    
    @Test
    void testCreateTeacherShouldReturnStatusBadRequestAndValidationErrorForTeacherIdWhenIdMoreThanZero() throws Exception {
        mockMvc.perform(post("/api/teachers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Teacher(1, "teacher"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.teacherId", is("Teacher ID for creating must be zero")));
        verifyNoInteractions(teacherService);
    }
    
    @Test
    void testCreateTeacherShouldReturnStatusBadRequestAndValidationErrorForFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(post("/api/teachers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Teacher(""))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is("Teacher name must not be empty")));
        
        mockMvc.perform(post("/api/teachers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Teacher())))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is("Teacher name must not be empty")));
        verifyNoInteractions(teacherService);
    }
    
    @Test
    void testUpdateTeacherShouldUpdateTeacherEntryAndReturnUpdatedEntry() throws Exception {
        when(teacherService.updateTeacher(Mockito.any(Teacher.class))).thenReturn(testTeacher);
        mockMvc.perform(put("/api/teachers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Teacher(1, "teacher"))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.teacherId", is(1)))
                .andExpect(jsonPath("$.name", is(testTeacher.getName())));
        verify(teacherService, times(1)).updateTeacher(Mockito.any(Teacher.class));
    }
    
    @Test
    void testUpdateTeacherShouldReturnStatusBadRequestAndValidationErrorForTeacherIdWhenPassedIdIsZero() throws Exception {
        mockMvc.perform(put("/api/teachers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Teacher("teacher"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.teacherId", is("Teacher ID for updating must be greater than 0")));
        verifyNoInteractions(teacherService);
    }
    
    @Test
    void testUpdateTeacherShouldReturnHttpStatusCode404WhenNonExistingTeacherIdPassed() throws Exception {
        when(teacherService.updateTeacher(Mockito.any(Teacher.class))).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(put("/api/teachers/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Teacher(10, "teacher"))))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
    }
    
    @Test
    void testUpdateTeacherShouldReturnStatusBadRequestAndValidationErrorForFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(put("/api/teachers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Teacher(1, ""))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is("Teacher name must not be empty")));
    }
    
    @Test
    void testDeleteTeacherShouldDeleteTeacherEntryAndReturnDeletedTeacher() throws Exception {
        when(teacherService.getTeacherById(anyLong())).thenReturn(testTeacher);
        mockMvc.perform(delete("/api/teachers/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.teacherId", is(1)))
                .andExpect(jsonPath("$.name", is(testTeacher.getName())));
    }
    
    @Test
    void testDeleteTeacherShouldReturnHttpStatusCode404WhenNonExistingTeacherIdPassed() throws Exception {
        when(teacherService.getTeacherById(anyLong())).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(get("/api/teachers/{id}", 10L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
        verify(teacherService, times(1)).getTeacherById(anyLong());
        verifyNoMoreInteractions(teacherService);
    }
    
    @Test
    void testDeleteTeacherShouldReturnHttpStatusCode400WhenTeacherHasLessons() throws Exception {
        when(teacherService.getTeacherById(anyLong())).thenThrow(new ServiceException("Teacher has lessons"));
        mockMvc.perform(get("/api/teachers/{id}", 10L))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ServiceException", is("Teacher has lessons")));
        verify(teacherService, times(1)).getTeacherById(anyLong());
        verifyNoMoreInteractions(teacherService);
    }
}
