package com.foxminded.university.controllers.rest;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static java.util.Collections.singletonList;

import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.dao.AuditoriumRepository;
import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.dao.DateTimeRepository;
import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.AuditoriumService;
import com.foxminded.university.domain.service.DateTimeService;
import com.foxminded.university.domain.service.LessonService;
import com.foxminded.university.domain.service.StudentService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@WebMvcTest(controllers = AuditoriumsRestController.class)
class AuditoriumsRestControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private AuditoriumService auditoriumService;
    @MockBean
    private DateTimeService dateTimeService;
    @MockBean
    private LessonService lessonService;
    @MockBean
    private StudentService studentService;
    
    @MockBean
    private AuditoriumRepository auditoriumRepository;
    @MockBean
    private CourseRepository courseRepository;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private DateTimeRepository dateTimeRepository;
    
    private Auditorium testAuditorium;
    private Lesson testLesson;
    private DateTime testDateTime;
    
    @BeforeEach
    public void setUp() throws Exception {
        testAuditorium = new Auditorium(1, 10, 100);
        testDateTime = new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        testLesson = new Lesson();
        testLesson.setLessonId(1L);
        testLesson.setAuditorium(testAuditorium);
        testLesson.setCourse(new Course(1, "course1", "description"));
        testLesson.setDateTime(testDateTime);
        testLesson.setGroup(new Group(1, "group"));
        testLesson.setTeacher(new Teacher(1, "teacher"));
    }

    @Test
    void testGetAllAuditoriumsShouldReturnFoundAuditoriumEntries() throws Exception {
        when(auditoriumService.getAllAuditoriums()).thenReturn(singletonList(testAuditorium));
        mockMvc.perform(get("/api/auditoriums"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].auditoriumId", is(1)))
                .andExpect(jsonPath("$[0].roomNumber", is(testAuditorium.getRoomNumber())))
                .andExpect(jsonPath("$[0].capacity", is(testAuditorium.getCapacity())));
        verify(auditoriumService, times(1)).getAllAuditoriums();
        verifyNoMoreInteractions(auditoriumService);
    }
    
    @Test
    void testCountShouldReturnCountAllAuditoriumEntries() throws Exception {
        when(auditoriumService.countAuditoriums()).thenReturn(10L);
        mockMvc.perform(get("/api/auditoriums/count"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(10)));
        verify(auditoriumService, times(1)).countAuditoriums();
        verifyNoMoreInteractions(auditoriumService);
    }
    
    @Test
    void testGetAuditoriumShouldReturnFoundAuditoriumEntryWhenExistingAuditoriumIdPassed() throws Exception {
        when(auditoriumService.getAuditoriumById(anyLong())).thenReturn(testAuditorium);
        mockMvc.perform(get("/api/auditoriums/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditoriumId", is(1)))
                .andExpect(jsonPath("$.roomNumber", is(testAuditorium.getRoomNumber())))
                .andExpect(jsonPath("$.capacity", is(testAuditorium.getCapacity())));
        verify(auditoriumService, times(1)).getAuditoriumById(anyLong());
        verifyNoMoreInteractions(auditoriumService);
    }
    
    @Test
    void testGetAuditoriumShouldReturnHttpStatusCode404WhenNonExistingAuditoriumIdPassed() throws Exception {
        when(auditoriumService.getAuditoriumById(anyLong())).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(get("/api/auditoriums/{id}", 10L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
        verify(auditoriumService, times(1)).getAuditoriumById(anyLong());
        verifyNoMoreInteractions(auditoriumService);
    }
    
    @Test
    void testGetAuditoriumLessonsShouldReturnAllLessonsForAuditoriumFoundByPassedId() throws Exception {
        testAuditorium.setLessons(singletonList(testLesson));
        when(auditoriumService.getAuditoriumById(anyLong())).thenReturn(testAuditorium);
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        String result = mockMvc.perform(get("/api/auditoriums/{id}/lessons", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(testLesson), objectMapper.readerForListOf(Lesson.class).readValue(result));
        verify(auditoriumService, times(1)).getAuditoriumById(anyLong());
        verifyNoMoreInteractions(auditoriumService);
    }

    @Test
    void testGetAllFreeAuditoriumsShouldReturnFreeAuditoriumsForPassedDateTime() throws Exception {
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        when(auditoriumService.getAllFreeAuditoriums(testDateTime)).thenReturn(singletonList(testAuditorium));
        mockMvc.perform(get("/api/auditoriums/free/{dateTimeId}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].auditoriumId", is(1)))
                .andExpect(jsonPath("$[0].roomNumber", is(testAuditorium.getRoomNumber())))
                .andExpect(jsonPath("$[0].capacity", is(testAuditorium.getCapacity())));
        verify(dateTimeService, times(1)).getById(anyLong());
        verify(auditoriumService, times(1)).getAllFreeAuditoriums(Mockito.any(DateTime.class));
    }
    
    @Test
    void testGetAllFreeAuditoriumsWithCapacityShouldReturnFreeAuditoriumsForPassedDateTimeWithPassedCapacity() throws Exception {
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        when(auditoriumService.getAllFreeAuditoriums(testDateTime, 30)).thenReturn(singletonList(testAuditorium));
        mockMvc.perform(get("/api/auditoriums/free/{dateTimeId}/{capacity}", 1, 30))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].auditoriumId", is(1)))
                .andExpect(jsonPath("$[0].roomNumber", is(testAuditorium.getRoomNumber())))
                .andExpect(jsonPath("$[0].capacity", is(testAuditorium.getCapacity())));
        verify(dateTimeService, times(1)).getById(anyLong());
        verify(auditoriumService, times(1)).getAllFreeAuditoriums(Mockito.any(DateTime.class), anyInt());
    }
    
    @Test
    void testCreateAuditoriumShouldCreateAuditoriumEntryAndReturnCreatedEntry() throws Exception {
        when(auditoriumService.addAuditorium(Mockito.any(Auditorium.class))).thenReturn(testAuditorium);
        mockMvc.perform(post("/api/auditoriums")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Auditorium(10, 100))))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditoriumId", is(1)))
                .andExpect(jsonPath("$.roomNumber", is(testAuditorium.getRoomNumber())))
                .andExpect(jsonPath("$.capacity", is(testAuditorium.getCapacity())));
        verify(auditoriumService, times(1)).addAuditorium(Mockito.any(Auditorium.class));
    }
    
    @Test
    void testCreateAuditoriumShouldReturnStatusBadRequestAndValidationErrorForAuditoriumIdWhenIdMoreThanZero() throws Exception {
        mockMvc.perform(post("/api/auditoriums")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Auditorium(2, 10, 100))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditoriumId", is("Auditorium ID for creating must be zero")));
        verifyNoInteractions(auditoriumService);
    }
    
    @Test
    void testCreateAuditoriumShouldReturnHttpStatusBadRequestAndGlobalValidationErrorWhenAuditoriumWithPassedRoomNumberAlreadyExists() throws Exception {
        when(auditoriumRepository.getByRoomNumber(anyInt())).thenReturn(testAuditorium);
        mockMvc.perform(post("/api/auditoriums")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Auditorium(10, 25))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditorium-CheckExisting", is("Auditorium with this room number already exist")));
        verifyNoInteractions(auditoriumService);
    }
    
    @Test
    void testCreateAuditoriumShouldReturnStatusBadRequestAndValidationErrorForFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(post("/api/auditoriums")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Auditorium(1000, 250))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.roomNumber", is("Room number cannot be more than 999")))
                .andExpect(jsonPath("$.capacity", is("Capacity cannot be more than 200")));
        
        mockMvc.perform(post("/api/auditoriums")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Auditorium(-1000, -250))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.roomNumber", is("must be greater than 0")))
                .andExpect(jsonPath("$.capacity", is("must be greater than 0")));
        
        mockMvc.perform(post("/api/auditoriums")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Auditorium())))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.roomNumber", is("must be greater than 0")))
                .andExpect(jsonPath("$.capacity", is("must be greater than 0")));
        verifyNoInteractions(auditoriumService);
    }
    
    @Test
    void testUpdateAuditoriumShouldUpdateAuditoriumEntryAndReturnUpdatedEntry() throws Exception {
        when(auditoriumService.updateAuditorium(Mockito.any(Auditorium.class))).thenReturn(testAuditorium);
        mockMvc.perform(put("/api/auditoriums")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Auditorium(1, 10, 100))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditoriumId", is(1)))
                .andExpect(jsonPath("$.roomNumber", is(testAuditorium.getRoomNumber())))
                .andExpect(jsonPath("$.capacity", is(testAuditorium.getCapacity())));
        verify(auditoriumService, times(1)).updateAuditorium(Mockito.any(Auditorium.class));
    }
    
    @Test
    void testUpdateAuditoriumShouldReturnStatusBadRequestAndValidationErrorForAuditoriumIdWhenPassedIdIsZero() throws Exception {
        mockMvc.perform(put("/api/auditoriums")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Auditorium(10, 100))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditoriumId", is("Auditorium ID for updating must be greater than 0")));
        verifyNoInteractions(auditoriumService);
    }
    
    @Test
    void testUpdateAuditoriumShouldReturnHttpStatusCode404WhenNonExistingAuditoriumIdPassed() throws Exception {
        when(auditoriumService.updateAuditorium(Mockito.any(Auditorium.class))).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(put("/api/auditoriums/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Auditorium(10, 10, 100))))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
    }
    
    @Test
    void testUpdateAuditoriumShouldReturnHttpStatusBadRequestAndGlobalValidationErrorWhenAuditoriumWithPassedRoomNumberAlreadyExists() throws Exception {
        when(auditoriumRepository.getByRoomNumber(anyInt())).thenReturn(testAuditorium);
        mockMvc.perform(put("/api/auditoriums")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Auditorium(2, 10, 25))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditorium-CheckExisting", is("Auditorium with this room number already exist")));
        verifyNoInteractions(auditoriumService);
    }
    
    @Test
    void testUpdateAuditoriumShouldReturnStatusBadRequestAndValidationErrorForFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(put("/api/auditoriums")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Auditorium(1, 1000, 250))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.roomNumber", is("Room number cannot be more than 999")))
                .andExpect(jsonPath("$.capacity", is("Capacity cannot be more than 200")));
        
        mockMvc.perform(put("/api/auditoriums")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Auditorium(1, -1000, -250))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.roomNumber", is("must be greater than 0")))
                .andExpect(jsonPath("$.capacity", is("must be greater than 0")));
        verifyNoInteractions(auditoriumService);
    }
    
    @Test
    void testDeleteAuditoriumShouldDeleteAuditoriumEntryAndReturnDeletedAuditorium() throws Exception {
        when(auditoriumService.getAuditoriumById(anyLong())).thenReturn(testAuditorium);
        mockMvc.perform(delete("/api/auditoriums/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditoriumId", is(1)))
                .andExpect(jsonPath("$.roomNumber", is(testAuditorium.getRoomNumber())))
                .andExpect(jsonPath("$.capacity", is(testAuditorium.getCapacity())));
    }
    
    @Test
    void testDeleteAuditoriumShouldReturnHttpStatusCode404WhenNonExistingAuditoriumIdPassed() throws Exception {
        when(auditoriumService.getAuditoriumById(anyLong())).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(get("/api/auditoriums/{id}", 10L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
        verify(auditoriumService, times(1)).getAuditoriumById(anyLong());
        verifyNoMoreInteractions(auditoriumService);
    }
    
    @Test
    void testDeleteAuditoriumShouldReturnHttpStatusCode400WhenAuditoriumHasLessons() throws Exception {
        when(auditoriumService.getAuditoriumById(anyLong())).thenThrow(new ServiceException("Auditorium has lessons"));
        mockMvc.perform(get("/api/auditoriums/{id}", 10L))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ServiceException", is("Auditorium has lessons")));
        verify(auditoriumService, times(1)).getAuditoriumById(anyLong());
        verifyNoMoreInteractions(auditoriumService);
    }
}
