package com.foxminded.university.controllers.rest;

import static java.util.Collections.singletonList;
import static java.util.Collections.singleton;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.dao.AuditoriumRepository;
import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.dao.DateTimeRepository;
import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.AuditoriumService;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.service.DateTimeService;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.LessonService;
import com.foxminded.university.domain.service.StudentService;
import com.foxminded.university.domain.service.TeacherService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@WebMvcTest(controllers = TimetableRestController.class)
class TimetableRestControllerTest {
    
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TeacherService teacherService;
    @MockBean
    private AuditoriumService auditoriumService;
    @MockBean
    private StudentService studentService;
    @MockBean
    private GroupService groupService;
    @MockBean
    private CourseService courseService;
    @MockBean
    private DateTimeService dateTimeService;
    @MockBean
    private LessonService lessonService;
    @MockBean
    private AuditoriumRepository auditoriumRepository;
    @MockBean
    private CourseRepository courseRepository;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private DateTimeRepository dateTimeRepository;

    
    private Auditorium testAuditorium;
    private Course testCourse;
    private DateTime testDateTime;
    private Group testGroup;
    private Teacher testTeacher;
    private Lesson testLesson;
    private Lesson existingLesson;

    @BeforeEach
    public void setUp() throws Exception {
        testAuditorium = new Auditorium(1, 10, 100);
        testCourse = new Course(1, "testCourse", "description");
        testDateTime = new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        testGroup = new Group(1, "group");
        testTeacher = new Teacher(1, "teacher");
        testTeacher.setCourses(singleton(testCourse));
        testLesson = new Lesson();
        testLesson.setLessonId(1);
        testLesson.setAuditorium(testAuditorium);
        testLesson.setCourse(testCourse);
        testLesson.setDateTime(testDateTime);
        testLesson.setGroup(testGroup);
        testLesson.setTeacher(testTeacher);
        existingLesson = new Lesson();
        existingLesson.setLessonId(2);
        existingLesson.setDateTime(testDateTime);
    }

    @Test
    void testGetAllDateTimesShouldReturnFoundDateTimeEntries() throws Exception {
        when(dateTimeService.getAllDateTimes()).thenReturn(singletonList(testDateTime));
        mockMvc.perform(get("/api/timetable/datetimes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].dateTimeId", is(1)))
                .andExpect(jsonPath("$[0].lessonDate", is(testDateTime.getLessonDate().toString())))
                .andExpect(jsonPath("$[0].startLesson", is(testDateTime.getStartLesson().toString())))
                .andExpect(jsonPath("$[0].endLesson", is(testDateTime.getEndLesson().toString())));
        verify(dateTimeService, times(1)).getAllDateTimes();
        verifyNoMoreInteractions(dateTimeService);
    }
    
    @Test
    void testGetDateTimeShouldReturnFoundDateTimeEntryWhenExistingDateTimeIdPassed() throws Exception {
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        mockMvc.perform(get("/api/timetable/datetimes/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTimeId", is(1)))
                .andExpect(jsonPath("$.lessonDate", is(testDateTime.getLessonDate().toString())))
                .andExpect(jsonPath("$.startLesson", is(testDateTime.getStartLesson().toString())))
                .andExpect(jsonPath("$.endLesson", is(testDateTime.getEndLesson().toString())));
        verify(dateTimeService, times(1)).getById(anyLong());
        verifyNoMoreInteractions(dateTimeService);
    }
    
    @Test
    void testGetDateTimeShouldReturnHttpStatusCode404WhenNonExistingDateTimeIdPassed() throws Exception {
        when(dateTimeService.getById(anyLong())).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(get("/api/timetable/datetimes/{id}", 10L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
        verify(dateTimeService, times(1)).getById(anyLong());
        verifyNoMoreInteractions(dateTimeService);
    }
    
    @Test
    void testGetDateTimeLessonsShouldReturnAllLessonsForDateTimeFoundByPassedId() throws Exception {
        testDateTime.setLessons(singletonList(testLesson));
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        String result = mockMvc.perform(get("/api/timetable/datetimes/{id}/lessons", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(testLesson), objectMapper.readerForListOf(Lesson.class).readValue(result));
        verify(dateTimeService, times(1)).getById(anyLong());
        verifyNoMoreInteractions(dateTimeService);
    }
    
    @Test
    void testGetAllLessonsShouldReturnFoundLessonsEntries() throws Exception {
        when(lessonService.getAllLessons()).thenReturn(singletonList(testLesson));
        mockMvc.perform(get("/api/timetable/lessons"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$.[0]dateTime", Matchers.notNullValue()))
                .andExpect(jsonPath("$.[0]group", Matchers.notNullValue()))
                .andExpect(jsonPath("$.[0]auditorium", Matchers.notNullValue()))
                .andExpect(jsonPath("$.[0]course", Matchers.notNullValue()))
                .andExpect(jsonPath("$.[0]teacher", Matchers.notNullValue()));
    }
    
    @Test
    void testGetLessonShouldReturnFoundLessonEntryWhenExistingLessonIdPassed() throws Exception {
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        mockMvc.perform(get("/api/timetable/lessons/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.lessonId", is(1)))
                .andExpect(jsonPath("$.dateTime", Matchers.notNullValue()))
                .andExpect(jsonPath("$.group", Matchers.notNullValue()))
                .andExpect(jsonPath("$.auditorium", Matchers.notNullValue()))
                .andExpect(jsonPath("$.course", Matchers.notNullValue()))
                .andExpect(jsonPath("$.teacher", Matchers.notNullValue()));
        verify(lessonService, times(1)).getLessonById(anyLong());
        verifyNoMoreInteractions(lessonService);
    }
    
    @Test
    void testCreateLessonShouldCreateLessonEntryAndReturnCreatedEntry() throws Exception {
        when(lessonService.addLesson(Mockito.any(Lesson.class))).thenReturn(testLesson);
        testLesson.setLessonId(0);
        checkLessonStub();
        mockMvc.perform(post("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testLesson)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTime", Matchers.notNullValue()))
                .andExpect(jsonPath("$.group", Matchers.notNullValue()))
                .andExpect(jsonPath("$.auditorium", Matchers.notNullValue()))
                .andExpect(jsonPath("$.course", Matchers.notNullValue()))
                .andExpect(jsonPath("$.teacher", Matchers.notNullValue()));
    }
    
    @Test
    void testCreateLessonShouldReturnStatusBadRequestAndValidationErrorForLessonFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(post("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Lesson())))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTime", is("must not be null")))
                .andExpect(jsonPath("$.group", is("must not be null")))
                .andExpect(jsonPath("$.auditorium", is("must not be null")))
                .andExpect(jsonPath("$.course", is("must not be null")))
                .andExpect(jsonPath("$.teacher", is("must not be null")));
    }
    
    @Test
    void testCreateLessonShouldReturnStatusBadRequestAndValidationErrorForLessonIdWhenIdMoreThanZero() throws Exception {
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        mockMvc.perform(post("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testLesson)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.lessonId", is("Lesson ID for creating must be zero")));
    }
    
    @Test
    void testCreateLessonShouldReturnStatusBadRequestAndValidationErrorForLessonWhenAuditoriumForPassedLessonIsBusy() throws Exception {
        testLesson.setLessonId(0);
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        testAuditorium.setLessons(singletonList(existingLesson));
        checkLessonStub();
        mockMvc.perform(post("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testLesson)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditorium", 
                        Matchers.stringContainsInOrder("At this time in the auditorium 10 another lesson")));
    }
    
    @Test
    void testCreateLessonShouldReturnStatusBadRequestAndValidationErrorForLessonWhenGroupForPassedLessonIsBusy() throws Exception {
        testLesson.setLessonId(0);
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        testGroup.setLessons(singletonList(existingLesson));
        checkLessonStub();
        mockMvc.perform(post("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testLesson)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.group", 
                        Matchers.stringContainsInOrder("At this time the group group is at another lesson")));
    }
    
    @Test
    void testCreateLessonShouldReturnStatusBadRequestAndValidationErrorForLessonWhenTeacherForPassedLessonIsBusy() throws Exception {
        testLesson.setLessonId(0);
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        testTeacher.setLessons(singletonList(existingLesson));
        checkLessonStub();
        mockMvc.perform(post("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testLesson)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.['teacher.lessons']", 
                        Matchers.stringContainsInOrder("At this time teacher teacher has another lesson")));
    }
    
    @Test
    void testCreateLessonShouldReturnStatusBadRequestAndValidationErrorForLessonWhenWrongTeacherForPassedLesson() throws Exception {
        testLesson.setLessonId(0);
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        testTeacher.setCourses(Collections.emptySet());
        checkLessonStub();
        mockMvc.perform(post("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testLesson)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.['teacher.courses']", 
                        Matchers.stringContainsInOrder("The teacher does not know this course")));
    }

    @Test
    void testCreateDateTimeShouldCreateDateTimeEntryAndReturnCreatedEntry() throws Exception {
        when(dateTimeService.addDateTime(Mockito.any(DateTime.class))).thenReturn(testDateTime);
        mockMvc.perform(post("/api/timetable/datetimes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)))))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTimeId", is(1)))
                .andExpect(jsonPath("$.lessonDate", is(testDateTime.getLessonDate().toString())))
                .andExpect(jsonPath("$.startLesson", is(testDateTime.getStartLesson().toString())))
                .andExpect(jsonPath("$.endLesson", is(testDateTime.getEndLesson().toString())));
        verify(dateTimeService, times(1)).addDateTime(Mockito.any(DateTime.class));
    }
    
    @Test
    void testCreateDateTimeShouldReturnStatusBadRequestAndValidationErrorForDateTimeIdWhenIdMoreThanZero() throws Exception {
        mockMvc.perform(post("/api/timetable/datetimes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new DateTime(2, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTimeId", is("DateTime ID for creating must be zero")));
        verifyNoInteractions(dateTimeService);
    }
    
    @Test
    void testCreateDateTimeShouldReturnHttpStatusBadRequestAndGlobalValidationErrorWhenPassedDateTimeAlreadyExists() throws Exception {
        when(dateTimeRepository.findAllByLessonDate(Mockito.any(LocalDate.class))).thenReturn(singletonList(testDateTime));
        mockMvc.perform(post("/api/timetable/datetimes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTime-CheckExisting", is("Date time already exists")));
        verifyNoInteractions(dateTimeService);
    }
    
    @Test
    void testCreateDateTimeShouldReturnStatusBadRequestAndValidationErrorForFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(post("/api/timetable/datetimes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new DateTime())))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.lessonDate", is("Date must not be null")))
                .andExpect(jsonPath("$.startLesson", is("Start lesson time must not be null")))
                .andExpect(jsonPath("$.endLesson", is("End lesson time must not be null")));
        verifyNoInteractions(dateTimeService);
    }
    
    @Test
    void testUpdateDateTimeShouldUpdateDateTimeEntryAndReturnUpdatedEntry() throws Exception {
        when(dateTimeService.updateDateTime(Mockito.any(DateTime.class))).thenReturn(testDateTime);
        mockMvc.perform(put("/api/timetable/datetimes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTimeId", is(1)))
                .andExpect(jsonPath("$.lessonDate", is(testDateTime.getLessonDate().toString())))
                .andExpect(jsonPath("$.startLesson", is(testDateTime.getStartLesson().toString())))
                .andExpect(jsonPath("$.endLesson", is(testDateTime.getEndLesson().toString())));
        verify(dateTimeService, times(1)).updateDateTime(Mockito.any(DateTime.class));
    }
    
    @Test
    void testUpdateDateTimeShouldReturnStatusBadRequestAndValidationErrorForDateTimeIdWhenPassedIdIsZero() throws Exception {
        mockMvc.perform(put("/api/timetable/datetimes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new DateTime(LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTimeId", is("DateTime ID for updating must be greater than 0")));
        verifyNoInteractions(dateTimeService);
    }
    
    @Test
    void testUpdateDateTimeShouldReturnHttpStatusCode404WhenNonExistingDateTimeIdPassed() throws Exception {
        when(dateTimeService.updateDateTime(Mockito.any(DateTime.class))).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(put("/api/timetable/datetimes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new DateTime(10, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)))))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
    }
    
    @Test
    void testUpdateDateTimeShouldReturnHttpStatusBadRequestAndGlobalValidationErrorWhenDateTimeWithPassedRoomNumberAlreadyExists() throws Exception {
        when(dateTimeRepository.findAllByLessonDate(Mockito.any(LocalDate.class))).thenReturn(singletonList(testDateTime));
        mockMvc.perform(put("/api/timetable/datetimes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new DateTime(2, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTime-CheckExisting", is("Date time already exists")));
        verifyNoInteractions(dateTimeService);
    }
    
    @Test
    void testUpdateDateTimeShouldReturnStatusBadRequestAndValidationErrorForFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(put("/api/timetable/datetimes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new DateTime(1))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.lessonDate", is("Date must not be null")))
                .andExpect(jsonPath("$.startLesson", is("Start lesson time must not be null")))
                .andExpect(jsonPath("$.endLesson", is("End lesson time must not be null")));
        verifyNoInteractions(dateTimeService);
    }
    
    @Test
    void testUpdateLessonShouldUpdateLessonEntryAndReturnUpdatedEntry() throws Exception {
        when(lessonService.updateLesson(Mockito.any(Lesson.class))).thenReturn(testLesson);
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        mockMvc.perform(put("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testLesson)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTime", Matchers.notNullValue()))
                .andExpect(jsonPath("$.group", Matchers.notNullValue()))
                .andExpect(jsonPath("$.auditorium", Matchers.notNullValue()))
                .andExpect(jsonPath("$.course", Matchers.notNullValue()))
                .andExpect(jsonPath("$.teacher", Matchers.notNullValue()));
    }
    
    @Test
    void testUpdateLessonShouldReturnStatusBadRequestAndValidationErrorForLessonFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(put("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Lesson())))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTime", is("must not be null")))
                .andExpect(jsonPath("$.group", is("must not be null")))
                .andExpect(jsonPath("$.auditorium", is("must not be null")))
                .andExpect(jsonPath("$.course", is("must not be null")))
                .andExpect(jsonPath("$.teacher", is("must not be null")));
    }
    
    @Test
    void testUpdateLessonShouldReturnStatusBadRequestAndValidationErrorForLessonIdWhenIdIsZero() throws Exception {
        testLesson.setLessonId(0);
        checkLessonStub();
        mockMvc.perform(put("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testLesson)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.lessonId", is("Lesson ID for updating must be greater than 0")));
    }
    
    @Test
    void testUpdateLessonShouldReturnStatusBadRequestAndValidationErrorForLessonWhenAuditoriumForPassedLessonIsBusy() throws Exception {
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        testAuditorium.setLessons(singletonList(existingLesson));
        mockMvc.perform(put("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testLesson)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditorium", 
                        Matchers.stringContainsInOrder("At this time in the auditorium 10 another lesson")));
    }
    
    @Test
    void testUpdateLessonShouldReturnStatusBadRequestAndValidationErrorForLessonWhenGroupForPassedLessonIsBusy() throws Exception {
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        testGroup.setLessons(singletonList(existingLesson));
        mockMvc.perform(put("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testLesson)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.group", 
                        Matchers.stringContainsInOrder("At this time the group group is at another lesson")));
    }
    
    @Test
    void testUpdateLessonShouldReturnStatusBadRequestAndValidationErrorForLessonWhenTeacherForPassedLessonIsBusy() throws Exception {
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        testTeacher.setLessons(singletonList(existingLesson));
        mockMvc.perform(put("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testLesson)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.['teacher.lessons']", 
                        Matchers.stringContainsInOrder("At this time teacher teacher has another lesson")));
    }
    
    @Test
    void testUpdateLessonShouldReturnStatusBadRequestAndValidationErrorForLessonWhenWrongTeacherForPassedLesson() throws Exception {
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        testTeacher.setCourses(Collections.emptySet());
        mockMvc.perform(put("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testLesson)))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.['teacher.courses']", 
                        Matchers.stringContainsInOrder("The teacher does not know this course")));
    }
    
    @Test
    void testDeleteDateTimeShouldDeleteDateTimeEntryAndReturnDeletedDateTime() throws Exception {
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        mockMvc.perform(delete("/api/timetable/datetimes/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTimeId", is(1)))
                .andExpect(jsonPath("$.lessonDate", is(testDateTime.getLessonDate().toString())))
                .andExpect(jsonPath("$.startLesson", is(testDateTime.getStartLesson().toString())))
                .andExpect(jsonPath("$.endLesson", is(testDateTime.getEndLesson().toString())));
    }
    
    @Test
    void testDeleteDateTimeShouldReturnHttpStatusCode404WhenNonExistingDateTimeIdPassed() throws Exception {
        when(dateTimeService.getById(anyLong())).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(get("/api/timetable/datetimes/{id}", 10L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
        verify(dateTimeService, times(1)).getById(anyLong());
        verifyNoMoreInteractions(dateTimeService);
    }
    
    @Test
    void testDeleteDateTimeShouldReturnHttpStatusCode400WhenDateTimeHasLessons() throws Exception {
        when(dateTimeService.getById(anyLong())).thenThrow(new ServiceException("DateTime has lessons"));
        mockMvc.perform(get("/api/timetable/datetimes/{id}", 10L))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ServiceException", is("DateTime has lessons")));
        verify(dateTimeService, times(1)).getById(anyLong());
        verifyNoMoreInteractions(dateTimeService);
    }
    
    @Test
    void testDeleteLessonShouldDeleteLessonEntryAndReturnDeletedLesson() throws Exception {
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        mockMvc.perform(delete("/api/timetable/lessons/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.lessonId", is(1)));
    }
    
    private void checkLessonStub() {
        when(auditoriumService.getAuditoriumById(anyLong())).thenReturn(testAuditorium);
        when(courseService.getCourseById(anyLong())).thenReturn(testCourse);
        when(dateTimeService.getById(anyLong())).thenReturn(testDateTime);
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        when(teacherService.getTeacherById(anyLong())).thenReturn(testTeacher);   
    }
}
