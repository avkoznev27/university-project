package com.foxminded.university.controllers.rest;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static java.util.Collections.singletonList;
import static java.util.Collections.singleton;

import java.time.LocalDate;
import java.time.LocalTime;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.DateTimeService;
import com.foxminded.university.domain.service.GroupService;
import com.foxminded.university.domain.service.StudentService;
import com.foxminded.university.domain.service.LessonService;
import com.foxminded.university.exceptions.ResourceNotFoundException;

@WebMvcTest(controllers = StudentsRestController.class)
class StudentsRestControllerTest {
    
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    
    @MockBean
    private StudentService studentService;
    @MockBean
    private LessonService lessonService;
    @MockBean
    private GroupService groupService;
    @MockBean
    private DateTimeService dateTimeService;
    
    private Group testGroup;
    private Course testCourse;
    private Student testStudent;
    private Lesson testLesson;
    
    @BeforeEach
    public void setUp() throws Exception {
        testGroup = new Group(1, "testGroup");
        testCourse = new Course(1, "course1", null);
        testStudent = new Student(1, 1, "student1");
        testStudent.getCourses().add(testCourse);
        testStudent.setGroup(testGroup);
        testLesson = new Lesson();
        testLesson.setAuditorium(new Auditorium(1, 10, 100));
        testLesson.setCourse(new Course(1, "course", "description"));
        testLesson.setDateTime(new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)));
        testLesson.setGroup(testGroup);
        testLesson.setTeacher(new Teacher(1, "teacher"));
    }

    @Test
    void testGetAllStudentsShouldReturnFoundStudentEntries() throws Exception {
        when(studentService.getAllStudents()).thenReturn(singletonList(testStudent));
        mockMvc.perform(get("/api/students"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].studentId", is(1)))
                .andExpect(jsonPath("$[0].groupId", is(1)))
                .andExpect(jsonPath("$[0].name", is(testStudent.getName())));
        verify(studentService, times(1)).getAllStudents();
        verifyNoMoreInteractions(studentService);
    }
    
    @Test
    void testCountShouldReturnCountAllStudentEntries() throws Exception {
        when(studentService.countStudents()).thenReturn(10L);
        mockMvc.perform(get("/api/students/count"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(10)));
        verify(studentService, times(1)).countStudents();
        verifyNoMoreInteractions(studentService);
    }
    
    @Test
    void testGetStudentShouldReturnFoundStudentEntryWhenExistingStudentIdPassed() throws Exception {
        when(studentService.getStudentById(anyLong())).thenReturn(testStudent);
        mockMvc.perform(get("/api/students/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.studentId", is(1)))
                .andExpect(jsonPath("$.groupId", is(1)))
                .andExpect(jsonPath("$.name", is(testStudent.getName())));
        verify(studentService, times(1)).getStudentById(anyLong());
        verifyNoMoreInteractions(studentService);
    }
    
    @Test
    void testGetStudentShouldReturnHttpStatusCode404WhenNonExistingStudentIdPassed() throws Exception {
        when(studentService.getStudentById(anyLong())).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(get("/api/students/{id}", 10L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
        verify(studentService, times(1)).getStudentById(anyLong());
        verifyNoMoreInteractions(studentService);
    }
    
    @Test
    void testGetStudentCoursesShouldReturnAllCoursesForStudentFoundByPassedId() throws Exception {
        testStudent.setCourses(singleton(testCourse));
        when(studentService.getStudentById(anyLong())).thenReturn(testStudent);
        mockMvc.perform(get("/api/students/{id}/courses", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].courseId", is(1)))
                .andExpect(jsonPath("$[0].courseName", is(testCourse.getCourseName())))
                .andExpect(jsonPath("$[0].courseDescription", is(testCourse.getCourseDescription())));
        verify(studentService, times(1)).getStudentById(anyLong());
        verifyNoMoreInteractions(studentService);
    }
    
    @Test
    void testGetStudentLessonsShouldReturnAllLessonsForStudentFoundByPassedId() throws Exception {
        testGroup.setLessons(singletonList(testLesson));
        when(studentService.getStudentById(anyLong())).thenReturn(testStudent);
        when(groupService.getGroupById(testStudent.getGroupId())).thenReturn(testGroup);
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        String result = mockMvc.perform(get("/api/students/{id}/lessons", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(testLesson), objectMapper.readerForListOf(Lesson.class).readValue(result));
        verify(studentService, times(1)).getStudentById(anyLong());
        verifyNoMoreInteractions(studentService);
    }
    
    @Test
    void testCreateStudentTimetableShouldReturnStudentLessonsForDayWhenRequestParameterIsDay() throws Exception {
        when(studentService.getStudentById(anyLong())).thenReturn(testStudent);
        when(studentService.getStudentTimetable(Mockito.any(Student.class), Mockito.any(LocalDate.class))).thenReturn(singletonList(testLesson));
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        String result = mockMvc.perform(get("/api/students/{id}/timetable", 1)
                .param("startDay", "2020-09-01"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(testLesson), objectMapper.readerForListOf(Lesson.class).readValue(result));
        String result1 = mockMvc.perform(get("/api/students/{id}/timetable", 1)
                .param("startDay", "2020-09-01")
                .param("endDay", "2020-09-01"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(testLesson), objectMapper.readerForListOf(Lesson.class).readValue(result1));
    }
    
    @Test
    void testCreateStudentTimetableShouldReturnStudentLessonsForPeriodWhenRequestParameterIsPeriodOfDays() throws Exception {
        when(studentService.getStudentById(anyLong())).thenReturn(testStudent);
        when(studentService.getStudentTimetable(Mockito.any(Student.class), Mockito.any(LocalDate.class), Mockito.any(LocalDate.class)))
        .thenReturn(singletonList(testLesson));
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        String result = mockMvc.perform(get("/api/students/{id}/timetable", 1)
                .param("startDay", "2020-09-01")
                .param("endDay", "2020-09-02"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(testLesson), objectMapper.readerForListOf(Lesson.class).readValue(result));
    }
    
    @Test
    void testCreateStudentTimetableShouldReturnHttpStatusBadRequestWhenRequestParameterIsWrongFormat() throws Exception {
        when(studentService.getStudentById(anyLong())).thenReturn(testStudent);
        mockMvc.perform(get("/api/students/{id}/timetable", 1)
                .param("startDay", "20-09-01"))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.DateTimeParseException", Matchers.isA(String.class)));
    }
    
    @Test
    void testCreateStudentShouldCreateStudentEntryAndReturnCreatedEntry() throws Exception {
        when(studentService.addStudent(Mockito.any(Group.class), Mockito.any(Student.class))).thenReturn(testStudent);
        when(groupService.getGroupById(anyLong())).thenReturn(testGroup);
        mockMvc.perform(post("/api/students")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Student(1, "student"))))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.studentId", is(1)))
                .andExpect(jsonPath("$.groupId", is(1)))
                .andExpect(jsonPath("$.name", is(testStudent.getName())));
        verify(studentService, times(1)).addStudent(Mockito.any(Group.class), Mockito.any(Student.class));
    }
    
    @Test
    void testCreateStudentShouldReturnStatusBadRequestAndValidationErrorForStudentIdWhenIdMoreThanZero() throws Exception {
        mockMvc.perform(post("/api/students")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Student(2, 1, "student"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.studentId", is("Student ID for creating must be zero")));
        verifyNoInteractions(studentService);
    }
    
    @Test
    void testCreateStudentShouldReturnStatusBadRequestAndValidationErrorForFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(post("/api/students")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Student(-10, ""))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupId", is("Wrong group or no group selected")))
                .andExpect(jsonPath("$.name", is("Student name must not be empty")));
        
        mockMvc.perform(post("/api/students")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Student())))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupId", is("Wrong group or no group selected")))
                .andExpect(jsonPath("$.name", is("Student name must not be empty")));
        verifyNoInteractions(studentService);
    }
    
    @Test
    void testUpdateStudentShouldUpdateStudentEntryAndReturnUpdatedEntry() throws Exception {
        when(studentService.updateStudent(Mockito.any(Student.class))).thenReturn(testStudent);
        mockMvc.perform(put("/api/students")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Student(1, 1, "student"))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.studentId", is(1)))
                .andExpect(jsonPath("$.groupId", is(1)))
                .andExpect(jsonPath("$.name", is(testStudent.getName())));
        verify(studentService, times(1)).updateStudent(Mockito.any(Student.class));
    }
    
    @Test
    void testUpdateStudentShouldReturnStatusBadRequestAndValidationErrorForStudentIdWhenPassedIdIsZero() throws Exception {
        mockMvc.perform(put("/api/students")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Student(1, "student"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.studentId", is("Student ID for updating must be greater than 0")));
        verifyNoInteractions(studentService);
    }
    
    @Test
    void testUpdateStudentShouldReturnHttpStatusCode404WhenNonExistingStudentIdPassed() throws Exception {
        when(studentService.updateStudent(Mockito.any(Student.class))).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(put("/api/students/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Student(10, 10, "student"))))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
    }
    
    @Test
    void testUpdateStudentShouldReturnStatusBadRequestAndValidationErrorForFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(put("/api/students")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Student(1, 0, ""))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is("Student name must not be empty")));
    }
    
    @Test
    void testDeleteStudentShouldDeleteStudentEntryAndReturnDeletedStudent() throws Exception {
        when(studentService.getStudentById(anyLong())).thenReturn(testStudent);
        mockMvc.perform(delete("/api/students/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.studentId", is(1)))
                .andExpect(jsonPath("$.groupId", is(1)))
                .andExpect(jsonPath("$.name", is(testStudent.getName())));
    }
    
    @Test
    void testDeleteStudentShouldReturnHttpStatusCode404WhenNonExistingStudentIdPassed() throws Exception {
        when(studentService.getStudentById(anyLong())).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(get("/api/students/{id}", 10L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
        verify(studentService, times(1)).getStudentById(anyLong());
        verifyNoMoreInteractions(studentService);
    }
}
