package com.foxminded.university.controllers.rest;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.hamcrest.Matchers.*;
import static java.util.Collections.singletonList;

import java.time.LocalDate;
import java.time.LocalTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.dao.AuditoriumRepository;
import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.dao.DateTimeRepository;
import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.service.CourseService;
import com.foxminded.university.domain.service.LessonService;
import com.foxminded.university.domain.service.StudentService;
import com.foxminded.university.exceptions.ResourceNotFoundException;
import com.foxminded.university.exceptions.ServiceException;

@WebMvcTest(controllers = CoursesRestController.class)
class CoursesRestControllerTest {
    
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    
    @MockBean
    private CourseService courseService;
    @MockBean
    private LessonService lessonService;
    @MockBean
    private StudentService studentService;
    
    @MockBean
    private AuditoriumRepository auditoriumRepository;
    @MockBean
    private CourseRepository courseRepository;
    @MockBean
    private GroupRepository groupRepository;
    @MockBean
    private DateTimeRepository dateTimeRepository;

    private Course testCourse;
    private Lesson testLesson;
    
    @BeforeEach
    public void setUp() throws Exception {
        testCourse = new Course(1, "course", "description");
        testLesson = new Lesson();
        testLesson.setAuditorium(new Auditorium(1, 10, 100));
        testLesson.setCourse(testCourse);
        testLesson.setDateTime(new DateTime(1, LocalDate.of(2020, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)));
        testLesson.setGroup(new Group(1, "group"));
        testLesson.setTeacher(new Teacher(1, "teacher"));
    }

    @Test
    void testGetAllCoursesShouldReturnFoundCourseEntries() throws Exception {
        when(courseService.getAllCourses()).thenReturn(singletonList(testCourse));
        mockMvc.perform(get("/api/courses"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].courseId", is(1)))
                .andExpect(jsonPath("$[0].courseName", is(testCourse.getCourseName())))
                .andExpect(jsonPath("$[0].courseDescription", is(testCourse.getCourseDescription())));
        verify(courseService, times(1)).getAllCourses();
        verifyNoMoreInteractions(courseService);
    }
    
    @Test
    void testCountShouldReturnCountAllCourseEntries() throws Exception {
        when(courseService.countCourses()).thenReturn(10L);
        mockMvc.perform(get("/api/courses/count"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(10)));
        verify(courseService, times(1)).countCourses();
        verifyNoMoreInteractions(courseService);
    }
    
    @Test
    void testGetCourseShouldReturnFoundCourseEntryWhenExistingCourseIdPassed() throws Exception {
        when(courseService.getCourseById(anyLong())).thenReturn(testCourse);
        mockMvc.perform(get("/api/courses/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.courseId", is(1)))
                .andExpect(jsonPath("$.courseName", is(testCourse.getCourseName())))
                .andExpect(jsonPath("$.courseDescription", is(testCourse.getCourseDescription())));
        verify(courseService, times(1)).getCourseById(anyLong());
        verifyNoMoreInteractions(courseService);
    }
    
    @Test
    void testGetCourseShouldReturnHttpStatusCode404WhenNonExistingCourseIdPassed() throws Exception {
        when(courseService.getCourseById(anyLong())).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(get("/api/courses/{id}", 10L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
        verify(courseService, times(1)).getCourseById(anyLong());
        verifyNoMoreInteractions(courseService);
    }
    
    @Test
    void testGetCourseLessonsShouldReturnAllLessonsForCourseFoundByPassedId() throws Exception {
        testCourse.setLessons(singletonList(testLesson));
        when(courseService.getCourseById(anyLong())).thenReturn(testCourse);
        when(lessonService.getLessonById(anyLong())).thenReturn(testLesson);
        String result = mockMvc.perform(get("/api/courses/{id}/lessons", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(testLesson), objectMapper.readerForListOf(Lesson.class).readValue(result));
        verify(courseService, times(1)).getCourseById(anyLong());
        verifyNoMoreInteractions(courseService);
    }
    
    @Test
    void testGetCourseStudentsShouldReturnAllStudentsForCourseFoundByPassedId() throws Exception {
        Student student = new Student(1, 1, "Student");
        testCourse.setStudents(singletonList(student));
        when(courseService.getCourseById(anyLong())).thenReturn(testCourse);
        String result = mockMvc.perform(get("/api/courses/{id}/students", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(student), objectMapper.readerForListOf(Student.class).readValue(result));
        verify(courseService, times(1)).getCourseById(anyLong());
        verifyNoMoreInteractions(courseService);
    }
    
    @Test
    void testGetCourseTeachersShouldReturnAllTeachersForCourseFoundByPassedId() throws Exception {
        Teacher teacher = new Teacher(1, "teacher");
        testCourse.setTeachers(singletonList(teacher));
        when(courseService.getCourseById(anyLong())).thenReturn(testCourse);
        String result = mockMvc.perform(get("/api/courses/{id}/teachers", 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(singletonList(teacher), objectMapper.readerForListOf(Teacher.class).readValue(result));
        verify(courseService, times(1)).getCourseById(anyLong());
        verifyNoMoreInteractions(courseService);
    }
    
    @Test
    void testCreateCourseShouldCreateCourseEntryAndReturnCreatedEntry() throws Exception {
        when(courseService.addCourse(Mockito.any(Course.class))).thenReturn(testCourse);
        mockMvc.perform(post("/api/courses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Course("course", "description"))))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.courseId", is(1)))
                .andExpect(jsonPath("$.courseName", is(testCourse.getCourseName())))
                .andExpect(jsonPath("$.courseDescription", is(testCourse.getCourseDescription())));
        verify(courseService, times(1)).addCourse(Mockito.any(Course.class));
    }
    
    @Test
    void testCreateCourseShouldReturnStatusBadRequestAndValidationErrorForCourseIdWhenIdMoreThanZero() throws Exception {
        mockMvc.perform(post("/api/courses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Course(2, "course", "description"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.courseId", is("Course ID for creating must be zero")));
        verifyNoInteractions(courseService);
    }
    
    @Test
    void testCreateCourseShouldReturnHttpStatusBadRequestAndGlobalValidationErrorWhenCourseWithPassedRoomNumberAlreadyExists() throws Exception {
        when(courseRepository.getByCourseName(anyString())).thenReturn(testCourse);
        mockMvc.perform(post("/api/courses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Course("course", "description"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.course-CheckExisting", is("Course with the same name already exists")));
        verifyNoInteractions(courseService);
    }
    
    @Test
    void testCreateCourseShouldReturnStatusBadRequestAndValidationErrorForFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(post("/api/courses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Course("", ""))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.courseName", is("Course name must not be empty")))
                .andExpect(jsonPath("$.courseDescription", is("Course description must not be empty")));
        
        mockMvc.perform(post("/api/courses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Course())))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.courseName", is("Course name must not be empty")))
                .andExpect(jsonPath("$.courseDescription", is("Course description must not be empty")));
        verifyNoInteractions(courseService);
    }
    
    @Test
    void testUpdateCourseShouldUpdateCourseEntryAndReturnUpdatedEntry() throws Exception {
        when(courseService.updateCourse(Mockito.any(Course.class))).thenReturn(testCourse);
        mockMvc.perform(put("/api/courses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Course(1, "course", "description"))))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.courseId", is(1)))
                .andExpect(jsonPath("$.courseName", is(testCourse.getCourseName())))
                .andExpect(jsonPath("$.courseDescription", is(testCourse.getCourseDescription())));
        verify(courseService, times(1)).updateCourse(Mockito.any(Course.class));
    }
    
    @Test
    void testUpdateCourseShouldReturnStatusBadRequestAndValidationErrorForCourseIdWhenPassedIdIsZero() throws Exception {
        mockMvc.perform(put("/api/courses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Course("course", "description"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.courseId", is("Course ID for updating must be greater than 0")));
        verifyNoInteractions(courseService);
    }
    
    @Test
    void testUpdateCourseShouldReturnHttpStatusCode404WhenNonExistingCourseIdPassed() throws Exception {
        when(courseService.updateCourse(Mockito.any(Course.class))).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(put("/api/courses/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Course(10, "course", "description"))))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
    }
    
    @Test
    void testUpdateCourseShouldReturnHttpStatusBadRequestAndGlobalValidationErrorWhenCourseWithPassedRoomNumberAlreadyExists() throws Exception {
        when(courseRepository.getByCourseName(anyString())).thenReturn(testCourse);
        mockMvc.perform(put("/api/courses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Course(2, "course", "description"))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.course-CheckExisting", is("Course with the same name already exists")));
        verifyNoInteractions(courseService);
    }
    
    @Test
    void testUpdateCourseShouldReturnStatusBadRequestAndValidationErrorForFieldsWhenNotValidDataPassed() throws Exception {
        mockMvc.perform(put("/api/courses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new Course(1, "", ""))))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.courseName", is("Course name must not be empty")))
                .andExpect(jsonPath("$.courseDescription", is("Course description must not be empty")));
    }
    
    @Test
    void testDeleteCourseShouldDeleteCourseEntryAndReturnDeletedCourse() throws Exception {
        when(courseService.getCourseById(anyLong())).thenReturn(testCourse);
        mockMvc.perform(delete("/api/courses/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.courseId", is(1)))
                .andExpect(jsonPath("$.courseName", is(testCourse.getCourseName())))
                .andExpect(jsonPath("$.courseDescription", is(testCourse.getCourseDescription())));
    }
    
    @Test
    void testDeleteCourseShouldReturnHttpStatusCode404WhenNonExistingCourseIdPassed() throws Exception {
        when(courseService.getCourseById(anyLong())).thenThrow(new ResourceNotFoundException("Message"));
        mockMvc.perform(get("/api/courses/{id}", 10L))
                .andExpect(status().isNotFound())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ResourceNotFoundException", is("Message")));
        verify(courseService, times(1)).getCourseById(anyLong());
        verifyNoMoreInteractions(courseService);
    }
    
    @Test
    void testDeleteCourseShouldReturnHttpStatusCode400WhenCourseHasLessons() throws Exception {
        when(courseService.getCourseById(anyLong())).thenThrow(new ServiceException("Course has lessons"));
        mockMvc.perform(get("/api/courses/{id}", 10L))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ServiceException", is("Course has lessons")));
        verify(courseService, times(1)).getCourseById(anyLong());
        verifyNoMoreInteractions(courseService);
    }
}
