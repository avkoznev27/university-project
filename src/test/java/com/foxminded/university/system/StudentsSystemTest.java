package com.foxminded.university.system;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.apache.commons.collections.CollectionUtils.isEqualCollection;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.dao.StudentRepository;
import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.Lesson;

@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Sql(scripts = { "/create_test_tables.sql", "/testData.sql" })
@AutoConfigureMockMvc
class StudentsSystemTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private GroupRepository groupRepository;

    @Test
    void testGetAllStudentsShouldRetrieveAllStudentsFromDataBase() throws Exception {
        String result = mockMvc.perform(get("/api/students"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(3)))
                .andReturn().getResponse().getContentAsString();
        List<Student> actual = objectMapper.readerForListOf(Student.class).readValue(result);
        List<Student> expected = studentRepository.findAll();
        assertEquals(expected, actual);
    }

    @Test
    void testCountStudentsShouldGetCountStudentsInDataBase() throws Exception {
        String result = mockMvc.perform(get("/api/students/count"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(3)))
                .andReturn().getResponse().getContentAsString();
        long actual = objectMapper.readValue(result, Long.class);
        long expected = studentRepository.count();
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetStudentShouldRetrieveStudentFromDataBaseById() throws Exception {
        long studentId = 1;
        Student expectedStudent = studentRepository.findById(studentId).get();
        String result = mockMvc.perform(get("/api/students/{id}", studentId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.studentId", is(1)))
                .andExpect(jsonPath("$.groupId", is(1)))
                .andExpect(jsonPath("$.name", is(expectedStudent.getName())))
                .andReturn().getResponse().getContentAsString();
        Student actualStudent = objectMapper.readValue(result, Student.class);
        assertEquals(expectedStudent, actualStudent);
    }
    
    @Test
    void testGetStudentLessonsShouldRetrieveAllLessonsForStudentById() throws Exception {
        long studentId = 1;
        Student student = studentRepository.findById(studentId).get();
        List<Lesson> expectedLessons = groupRepository.findById(student.getGroupId()).get().getLessons();
        String result = mockMvc.perform(get("/api/students/{id}/lessons", studentId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
        List<Lesson> actuaLessons = objectMapper.readerForListOf(Lesson.class).readValue(result);
        assertTrue(isEqualCollection(expectedLessons, actuaLessons));
    }
    
    @Test
    void testGetStudentCoursesShouldRetrieveAllCoursesForStudentById() throws Exception {
        long studentId = 1;
        Set<Course> expectedCourses = studentRepository.findById(studentId).get().getCourses();
        String result = mockMvc.perform(get("/api/students/{id}/courses", studentId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andReturn().getResponse().getContentAsString();
        List<Course> actualCourses = objectMapper.readerForListOf(Course.class).readValue(result);
        assertTrue(isEqualCollection(expectedCourses, actualCourses));
    }
    
    @Test
    void testPostStudentShouldCreateStudentEntryInDataBase() throws Exception {
        Student student = new Student(1, "test student");
        long numberOfEntriesBeforePost = studentRepository.count();
        mockMvc.perform(post("/api/students")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(student)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.studentId", is(4)))
                .andExpect(jsonPath("$.groupId", is(1)))
                .andExpect(jsonPath("$.name", is(student.getName())));
        long numberOfEntriesAfterPost = studentRepository.count();
        assertEquals(student.getName(), studentRepository.findById(4L).get().getName());
        assertEquals(1, numberOfEntriesAfterPost - numberOfEntriesBeforePost);
    }
    
    @Test
    void testPutStudentShouldUpdateStudentEntryInDataBase() throws Exception {
        Student student = new Student(1, 2, "updated student");
        Student studentBeforeUpdating = studentRepository.findById(1L).get();
        mockMvc.perform(put("/api/students")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(student)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.studentId", is(1)))
                .andExpect(jsonPath("$.groupId", is(2)))
                .andExpect(jsonPath("$.name", is(student.getName())));
        Student studentAfterUpdating = studentRepository.findById(1L).get();
        assertNotEquals(student, studentBeforeUpdating);
        assertEquals(student, studentAfterUpdating);
    }
    
    @Test
    void testDeleteStudentSchouldDeleteStudentEntryFromDataBase() throws Exception {
        studentRepository.save(new Student(1, "new student"));
        long numberOfEntriesBeforeDelete = studentRepository.count();
        mockMvc.perform(delete("/api/students/{id}", 4L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.studentId", is(4)));
        long numberOfEntriesAfterDelete = studentRepository.count();
        assertEquals(1, numberOfEntriesBeforeDelete - numberOfEntriesAfterDelete);
        assertFalse(studentRepository.findById(4L).isPresent());
    }
}
