package com.foxminded.university.system;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.apache.commons.collections.CollectionUtils.isEqualCollection;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.dao.GroupRepository;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Student;

@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Sql(scripts = { "/create_test_tables.sql", "/testData.sql" })
@AutoConfigureMockMvc
class GroupsSystemTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private GroupRepository groupRepository;

    @Test
    void testGetAllGroupsShouldRetrieveAllGroupsFromDataBase() throws Exception {
        String result = mockMvc.perform(get("/api/groups"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(3)))
                .andReturn().getResponse().getContentAsString();
        List<Group> actual = objectMapper.readerForListOf(Group.class).readValue(result);
        List<Group> expected = groupRepository.findAll();
        assertEquals(expected, actual);
    }

    @Test
    void testCountGroupsShouldGetCountGroupsInDataBase() throws Exception {
        String result = mockMvc.perform(get("/api/groups/count"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(3)))
                .andReturn().getResponse().getContentAsString();
        long actual = objectMapper.readValue(result, Long.class);
        long expected = groupRepository.count();
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetGroupShouldRetrieveGroupFromDataBaseById() throws Exception {
        long groupId = 1;
        Group expectedGroup = groupRepository.findById(groupId).get();
        String result = mockMvc.perform(get("/api/groups/{id}", groupId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupId", is(1)))
                .andExpect(jsonPath("$.groupName", is(expectedGroup.getGroupName())))
                .andReturn().getResponse().getContentAsString();
        Group actualGroup = objectMapper.readValue(result, Group.class);
        assertEquals(expectedGroup, actualGroup);
    }
    
    @Test
    void testGetGroupLessonsShouldRetrieveAllLessonsForGroupById() throws Exception {
        long groupId = 1;
        List<Lesson> expectedLessons = groupRepository.findById(groupId).get().getLessons();
        String result = mockMvc.perform(get("/api/groups/{id}/lessons", groupId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
        List<Lesson> actuaLessons = objectMapper.readerForListOf(Lesson.class).readValue(result);
        assertTrue(isEqualCollection(expectedLessons, actuaLessons));
    }
    
    @Test
    void testGetGroupStudentsShouldRetrieveAllStudentsForGroupById() throws Exception {
        long groupId = 1;
        List<Student> expectedStudents = groupRepository.findById(groupId).get().getStudents();
        String result = mockMvc.perform(get("/api/groups/{id}/students", groupId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andReturn().getResponse().getContentAsString();
        List<Lesson> actualStudents = objectMapper.readerForListOf(Student.class).readValue(result);
        assertTrue(isEqualCollection(expectedStudents, actualStudents));
    }
    
    @Test
    void testPostGroupShouldCreateGroupEntryInDataBase() throws Exception {
        Group group = new Group("test group");
        long numberOfEntriesBeforePost = groupRepository.count();
        mockMvc.perform(post("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(group)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupId", is(4)))
                .andExpect(jsonPath("$.groupName", is(group.getGroupName())));
        long numberOfEntriesAfterPost = groupRepository.count();
        assertNotNull(groupRepository.getByGroupName(group.getGroupName()));
        assertEquals(group.getGroupName(), groupRepository.findById(4L).get().getGroupName());
        assertEquals(1, numberOfEntriesAfterPost - numberOfEntriesBeforePost);
    }
    
    @Test
    void testPutGroupShouldUpdateGroupEntryInDataBase() throws Exception {
        Group group = new Group(1, "updated group");
        Group groupBeforeUpdating = groupRepository.findById(1L).get();
        mockMvc.perform(put("/api/groups")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(group)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupId", is(1)))
                .andExpect(jsonPath("$.groupName", is(group.getGroupName())));
        Group groupAfterUpdating = groupRepository.findById(1L).get();
        assertNotEquals(group, groupBeforeUpdating);
        assertEquals(group, groupAfterUpdating);
    }
    
    @Test
    void testDeleteGroupSchouldDeleteGroupEntryFromDataBase() throws Exception {
        groupRepository.save(new Group("new group"));
        long numberOfEntriesBeforeDelete = groupRepository.count();
        mockMvc.perform(delete("/api/groups/{id}", 4L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.groupId", is(4)));
        long numberOfEntriesAfterDelete = groupRepository.count();
        assertEquals(1, numberOfEntriesBeforeDelete - numberOfEntriesAfterDelete);
        assertFalse(groupRepository.findById(4L).isPresent());
    }
}
