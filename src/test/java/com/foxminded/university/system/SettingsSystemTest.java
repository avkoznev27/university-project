package com.foxminded.university.system;

import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.dao.SettingsRepository;
import com.foxminded.university.domain.model.Settings;

@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Sql(scripts = { "/create_test_tables.sql", "/testData.sql" })
@AutoConfigureMockMvc
class SettingsSystemTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private SettingsRepository settingsRepository;

    @Test
    void testGetSettingsShouldReturnCurrentSettingsEntry() throws Exception {
        Settings expectedSettings = settingsRepository.findById(1L).get();
        String result = mockMvc.perform(get("/api/settings/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.lessonDuration", is(expectedSettings.getLessonDuration())))
                .andExpect(jsonPath("$.maxGroupSize", is(expectedSettings.getMaxGroupSize())))
                .andReturn().getResponse().getContentAsString();
        Settings actualSettings = objectMapper.readValue(result, Settings.class);
        assertEquals(expectedSettings, actualSettings);
    }

    @Test
    void testSetDefaultShouldSetDefaultAndReturnSettingsEntry() throws Exception {
        Settings defaultSettings = new Settings(1, 45, 30);
        String result = mockMvc.perform(post("/api/settings/default"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.lessonDuration", is(defaultSettings.getLessonDuration())))
                .andExpect(jsonPath("$.maxGroupSize", is(defaultSettings.getMaxGroupSize())))
                .andReturn().getResponse().getContentAsString();
        assertEquals(defaultSettings, objectMapper.readValue(result, Settings.class));
    }

    @Test
    void testUpdateSettingsShouldReturnStatusIsOkAndReturnUpdatedSettingsEntryWhenValidDataPassed() throws Exception {
        Settings updatedSettings = new Settings(1, 90, 60);
        String result = mockMvc.perform(put("/api/settings/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedSettings)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.lessonDuration", is(90)))
                .andExpect(jsonPath("$.maxGroupSize", is(60)))
                .andReturn().getResponse().getContentAsString();
        assertEquals(updatedSettings, objectMapper.readValue(result, Settings.class));
    }
}
