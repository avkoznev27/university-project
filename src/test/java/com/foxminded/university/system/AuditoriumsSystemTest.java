package com.foxminded.university.system;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.apache.commons.collections.CollectionUtils.isEqualCollection;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.dao.AuditoriumRepository;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Lesson;

@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Sql(scripts = { "/create_test_tables.sql", "/testData.sql" })
@AutoConfigureMockMvc
class AuditoriumsSystemTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private AuditoriumRepository auditoriumRepository;

    @Test
    void testGetAllAuditoriumsShouldRetrieveAllAuditoriumsFromDataBase() throws Exception {
        String result = mockMvc.perform(get("/api/auditoriums"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(3)))
                .andReturn().getResponse().getContentAsString();
        List<Auditorium> actual = objectMapper.readerForListOf(Auditorium.class).readValue(result);
        List<Auditorium> expected = auditoriumRepository.findAll();
        assertEquals(expected, actual);
    }

    @Test
    void testCountAuditoriumsShouldGetCountAuditoriumsInDataBase() throws Exception {
        String result = mockMvc.perform(get("/api/auditoriums/count"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(3)))
                .andReturn().getResponse().getContentAsString();
        long actual = objectMapper.readValue(result, Long.class);
        long expected = auditoriumRepository.count();
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetAuditoriumShouldRetrieveAuditoriumFromDataBaseById() throws Exception {
        long auditoriumId = 1;
        Auditorium expectedAuditorium = auditoriumRepository.findById(auditoriumId).get();
        String result = mockMvc.perform(get("/api/auditoriums/{id}", auditoriumId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditoriumId", is(1)))
                .andExpect(jsonPath("$.roomNumber", is(expectedAuditorium.getRoomNumber())))
                .andExpect(jsonPath("$.capacity", is(expectedAuditorium.getCapacity())))
                .andReturn().getResponse().getContentAsString();
        Auditorium actualAuditorium = objectMapper.readValue(result, Auditorium.class);
        assertEquals(expectedAuditorium, actualAuditorium);
    }
    
    @Test
    void testGetAuditoriumLessonsShouldRetrieveAllLessonsForAuditoriumById() throws Exception {
        long auditoriumId = 1;
        List<Lesson> expectedLessons = auditoriumRepository.findById(auditoriumId).get().getLessons();
        String result = mockMvc.perform(get("/api/auditoriums/{id}/lessons", auditoriumId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        List<Lesson> actuaLessons = objectMapper.readerForListOf(Lesson.class).readValue(result);
        assertTrue(isEqualCollection(expectedLessons, actuaLessons));
    }
    
    @Test
    void testPostAuditoriumShouldCreateAuditoriumEntryInDataBase() throws Exception {
        Auditorium auditorium = new Auditorium(100, 100);
        long numberOfEntriesBeforePost = auditoriumRepository.count();
        mockMvc.perform(post("/api/auditoriums")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(auditorium)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditoriumId", is(4)))
                .andExpect(jsonPath("$.roomNumber", is(auditorium.getRoomNumber())))
                .andExpect(jsonPath("$.capacity", is(auditorium.getCapacity())));
        long numberOfEntriesAfterPost = auditoriumRepository.count();
        assertNotNull(auditoriumRepository.getByRoomNumber(100));
        assertEquals(auditorium.getRoomNumber(), auditoriumRepository.findById(4L).get().getRoomNumber());
        assertEquals(1, numberOfEntriesAfterPost - numberOfEntriesBeforePost);
    }
    
    @Test
    void testPutAuditoriumShouldUpdateAuditoriumEntryInDataBase() throws Exception {
        Auditorium auditorium = new Auditorium(1, 199, 29);
        Auditorium auditoriumBeforeUpdating = auditoriumRepository.findById(1L).get();
        mockMvc.perform(put("/api/auditoriums")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(auditorium)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditoriumId", is(1)))
                .andExpect(jsonPath("$.roomNumber", is(auditorium.getRoomNumber())))
                .andExpect(jsonPath("$.capacity", is(auditorium.getCapacity())));
        Auditorium auditoriumAfterUpdating = auditoriumRepository.findById(1L).get();
        assertNotEquals(auditorium, auditoriumBeforeUpdating);
        assertEquals(auditorium, auditoriumAfterUpdating);
    }
    
    @Test
    void testDeleteAuditoriumSchouldDeleteAuditoriumEntryFromDataBase() throws Exception {
        auditoriumRepository.save(new Auditorium(199, 29));
        long numberOfEntriesBeforeDelete = auditoriumRepository.count();
        mockMvc.perform(delete("/api/auditoriums/{id}", 4L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.auditoriumId", is(4)));
        long numberOfEntriesAfterDelete = auditoriumRepository.count();
        assertEquals(1, numberOfEntriesBeforeDelete - numberOfEntriesAfterDelete);
        assertFalse(auditoriumRepository.findById(4L).isPresent());
    }
}
