package com.foxminded.university.system;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.apache.commons.collections.CollectionUtils.isEqualCollection;

import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.dao.TeacherRepository;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.Lesson;

@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Sql(scripts = { "/create_test_tables.sql", "/testData.sql" })
@AutoConfigureMockMvc
class TeachersSystemTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private TeacherRepository teacherRepository;

    @Test
    void testGetAllTeachersShouldRetrieveAllTeachersFromDataBase() throws Exception {
        String result = mockMvc.perform(get("/api/teachers"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(3)))
                .andReturn().getResponse().getContentAsString();
        List<Teacher> actual = objectMapper.readerForListOf(Teacher.class).readValue(result);
        List<Teacher> expected = teacherRepository.findAll();
        assertEquals(expected, actual);
    }

    @Test
    void testCountTeachersShouldGetCountTeachersInDataBase() throws Exception {
        String result = mockMvc.perform(get("/api/teachers/count"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(3)))
                .andReturn().getResponse().getContentAsString();
        long actual = objectMapper.readValue(result, Long.class);
        long expected = teacherRepository.count();
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetTeacherShouldRetrieveTeacherFromDataBaseById() throws Exception {
        long teacherId = 1;
        Teacher expectedTeacher = teacherRepository.findById(teacherId).get();
        String result = mockMvc.perform(get("/api/teachers/{id}", teacherId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.teacherId", is(1)))
                .andExpect(jsonPath("$.name", is(expectedTeacher.getName())))
                .andReturn().getResponse().getContentAsString();
        Teacher actualTeacher = objectMapper.readValue(result, Teacher.class);
        assertEquals(expectedTeacher, actualTeacher);
    }
    
    @Test
    void testGetTeacherLessonsShouldRetrieveAllLessonsForTeacherById() throws Exception {
        long teacherId = 2;
        List<Lesson> expectedLessons = teacherRepository.findById(teacherId).get().getLessons();
        String result = mockMvc.perform(get("/api/teachers/{id}/lessons", teacherId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
        List<Lesson> actuaLessons = objectMapper.readerForListOf(Lesson.class).readValue(result);
        assertTrue(isEqualCollection(expectedLessons, actuaLessons));
    }
    
    @Test
    void testGetTeacherCoursesShouldRetrieveAllCoursesForTeacherById() throws Exception {
        long teacherId = 1;
        Set<Course> expectedCourses = teacherRepository.findById(teacherId).get().getCourses();
        String result = mockMvc.perform(get("/api/teachers/{id}/courses", teacherId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andReturn().getResponse().getContentAsString();
        List<Course> actualCourses = objectMapper.readerForListOf(Course.class).readValue(result);
        assertTrue(isEqualCollection(expectedCourses, actualCourses));
    }
    
    @Test
    void testPostTeacherShouldCreateTeacherEntryInDataBase() throws Exception {
        Teacher teacher = new Teacher("test teacher");
        long numberOfEntriesBeforePost = teacherRepository.count();
        mockMvc.perform(post("/api/teachers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(teacher)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.teacherId", is(4)))
                .andExpect(jsonPath("$.name", is(teacher.getName())));
        long numberOfEntriesAfterPost = teacherRepository.count();
        assertEquals(teacher.getName(), teacherRepository.findById(4L).get().getName());
        assertEquals(1, numberOfEntriesAfterPost - numberOfEntriesBeforePost);
    }
    
    @Test
    void testPutTeacherShouldUpdateTeacherEntryInDataBase() throws Exception {
        Teacher teacher = new Teacher(1, "updated teacher");
        Teacher teacherBeforeUpdating = teacherRepository.findById(1L).get();
        mockMvc.perform(put("/api/teachers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(teacher)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.teacherId", is(1)))
                .andExpect(jsonPath("$.name", is(teacher.getName())));
        Teacher teacherAfterUpdating = teacherRepository.findById(1L).get();
        assertNotEquals(teacher, teacherBeforeUpdating);
        assertEquals(teacher, teacherAfterUpdating);
    }
    
    @Test
    void testDeleteTeacherSchouldDeleteTeacherEntryFromDataBase() throws Exception {
        teacherRepository.save(new Teacher("new teacher"));
        long numberOfEntriesBeforeDelete = teacherRepository.count();
        mockMvc.perform(delete("/api/teachers/{id}", 4L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.teacherId", is(4)));
        long numberOfEntriesAfterDelete = teacherRepository.count();
        assertEquals(1, numberOfEntriesBeforeDelete - numberOfEntriesAfterDelete);
        assertFalse(teacherRepository.findById(4L).isPresent());
    }
}
