package com.foxminded.university.system;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.apache.commons.collections.CollectionUtils.isEqualCollection;

import java.util.List;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.dao.CourseRepository;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.Lesson;
import com.foxminded.university.domain.model.Student;
import com.foxminded.university.domain.model.Teacher;

@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Sql(scripts = { "/create_test_tables.sql", "/testData.sql" })
@AutoConfigureMockMvc
class CoursesSystemTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private CourseRepository courseRepository;

    @Test
    void testGetAllCoursesShouldRetrieveAllCoursesFromDataBase() throws Exception {
        String result = mockMvc.perform(get("/api/courses"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(3)))
                .andReturn().getResponse().getContentAsString();
        List<Course> actual = objectMapper.readerForListOf(Course.class).readValue(result);
        List<Course> expected = courseRepository.findAll();
        assertEquals(expected, actual);
    }

    @Test
    void testCountCoursesShouldGetCountCoursesInDataBase() throws Exception {
        String result = mockMvc.perform(get("/api/courses/count"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", is(3)))
                .andReturn().getResponse().getContentAsString();
        long actual = objectMapper.readValue(result, Long.class);
        long expected = courseRepository.count();
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetCourseShouldRetrieveCourseFromDataBaseById() throws Exception {
        long courseId = 1;
        Course expectedCourse = courseRepository.findById(courseId).get();
        String result = mockMvc.perform(get("/api/courses/{id}", courseId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.courseId", is(1)))
                .andExpect(jsonPath("$.courseName", is(expectedCourse.getCourseName())))
                .andExpect(jsonPath("$.courseDescription", is(expectedCourse.getCourseDescription())))
                .andReturn().getResponse().getContentAsString();
        Course actualCourse = objectMapper.readValue(result, Course.class);
        assertEquals(expectedCourse, actualCourse);
    }
    
    @Test
    void testGetCourseLessonsShouldRetrieveAllLessonsForCourseById() throws Exception {
        long courseId = 1;
        List<Lesson> expectedLessons = courseRepository.findById(courseId).get().getLessons();
        String result = mockMvc.perform(get("/api/courses/{id}/lessons", courseId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andReturn().getResponse().getContentAsString();
        System.out.println(result);
        List<Lesson> actuaLessons = objectMapper.readerForListOf(Lesson.class).readValue(result);
        assertTrue(isEqualCollection(expectedLessons, actuaLessons));
    }
    
    @Test
    void testGetCourseStudentsShouldRetrieveAllStudentsForCourseById() throws Exception {
        long courseId = 1;
        List<Student> expectedStudents = courseRepository.findById(courseId).get().getStudents();
        String result = mockMvc.perform(get("/api/courses/{id}/students", courseId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        List<Lesson> actuaLessons = objectMapper.readerForListOf(Student.class).readValue(result);
        assertTrue(isEqualCollection(expectedStudents, actuaLessons));
    }
    
    @Test
    void testGetCourseTeachersShouldRetrieveAllTeachersForCourseById() throws Exception {
        long courseId = 1;
        List<Teacher> expectedTeachers = courseRepository.findById(courseId).get().getTeachers();
        String result = mockMvc.perform(get("/api/courses/{id}/teachers", courseId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        List<Lesson> actuaLessons = objectMapper.readerForListOf(Teacher.class).readValue(result);
        assertTrue(isEqualCollection(expectedTeachers, actuaLessons));
    }
    
    @Test
    void testPostCourseShouldCreateCourseEntryInDataBase() throws Exception {
        Course course = new Course("test course", "description");
        long numberOfEntriesBeforePost = courseRepository.count();
        mockMvc.perform(post("/api/courses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(course)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.courseId", is(4)))
                .andExpect(jsonPath("$.courseName", is(course.getCourseName())))
                .andExpect(jsonPath("$.courseDescription", is(course.getCourseDescription())));
        long numberOfEntriesAfterPost = courseRepository.count();
        assertNotNull(courseRepository.getByCourseName(course.getCourseName()));
        assertEquals(course.getCourseDescription(), courseRepository.findById(4L).get().getCourseDescription());
        assertEquals(1, numberOfEntriesAfterPost - numberOfEntriesBeforePost);
    }
    
    @Test
    void testPutCourseShouldUpdateCourseEntryInDataBase() throws Exception {
        Course course = new Course(1, "update course", "upd");
        Course courseBeforeUpdating = courseRepository.findById(1L).get();
        mockMvc.perform(put("/api/courses")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(course)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.courseId", is(1)))
                .andExpect(jsonPath("$.courseName", is(course.getCourseName())))
                .andExpect(jsonPath("$.courseDescription", is(course.getCourseDescription())));
        Course courseAfterUpdating = courseRepository.findById(1L).get();
        assertNotEquals(course, courseBeforeUpdating);
        assertEquals(course, courseAfterUpdating);
    }
    
    @Test
    void testDeleteCourseSchouldDeleteCourseEntryFromDataBase() throws Exception {
        courseRepository.save(new Course("new course", "description"));
        long numberOfEntriesBeforeDelete = courseRepository.count();
        mockMvc.perform(delete("/api/courses/{id}", 4L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.courseId", is(4)));
        long numberOfEntriesAfterDelete = courseRepository.count();
        assertEquals(1, numberOfEntriesBeforeDelete - numberOfEntriesAfterDelete);
        assertFalse(courseRepository.findById(4L).isPresent());
    }
}
