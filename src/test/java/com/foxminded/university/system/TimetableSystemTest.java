package com.foxminded.university.system;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static java.util.Collections.singleton;
import static org.apache.commons.collections.CollectionUtils.isEqualCollection;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.foxminded.university.dao.DateTimeRepository;
import com.foxminded.university.dao.LessonRepository;
import com.foxminded.university.domain.model.Teacher;
import com.foxminded.university.domain.model.DateTime;
import com.foxminded.university.domain.model.Group;
import com.foxminded.university.domain.model.Auditorium;
import com.foxminded.university.domain.model.Course;
import com.foxminded.university.domain.model.Lesson;

@SpringBootTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
@Sql(scripts = { "/create_test_tables.sql", "/testData.sql" })
@AutoConfigureMockMvc
class TimetableSystemTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private DateTimeRepository dateTimeRepository;
    @Autowired
    private LessonRepository lessonRepository;

    @Test
    void testGetAllDateTimesShouldRetrieveAllDateTimesFromDataBase() throws Exception {
        String result = mockMvc.perform(get("/api/timetable/datetimes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(3)))
                .andReturn().getResponse().getContentAsString();
        List<DateTime> actual = objectMapper.readerForListOf(DateTime.class).readValue(result);
        List<DateTime> expected = dateTimeRepository.findAll();
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetDateTimeShouldRetrieveDateTimeFromDataBaseById() throws Exception {
        long dateTimeId = 1;
        DateTime expectedDateTime = dateTimeRepository.findById(dateTimeId).get();
        String result = mockMvc.perform(get("/api/timetable/datetimes/{dateTimeId}", dateTimeId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTimeId", is(1)))
                .andExpect(jsonPath("$.lessonDate", is(expectedDateTime.getLessonDate().toString())))
                .andExpect(jsonPath("$.startLesson", is(expectedDateTime.getStartLesson().toString())))
                .andExpect(jsonPath("$.endLesson", is(expectedDateTime.getEndLesson().toString())))
                .andReturn().getResponse().getContentAsString();
        DateTime actualDateTime = objectMapper.readValue(result, DateTime.class);
        assertEquals(expectedDateTime, actualDateTime);
    }
    
    @Test
    void testGetDateTimeLessonsShouldRetrieveAllLessonsForDateTimeById() throws Exception {
        long dateTimeId = 1;
        List<Lesson> expectedLessons = dateTimeRepository.findById(dateTimeId).get().getLessons();
        String result = mockMvc.perform(get("/api/timetable/datetimes/{dateTimeId}/lessons", dateTimeId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        List<Lesson> actuaLessons = objectMapper.readerForListOf(Lesson.class).readValue(result);
        assertTrue(isEqualCollection(expectedLessons, actuaLessons));
    }
    
    @Test
    void testPostDateTimeShouldCreateDateTimeEntryInDataBase() throws Exception {
        DateTime dateTime = new DateTime(LocalDate.of(2021, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        long numberOfEntriesBeforePost = dateTimeRepository.count();
        mockMvc.perform(post("/api/timetable/datetimes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dateTime)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTimeId", is(4)))
                .andExpect(jsonPath("$.lessonDate", is(dateTime.getLessonDate().toString())))
                .andExpect(jsonPath("$.startLesson", is(dateTime.getStartLesson().toString())))
                .andExpect(jsonPath("$.endLesson", is(dateTime.getEndLesson().toString())));
        long numberOfEntriesAfterPost = dateTimeRepository.count();
        assertEquals(dateTime.getLessonDate(), dateTimeRepository.findById(4L).get().getLessonDate());
        assertEquals(1, numberOfEntriesAfterPost - numberOfEntriesBeforePost);
    }
    
    @Test
    void testPutDateTimeShouldUpdateDateTimeEntryInDataBase() throws Exception {
        DateTime dateTime = new DateTime(1, LocalDate.of(2021, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        DateTime dateTimeBeforeUpdating = dateTimeRepository.findById(1L).get();
        mockMvc.perform(put("/api/timetable/datetimes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dateTime)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTimeId", is(1)))
                .andExpect(jsonPath("$.lessonDate", is(dateTime.getLessonDate().toString())))
                .andExpect(jsonPath("$.startLesson", is(dateTime.getStartLesson().toString())))
                .andExpect(jsonPath("$.endLesson", is(dateTime.getEndLesson().toString())));
        DateTime dateTimeAfterUpdating = dateTimeRepository.findById(1L).get();
        assertNotEquals(dateTime, dateTimeBeforeUpdating);
        assertEquals(dateTime, dateTimeAfterUpdating);
    }
    
    @Test
    void testDeleteDateTimeSchouldDeleteDateTimeEntryFromDataBase() throws Exception {
        dateTimeRepository.save(new DateTime(LocalDate.of(2021, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)));
        long numberOfEntriesBeforeDelete = dateTimeRepository.count();
        mockMvc.perform(delete("/api/timetable/datetimes/{dateTimeId}", 4L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTimeId", is(4)));
        long numberOfEntriesAfterDelete = dateTimeRepository.count();
        assertEquals(1, numberOfEntriesBeforeDelete - numberOfEntriesAfterDelete);
        assertFalse(dateTimeRepository.findById(4L).isPresent());
    }
    
    @Test
    void testGetAllLessonsShouldRetrieveAllLessonsFromDataBase() throws Exception {
        String result = mockMvc.perform(get("/api/timetable/lessons"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(3)))
                .andReturn().getResponse().getContentAsString();
        List<Lesson> actual = objectMapper.readerForListOf(Lesson.class).readValue(result);
        List<Lesson> expected = lessonRepository.findAll();
        assertEquals(expected, actual);
    }
    
    @Test
    void testGetLessonShouldRetrieveLessonFromDataBaseById() throws Exception {
        long lessonId = 1;
        Lesson expectedLesson = lessonRepository.findById(lessonId).get();
        String result = mockMvc.perform(get("/api/timetable/lessons/{id}", lessonId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.lessonId", is(1)))
                .andExpect(jsonPath("$.dateTime", Matchers.notNullValue()))
                .andExpect(jsonPath("$.group", Matchers.notNullValue()))
                .andExpect(jsonPath("$.auditorium", Matchers.notNullValue()))
                .andExpect(jsonPath("$.course", Matchers.notNullValue()))
                .andExpect(jsonPath("$.teacher", Matchers.notNullValue()))
                .andReturn().getResponse().getContentAsString();
        assertEquals(expectedLesson, objectMapper.readValue(result, Lesson.class));
    }
    
    @Test
    void testPostLessonShouldCreateLessonEntryInDataBase() throws Exception {
        Lesson lesson = getTestLesson();
        dateTimeRepository.save(new DateTime(LocalDate.of(2021, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)));
        long numberOfEntriesBeforePost = lessonRepository.count();
        mockMvc.perform(post("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(lesson)))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTime", Matchers.notNullValue()))
                .andExpect(jsonPath("$.group", Matchers.notNullValue()))
                .andExpect(jsonPath("$.auditorium", Matchers.notNullValue()))
                .andExpect(jsonPath("$.course", Matchers.notNullValue()))
                .andExpect(jsonPath("$.teacher", Matchers.notNullValue()));
        long numberOfEntriesAfterPost = lessonRepository.count();
        assertEquals(lesson.getDateTime(), lessonRepository.findById(4L).get().getDateTime());
        assertEquals(1, numberOfEntriesAfterPost - numberOfEntriesBeforePost);
    }
    
    @Test
    void testPutLessonShouldUpdateLessonEntryInDataBase() throws Exception {
        dateTimeRepository.save(new DateTime(LocalDate.of(2021, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45)));
        Lesson lesson = getTestLesson();
        lesson.setLessonId(1);
        Lesson lessonBeforeUpdating = lessonRepository.findById(1L).get();
        mockMvc.perform(put("/api/timetable/lessons")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(lesson)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.dateTime", Matchers.notNullValue()))
                .andExpect(jsonPath("$.group", Matchers.notNullValue()))
                .andExpect(jsonPath("$.auditorium", Matchers.notNullValue()))
                .andExpect(jsonPath("$.course", Matchers.notNullValue()))
                .andExpect(jsonPath("$.teacher", Matchers.notNullValue()));
        Lesson lessonAfterUpdating = lessonRepository.findById(1L).get();
        assertNotEquals(lesson, lessonBeforeUpdating);
        assertEquals(lesson, lessonAfterUpdating);
    }
    
    @Test
    void testDeleteLessonSchouldDeleteLessonEntryFromDataBase() throws Exception {
        long numberOfEntriesBeforeDelete = lessonRepository.count();
        mockMvc.perform(delete("/api/timetable/lessons/{id}", 3L)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
        long numberOfEntriesAfterDelete = lessonRepository.count();
        assertEquals(1, numberOfEntriesBeforeDelete - numberOfEntriesAfterDelete);
        assertFalse(lessonRepository.findById(3L).isPresent());
    }
    
    private Lesson getTestLesson() {
        Auditorium testAuditorium = new Auditorium(1, 10, 100);
        Course testCourse = new Course(1, "course1", "course description1");
        DateTime testDateTime = new DateTime(4, LocalDate.of(2021, 9, 1), LocalTime.of(8, 0), LocalTime.of(8, 45));
        Group testGroup = new Group(1, "group1");
        Teacher testTeacher = new Teacher(1, "teacher1");
        testTeacher.setCourses(singleton(testCourse));
        Lesson testLesson = new Lesson();
        testLesson.setAuditorium(testAuditorium);
        testLesson.setCourse(testCourse);
        testLesson.setDateTime(testDateTime);
        testLesson.setGroup(testGroup);
        testLesson.setTeacher(testTeacher);
        return testLesson;
    }
}
