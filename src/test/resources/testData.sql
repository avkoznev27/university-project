INSERT INTO AUDITORIUMS(auditorium_id, room_number, capacity) VALUES(DEFAULT, 10, 100);
INSERT INTO AUDITORIUMS(auditorium_id, room_number, capacity) VALUES(DEFAULT, 11, 101);
INSERT INTO AUDITORIUMS(auditorium_id, room_number, capacity) VALUES(DEFAULT, 12, 102);

INSERT INTO COURSES(course_id, course_name, course_description) VALUES(DEFAULT, 'course1', 'course description1');
INSERT INTO COURSES(course_id, course_name, course_description) VALUES(DEFAULT, 'course2', 'course description2');
INSERT INTO COURSES(course_id, course_name, course_description) VALUES(DEFAULT, 'course3', 'course description3');

INSERT INTO DATE_TIMES(date_time_id, lesson_date, start_lesson, end_lesson) VALUES(DEFAULT, '2020-09-01', '08:00:00', '08:45:00');
INSERT INTO DATE_TIMES(date_time_id, lesson_date, start_lesson, end_lesson) VALUES(DEFAULT, '2020-09-02', '09:00:00', '09:45:00');
INSERT INTO DATE_TIMES(date_time_id, lesson_date, start_lesson, end_lesson) VALUES(DEFAULT, '2020-09-03', '10:00:00', '10:45:00');

INSERT INTO GROUPS(group_id, group_name) VALUES(DEFAULT, 'group1');
INSERT INTO GROUPS(group_id, group_name) VALUES(DEFAULT, 'group2');
INSERT INTO GROUPS(group_id, group_name) VALUES(DEFAULT, 'group3');

INSERT INTO STUDENTS(student_id, group_id, name) VALUES(DEFAULT, 1, 'student1');
INSERT INTO STUDENTS(student_id, group_id, name) VALUES(DEFAULT, 1, 'student2');
INSERT INTO STUDENTS(student_id, group_id, name) VALUES(DEFAULT, 2, 'student3');

INSERT INTO TEACHERS(teacher_id, name) VALUES(DEFAULT, 'teacher1');
INSERT INTO TEACHERS(teacher_id, name) VALUES(DEFAULT, 'teacher2');
INSERT INTO TEACHERS(teacher_id, name) VALUES(DEFAULT, 'teacher3');

INSERT INTO LESSONS(lesson_id, date_time_id, group_id, auditorium_id, course_id, teacher_id) VALUES(DEFAULT, 1, 3, 2, 1, 1);
INSERT INTO LESSONS(lesson_id, date_time_id, group_id, auditorium_id, course_id, teacher_id) VALUES(DEFAULT, 2, 1, 3, 1, 2);
INSERT INTO LESSONS(lesson_id, date_time_id, group_id, auditorium_id, course_id, teacher_id) VALUES(DEFAULT, 3, 1, 1, 3, 2);

INSERT INTO TEACHERS_COURSES(teacher_id, course_id) VALUES(1, 1);
INSERT INTO TEACHERS_COURSES(teacher_id, course_id) VALUES(1, 2);
INSERT INTO TEACHERS_COURSES(teacher_id, course_id) VALUES(2, 3);

INSERT INTO STUDENTS_COURSES(student_id, course_id) VALUES(1, 1);
INSERT INTO STUDENTS_COURSES(student_id, course_id) VALUES(1, 2);
INSERT INTO STUDENTS_COURSES(student_id, course_id) VALUES(2, 3);

INSERT INTO SETTINGS(id, lesson_duration, max_group_size) values(1, 45, 30);
