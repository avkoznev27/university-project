-- Table: public.auditoriums
DROP TABLE IF EXISTS public.auditoriums CASCADE;
CREATE TABLE public.auditoriums
(
    auditorium_id SERIAL NOT NULL,
    room_number integer NOT NULL,
    capacity integer NOT NULL,
    CONSTRAINT auditoriums_pkey PRIMARY KEY (auditorium_id)
);

-- Table: public.courses
DROP TABLE IF EXISTS public.courses CASCADE;
CREATE TABLE public.courses
(
    course_id SERIAL NOT NULL,
    course_name character varying NOT NULL,
    course_description text NOT NULL,
    CONSTRAINT courses_pkey PRIMARY KEY (course_id)
);
    
-- Table: public.groups
DROP TABLE IF EXISTS public.groups CASCADE;
CREATE TABLE public.groups
(
    group_id SERIAL NOT NULL,
    group_name character varying NOT NULL,
    CONSTRAINT groups_pkey PRIMARY KEY (group_id)
);

-- Table: public.students
DROP TABLE IF EXISTS public.students CASCADE;
CREATE TABLE public.students
(
    student_id SERIAL NOT NULL,
    group_id integer NOT NULL,
    name character varying NOT NULL,
    CONSTRAINT students_pkey PRIMARY KEY (student_id),
    CONSTRAINT group_id_student FOREIGN KEY (group_id) REFERENCES public.groups (group_id) ON UPDATE CASCADE ON DELETE CASCADE
);

-- Table: public.teachers
DROP TABLE IF EXISTS public.teachers CASCADE;
CREATE TABLE public.teachers
(
    teacher_id SERIAL NOT NULL,
    name character varying NOT NULL,
    CONSTRAINT teachers_pkey PRIMARY KEY (teacher_id)
);

-- Table: public.students_courses
DROP TABLE IF EXISTS public.students_courses CASCADE;
CREATE TABLE public.students_courses
(
    student_id integer,
    course_id integer,
    CONSTRAINT course_id_scfk FOREIGN KEY (course_id) REFERENCES public.courses (course_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT student_id_scfk FOREIGN KEY (student_id) REFERENCES public.students (student_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT student_course UNIQUE (student_id, course_id)
);

-- Table: public.teachers_courses
DROP TABLE IF EXISTS public.teachers_courses CASCADE;
CREATE TABLE public.teachers_courses
(
    teacher_id integer,
    course_id integer,
    CONSTRAINT course_id_tkfk FOREIGN KEY (course_id) REFERENCES public.courses (course_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT teacher_id_tkfk FOREIGN KEY (teacher_id) REFERENCES public.teachers (teacher_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT teacher_course UNIQUE (teacher_id, course_id)
);

-- Table: public.date_times
DROP TABLE IF EXISTS public.date_times CASCADE;
CREATE TABLE public.date_times
(
    date_time_id SERIAL NOT NULL,
    lesson_date date NOT NULL,
    start_lesson time NOT NULL,
    end_lesson time NOT NULL,
    CONSTRAINT date_times_pkey PRIMARY KEY (date_time_id)
);

-- Table: public.lessons
DROP TABLE IF EXISTS public.lessons CASCADE;
CREATE TABLE public.lessons
(
    lesson_id SERIAL NOT NULL,
    date_time_id integer NOT NULL,
    group_id integer NOT NULL,
    auditorium_id integer NOT NULL,
    course_id integer NOT NULL,
    teacher_id integer NOT NULL,
    CONSTRAINT lessons_pkey PRIMARY KEY (lesson_id),
    CONSTRAINT date_time_id_lessonsfk FOREIGN KEY (date_time_id) REFERENCES public.date_times (date_time_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT group_id_lessonsfk FOREIGN KEY (group_id) REFERENCES public.groups (group_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT auditorium_id_lessonsfk FOREIGN KEY (auditorium_id) REFERENCES public.auditoriums (auditorium_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT course_id_lessonsfk FOREIGN KEY (course_id) REFERENCES public.courses (course_id) ON UPDATE CASCADE ON DELETE CASCADE,
    CONSTRAINT teacher_id_lessonsfk FOREIGN KEY (teacher_id) REFERENCES public.teachers (teacher_id) ON UPDATE CASCADE ON DELETE CASCADE
);
    
-- Table: public.settings
DROP TABLE IF EXISTS public.settings CASCADE;  
CREATE TABLE public.settings
(
    id bigint NOT NULL,
    lesson_duration integer NOT NULL DEFAULT 45,
    max_group_size integer NOT NULL DEFAULT 30,
    CONSTRAINT settings_pkey PRIMARY KEY (id)
);  
    